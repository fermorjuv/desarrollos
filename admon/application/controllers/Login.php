<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->session->userdata('user') != null) {
            redirect("home");
        }

        $this->load->library('form_validation');
        $this->load->model('login_model');
        
        $username = $this->input->post("user");
        $password = $this->input->post("password");

        $this->form_validation->set_rules("user", "Usuario", "trim|required");
        $this->form_validation->set_rules("password", "Clave", "trim|required");

        if ($this->form_validation->run() == FALSE) {

            $this->session->sess_destroy();
            $this->load->view('login/index');

        } else {

            if ($this->input->post('btn_login') == "Ingresar") {

                $users = $this->login_model->getUser($username, $password);

                if (count($users) > 0) {

                    $user = array_shift($users);

                    $sessiondata = array(
                        'user'  => $username,
                        'name'  => $user->name,
                        'email' => $user->email,
                        'role'  => $user->role
                    );

                    $sessiondata['modules'] = $this->login_model->getModules($user->idRole);

                    $this->session->set_userdata('user', $sessiondata);
                    redirect("home");

                } else {

                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Usuario y/o clave incorrectos</div>');
                    redirect('login');

                }
            } else {

                redirect('login');

            }
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }
}
