<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provider extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('rat');
        $this->load->model('provider_model');
    }

    public function index()
    {
        $this->load->library('pagination');
        $this->load->helper('crud');

        $config['per_page'] = 8;
        $config['base_url'] = site_url('/'.$this->router->fetch_class().'/');
        $config['total_rows'] = $this->provider_model->getCountElements($this->input->get('search'));
        $choice = $config["total_rows"] / $config["per_page"];
        $config['suffix'] = ($this->input->get('search') != null) ? '?'.http_build_query($_GET, '', "&") : '' ;
        $config['first_url'] = $config['base_url'].$config['suffix'];
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

        $data['elements'] = $this->provider_model->getElements($config["per_page"], max(0, ( $data['page'] -1 ) * $config["per_page"]), $this->input->get('search'));

        $data['pagination'] = $this->pagination->create_links(); 

        $data['search'] = $this->input->get('search');

        // Valores de las columnas, id => nombre del campo de la tabla, name => texto de la tabla, size => valor responsive de la tabla (hasta 10, hay que dejar 2 para los botones)
        $data['columns'] = array(
            array('id' => 'code', 'name' => 'C&oacute;digo', 'size' => '3'),
            array('id' => 'name', 'name' => 'Nombre', 'size' => '7')
        );

        $data['texts'] = array(
            'title' => 'Proveedores',
            'add' => 'Nuevo proveedor',
            'edit' => 'Modificar proveedor',
            'del' => 'Eliminar proveedor',
            'view' => 'Consultar proveedor',
            'confirm' => 'Seguro que desea eliminar el proveedor?'
        );
        
        $data['fields'] = $this->getFields();

        $this->load->view($this->router->fetch_class().'/index', $data);
    }

    public function add()
    {
        $this->load->library('form_validation');
        $this->load->helper('crud');

        $this->form_validation->set_rules('code', 'C&oacute;digo', 'required|callback_code_check');
        $this->form_validation->set_rules('name', 'Nombre', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $data['texts'] = array(
                'title' => 'Nuevo proveedor',
                'saveBtn' => 'Insertar'
            );
            $data['fields'] = $this->getFields();
            $this->load->view($this->router->fetch_class().'/add', $data);
        } else {
            $insert = $this->provider_model->insert();
            if ($insert === TRUE){
                $this->session->set_flashdata('msg', '<div class="alert alert-success">El proveedor se ha insertado correctamente</div>');
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger">Ha ocurrido un error insertando el proveedor: ' . $insert . '</div>');
            }
            redirect($this->router->fetch_class());
        }
    }

    public function edit($id)
    {
        $this->load->library('form_validation');
        $this->load->helper('crud');

        $this->form_validation->set_rules('code', 'C&oacute;digo', 'required|callback_code_check');
        $this->form_validation->set_rules('name', 'Nombre', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $data['texts'] = array(
                'title' => 'Modificar proveedor',
                'saveBtn' => 'Modificar'
            );
            $data['fields'] = $this->getFields();
            $data['element'] = $this->provider_model->getElement($id);
            $this->load->view($this->router->fetch_class().'/edit', $data);
        } else {
            $edit = $this->provider_model->update($id);
            if ($edit === TRUE){
                $this->session->set_flashdata('msg', '<div class="alert alert-success">El proveedor se ha modificado correctamente</div>');
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger">Ha ocurrido un error modificando el proveedor: ' . $edit . '</div>');
            }
            redirect('provider');
        }
    }

    public function del($id)
    {
        $delete = $this->provider_model->delete($id);
        if ($delete === TRUE){
            $this->session->set_flashdata('msg', '<div class="alert alert-success">El proveedor se ha eliminado correctamente</div>');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger">Ha ocurrido un error eliminando el proveedor: ' . $delete . '</div>');
        }
        redirect('provider');
    }

    public function ajaxGetElement($id)
    {
        if ($this->input->is_ajax_request()) {
            $this->provider_model->jsonGetElement($id);
        }
    }

    public function code_check($code)
    {
        if ($this->provider_model->elementExist('code', $code, $this->input->post('id'))) {
            $this->form_validation->set_message('code_check', 'El c&oacute;digo insertado ya corresponde a otro proveedor');
            return FALSE;
        }
        
        return TRUE;
    }

    private function getFields()
    {
        return array(
            array(
                'label'         => 'C&oacute;digo*',
                'type'          => 'number',
                'name'          => 'code',
                'id'            => 'code',
                'maxlength'     => '100',
                'class'         => 'form-control'
            ),
            array(
                'label'         => 'Nombre*',
                'type'          => 'text',
                'name'          => 'name',
                'id'            => 'name',
                'maxlength'     => '100',
                'class'         => 'form-control'
            ),
            array(
                'label'         => 'NIF',
                'type'          => 'text',
                'name'          => 'nif',
                'id'            => 'nif',
                'maxlength'     => '100',
                'class'         => 'form-control'
            ),
            array(
                'label'         => 'IBAN',
                'type'          => 'text',
                'name'          => 'iban',
                'id'            => 'iban',
                'maxlength'     => '100',
                'class'         => 'form-control'
            ),
            array(
                'label'         => 'Responsable',
                'type'          => 'text',
                'name'          => 'responsable',
                'id'            => 'responsable',
                'maxlength'     => '100',
                'class'         => 'form-control'
            ),
            array(
                'label'         => 'Descuento',
                'type'          => 'number',
                'name'          => 'discount',
                'id'            => 'discount',
                'maxlength'     => '6',
                'class'         => 'form-control',
                'step'          => 'any'
            ),

        );
    }

    public function ajaxGetProviders()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('provider_model');
            if ($this->input->get('term') != null) {
                $request = $this->input->get('term');
                $this->provider_model->jGetProviders($request);
            }
        }
    }
}
