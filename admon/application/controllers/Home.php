<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('rat');
    }

    public function index()
    {
        //$this->rat->log('MENSAJE', 'INFO', $this->session->userdata('user')['user']);
        $this->load->view('home/index');
    }

    public function getProviders()
    {
        $this->load->model('provider_model');
        if (isset($_GET['term'])){
            $request = $_GET['term'];
            $this->provider_model->getProviders($request);
        }
    }
}
