<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getUser($user, $password)
    {
        $this->db->select('user.id, user.user, user.name, user.email, role.id as idRole, role.name as role');
        $this->db->from('user');
        $this->db->join('role', 'user.id_role = role.id');
        $this->db->where(
            array(
                'user'      => $user,
                'password'  => md5($password),
                'status'    => '1'
            )
        );
        $query = $this->db->get();

        return $query->result();
    }

    function getModules($role)
    {
        $this->db->select('module.controller, module.action');
        $this->db->from('module');
        $this->db->join('module_role', 'module.id = module_role.id_module');
        $this->db->where('module_role.id_role', $role);
        $query = $this->db->get();

        $result = array();
        foreach ($query->result() as $modules) {
            $result[$modules->controller][] = $modules->action;
        }

        return $result;
    }
}