<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class provider_model extends CI_Model
{
    var $table;
    var $fields = array();
    var $fkTable = '';
    var $fkJoinField = '';
    var $fkFields = array();

    function __construct()
    {
        parent::__construct();
        $this->table = 'provider';
        $this->fields = array('id' => 'id', 'code' => 'code', 'name' => 'name', 'nif' => 'nif', 'iban' => 'iban', 'responsable' => 'responsable', 'discount' => 'discount');
    }

    function getCountElements($request = NULL)
    {
        if ($request != NULL) {
            $this->db->like('name', $request);
            $this->db->or_like('code', $request);
        }
        $this->db->where('status', 1);
        return $this->db->count_all_results($this->table);
    }

    function getElements($limit, $start, $request = NULL)
    {
        foreach($this->fields as $field => $alias) {
            $this->db->select($this->table.'.'.$field.' as '.$alias);
        }
        foreach($this->fkFields as $field => $alias) {
            $this->db->select($this->fkTable.'.'.$field.' as '.$alias);
        }
        if ($request != NULL) {
            $this->db->like('name', $request);
            $this->db->or_like('code', $request);
        }
        if ($this->fkTable != '') {
            $this->db->join($this->fkTable, $this->table.'.'.$this->fkJoinField.'='.$this->fkTable.'.id');
        }
        $this->db->where($this->table.'.status', 1);
        $this->db->limit($limit, $start);
        $query = $this->db->get($this->table);

        return $query->result_object();
    }

    function getActiveElements()
    {
        foreach($this->fields as $field => $alias) {
            $this->db->select($this->table.'.'.$field.' as '.$alias);
        }
        $this->db->where('status', 1);
        $query = $this->db->get($this->table);

        return $query->result_object();
    }

    function getElement($id)
    {
        foreach($this->fields as $field => $alias) {
            $this->db->select($this->table.'.'.$field.' as '.$alias);
        }
        $this->db->where('id', $id);
        $query = $this->db->get($this->table);

        return $query->row();
    }

    function insert()
    {
        if(!$this->db->insert($this->table, $_POST)) {
            return $this->db->error()['message'];
        }

        return TRUE;
    }

    function update($id)
    {
        $data = $_POST;
        $data['update'] = date('Y-m-d H:i:s');
        if(!$this->db->update($this->table, $data, 'id = '.$id)) {
            return $this->db->error()['message'];
        }

        return TRUE;
    }

    function delete($id)
    {
        $data = array('status' => 0);
        $data['update'] = date('Y-m-d H:i:s');
        if(!$this->db->update($this->table, $data, 'id = '.$id)) {
            return $this->db->error()['message'];
        }

        return TRUE;
    }

    function elementExist($field, $value, $id = NULL)
    {
        $this->db->where($field, $value);
        $this->db->where('status', 1);
        if ($id != NULL) {            
            $this->db->where('id !=', $id);
        }
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function jsonGetElement($id)
    {
        foreach($this->fields as $field => $alias) {
            $this->db->select($this->table.'.'.$field.' as '.$alias);
        }
        $this->db->where('id', $id);
        $query = $this->db->get($this->table);
        echo json_encode($query->row());
    }

    function jGetProviders($request = '')
    {
        $this->db->select('id, code, name');
        if ($request != '') {
            $this->db->like('name', $request);
            $this->db->or_like('code', $request);
        }
        $this->db->where('status', 1);
        $query = $this->db->get($this->table);

        $providers = array();
        if($query->num_rows() > 0) {
            foreach ($query->result_object() as $row) {
                $provider['label']=htmlentities(stripslashes($row->code . ' - ' . $row->name));
                $provider['id']=htmlentities(stripslashes($row->id));
                $provider['code']=htmlentities(stripslashes($row->code));
                $provider['name']=htmlentities(stripslashes($row->name));
                $providers[] = $provider;
            }
        }
        echo json_encode($providers);
    }
}