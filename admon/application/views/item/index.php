<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
    <title><?php echo APP_TITLE.' - '.$texts['title']?></title>
</head>
<body>
    <?php $this->load->view('header');?>
    <div class="container">
        <div class="page-header" id="banner">
            <div class="row">
                <div class="col-lg-12">
                    <h1><?php echo $texts['title']?></h1>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <?php echo form_open($this->router->fetch_class(), 'method="get" id="searchForm"');?>
                        <div class="input-group">
                            <input type="text" class="form-control input-sm" name="search" id="search" placeholder="Introducir c&oacute;digo o nombre" value="<?php echo $search?>">
                            <span class="input-group-btn">
                            <button class="btn btn-primary btn-sm" id="searchBtn" type="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                            </span>
                        </div>
                    <?php echo form_close();?>
                </div>
                <div class="col-lg-6">
                    <a href="<?php echo site_url($this->router->fetch_class().'/add')?>" id="addBtn" class="btn btn-primary btn-sm pull-right"><?php echo $texts['add']?></a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="bs-component">
                        <table class="table table-striped table-condensed table-responsive">
                            <thead>
                              <tr>
                                <?php foreach($columns as $column) { ?>
                                <th><?php echo $column['name']?></td>
                                <?php } ?>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($elements) > 0) {
                                foreach($elements as $element) { 
                                ?>
                                  <tr>
                                    <?php foreach($columns as $column) { ?>
                                    <td class="col-md-<?php echo $column['size']?>"><?php echo $element->$column['id'];?></td>
                                    <?php } ?>
                                    <td class="col-md-2 text-right">
                                        <a href="javascript:view(0)" data-id="<?php echo $element->id?>" data-url="<?php echo site_url($this->router->fetch_class().'/ajaxGetElement/')?>" title="<?php echo $texts['view']?>" class="btn btn-default btn-sm viewBtn"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                                        <a href="<?php echo site_url($this->router->fetch_class().'/edit/'.$element->id)?>" title="<?php echo $texts['edit']?>" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                        <a href="<?php echo site_url($this->router->fetch_class().'/del/'.$element->id)?>" title="<?php echo $texts['del']?>" class="btn btn-danger btn-sm delBtn"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                    </td>
                                  </tr>
                                <?php 
                                }
                            } else {
                            ?>
                                <tr><td colspan="<?php echo count($columns)+1?>">No se encontraron registros con ese criterio de b&uacute;squeda</td></tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 ">
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
    <div id="dataConfirmModal" class="modal" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="dataConfirmLabel"></h3>
                </div>
                <div class="modal-body"><?php echo $texts['confirm']?></div>
                <div class="modal-footer">
                    <a class="btn btn-primary" id="dataConfirmOK">Aceptar</a>
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="dataViewModal" class="modal fade" role="dialog" aria-labelledby="dataViewLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="dataViewLabel"></h3>
                </div>
                <div class="modal-body">
                    <?php echo form_open('', 'method="post" id="modalForm" class="form-horizontal"');?>
                    <?php foreach($fields as $field) { ?>
                        <div class="form-group">
                            <label for="<?php echo $field['id']?>" class="col-lg-2 control-label"><?php echo $field['label']?></label>
                            <div class="col-lg-10">
                                <?php echo createFormElement($field) ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php echo form_close(); ?>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Volver</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/crud.js'); ?>"></script>
</body>
</html>