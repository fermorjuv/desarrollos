<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <!-- Latest compiled and minified CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
    <!-- CSS  -->
    <link href="<?php echo base_url("assets/css/login.css")?>" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
    <div class="container">
        <div class="login-container">
            <div id="output"></div>
            <div class="form-box">
                <?php
                $attributes = array("id" => "loginform", "name" => "loginform", "role" => "form", "class" => "form-signin");
                echo form_open("login/index", $attributes);
                ?>
                <div class="form-group">
                    <input class="form-control" id="user" name="user" placeholder="Usuario" type="text" value="<?php echo set_value('user'); ?>" autofocus />
                    <span class="text-danger"><?php echo form_error('user'); ?></span>
                </div>
                <div class="form-group">
                    <input class="form-control" id="password" name="password" placeholder="Clave" type="password" value="" />
                    <span class="text-danger"><?php echo form_error('password'); ?></span>
                </div>
                <?php echo $this->session->flashdata('msg'); ?>
                <!-- Change this to a button or input when using this as a form -->
                <input id="btn_login" name="btn_login" type="submit" class="btn btn-lg btn-success btn-block btn-signin" value="Ingresar" />
                <?php echo form_close(); ?>
            </div>
        </div><!-- /card-container -->
    </div><!-- /container -->
</body>
</html>