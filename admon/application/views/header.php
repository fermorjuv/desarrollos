<div class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Acuafix</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li <?php echo ($this->router->fetch_class() == 'home') ? 'class="active"' : '' ;?>><a href="<?php echo site_url('home')?>">Home</a></li>
        <li <?php echo ($this->router->fetch_class() == 'provider') ? 'class="active"' : '' ;?>><a href="<?php echo site_url('provider')?>">Proveedores</a></li>
        <li <?php echo ($this->router->fetch_class() == 'item') ? 'class="active"' : '' ;?>><a href="<?php echo site_url('item')?>">Art&iacute;culo</a></li>
      </ul>
      <!--<ul class="nav navbar-nav navbar-right">
        <li><a href="../navbar/">Default</a></li>
        <li><a href="../navbar-static-top/">Static top</a></li>
        <li class="active"><a href="./">Fixed top <span class="sr-only">(current)</span></a></li>
      </ul>-->
    </div><!--/.nav-collapse -->
  </div>
</div>