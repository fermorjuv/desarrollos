<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
    <title><?php echo APP_TITLE.' - '.$texts['title']?></title>
</head>
<body>
    <?php $this->load->view('header');?>
    <div class="container">
        <div class="page-header" id="banner">
            <div class="row">
                <div class="col-lg-12">
                    <h1><?php echo $texts['title']?></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <?php if (validation_errors() != '') { ?>
                        <div class="alert alert-dismissible alert-danger"><?php echo validation_errors(); ?></div>  
                    <?php } ?>                  
                    <?php echo form_open($this->router->fetch_class().'/add', 'method="post" id="searchForm" class="form-horizontal"');?>
                    <fieldset>
                        <?php foreach($fields as $field) { ?>
                        <div class="form-group <?php echo (form_error($field['id']) != '') ? 'has-error' : '' ;?>">
                            <label for="<?php echo $field['id']?>" class="col-lg-2 control-label"><?php echo $field['label']?></label>
                            <div class="col-lg-10">
                                <?php echo createFormElement($field, set_value($field['id'])) ?>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button type="submit" class="btn btn-primary"><?php echo $texts['saveBtn']?></button>
                                <a href="<?php echo site_url($this->router->fetch_class())?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </fieldset>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
</body>
</html>