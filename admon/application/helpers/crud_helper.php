<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('createFormElement')) {

    function createFormElement($element, $value = '')
    {
        $CI =& get_instance();

        $element['value'] = $value;

        switch($element['type']) {
            case 'text':
            case 'number':
                return form_input($element, $value);
                break;
            case 'select':
                return form_dropdown($element, $element['options'], $value);
                break;
            case 'textarea':
                return form_textarea($element, $value);
                break;
        }
    }   

}