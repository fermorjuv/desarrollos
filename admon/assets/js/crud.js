$(function () {
    $('#searchBtn').on('click', function(){
        ('#searchForm').submit();
    });
    $('.delBtn').on('click', function(){
        var href = $(this).attr('href');
        $('#dataConfirmOK').attr('href', href);
        $('#dataConfirmModal').modal({show:true});
        return false;
    });
    $('.viewBtn').on('click', function(){
        $.get( $(this).data('url') + $(this).data('id'), function( data ) {
            $.each(data, function(name, val){
                var $el = $('[name="'+name+'"]'),
                    type = $el.attr('type');
                switch(type){
                    case 'checkbox':
                        $el.attr('checked', 'checked');
                        break;
                    case 'radio':
                        $el.filter('[value="'+val+'"]').attr('checked', 'checked');
                        break;
                    default:
                        $el.val(val);
                }
            });
        }, "json" );
        $('#modalForm :input').attr("disabled", true);
        $('#dataViewModal').modal({show:true});
        return false;
    });
});