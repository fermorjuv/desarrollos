-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.6.21 - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.4.0.5169
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para admon
CREATE DATABASE IF NOT EXISTS `admon` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `admon`;

-- Volcando estructura para tabla admon.item
CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idProvider` int(11) NOT NULL DEFAULT '0',
  `code` varchar(100) NOT NULL DEFAULT '0',
  `name` varchar(200) NOT NULL DEFAULT '0',
  `description` text,
  `insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK__provider` (`idProvider`),
  CONSTRAINT `FK__provider` FOREIGN KEY (`idProvider`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla admon.item: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` (`id`, `idProvider`, `code`, `name`, `description`, `insert`, `update`, `status`) VALUES
	(2, 2, '1', 'Item 1', 'Item uno!', '2017-05-01 21:00:33', '2017-05-01 23:09:13', 1),
	(3, 14, '2', 'Item2', 'Motor dos!', '2017-05-01 23:52:58', '2017-05-01 23:53:07', 1);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;

-- Volcando estructura para tabla admon.module
CREATE TABLE IF NOT EXISTS `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controller` varchar(50) NOT NULL DEFAULT '0',
  `action` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla admon.module: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` (`id`, `controller`, `action`) VALUES
	(1, 'home', 'index'),
	(2, 'home', 'getProviders');
/*!40000 ALTER TABLE `module` ENABLE KEYS */;

-- Volcando estructura para tabla admon.module_role
CREATE TABLE IF NOT EXISTS `module_role` (
  `id_module` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `insert` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_module`,`id_role`),
  KEY `FK2_role` (`id_role`),
  KEY `FK1_module` (`id_module`),
  CONSTRAINT `FK1_module` FOREIGN KEY (`id_module`) REFERENCES `module` (`id`),
  CONSTRAINT `FK2_role` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla admon.module_role: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `module_role` DISABLE KEYS */;
INSERT INTO `module_role` (`id_module`, `id_role`, `insert`) VALUES
	(1, 1, '2017-04-09 23:20:04'),
	(1, 2, '2017-04-13 21:35:13');
/*!40000 ALTER TABLE `module_role` ENABLE KEYS */;

-- Volcando estructura para tabla admon.provider
CREATE TABLE IF NOT EXISTS `provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(6) NOT NULL DEFAULT '1',
  `name` varchar(100) NOT NULL DEFAULT '0',
  `nif` varchar(100) DEFAULT NULL,
  `iban` varchar(100) DEFAULT NULL,
  `responsable` varchar(100) DEFAULT NULL,
  `discount` decimal(5,2) NOT NULL DEFAULT '0.00',
  `insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla admon.provider: ~16 rows (aproximadamente)
/*!40000 ALTER TABLE `provider` DISABLE KEYS */;
INSERT INTO `provider` (`id`, `code`, `name`, `nif`, `iban`, `responsable`, `discount`, `insert`, `update`, `status`) VALUES
	(1, 1, 'proveedor 1', '457', '2346', 'Luisa Lanas', 11.50, '2017-04-14 12:07:09', '2017-05-01 20:29:09', 1),
	(2, 2, 'proveedor 2', '123', '3456', 'Pedro Pérez', 10.00, '2017-04-14 12:07:21', '2017-05-01 23:01:33', 1),
	(3, 3, 'proveedor3', NULL, NULL, NULL, 0.00, '2017-04-14 22:13:09', NULL, 1),
	(4, 4, 'proveedor4', NULL, NULL, NULL, 0.00, '2017-04-14 22:13:18', NULL, 1),
	(5, 5, 'proveedor5', NULL, NULL, NULL, 0.00, '2017-04-14 22:13:28', NULL, 1),
	(6, 6, 'proveedor6', NULL, NULL, NULL, 0.00, '2017-04-14 22:50:14', NULL, 1),
	(7, 7, 'proveedor7', NULL, NULL, NULL, 0.00, '2017-04-14 22:50:21', NULL, 1),
	(8, 8, 'proveedor8', NULL, NULL, NULL, 0.00, '2017-04-14 22:50:39', NULL, 1),
	(9, 9, 'proveedor9', NULL, NULL, NULL, 0.00, '2017-04-14 22:50:49', NULL, 1),
	(10, 10, 'proveedor10', NULL, NULL, NULL, 0.00, '2017-04-14 22:51:06', NULL, 1),
	(11, 11, 'proveedor11', NULL, NULL, NULL, 0.00, '2017-04-14 22:51:13', NULL, 1),
	(12, 12, 'proveedor12', NULL, NULL, NULL, 0.00, '2017-04-14 23:59:31', NULL, 1),
	(14, 13, 'proveedor13', NULL, NULL, NULL, 0.00, '2017-04-14 23:59:41', NULL, 1),
	(15, 14, 'Proveedor Prueba', NULL, NULL, NULL, 0.00, '2017-04-15 13:45:12', '2017-04-15 16:13:57', 0),
	(29, 111, 'sdx', NULL, NULL, NULL, 0.00, '2017-04-15 14:15:14', NULL, 0),
	(30, 15, 'Test', NULL, NULL, NULL, 0.00, '2017-04-15 14:24:57', NULL, 0),
	(32, 14, 'proveedor14', '', '', '', 0.00, '2017-05-01 20:15:45', NULL, 1),
	(33, 15, 'proveedor15', '', '', '', 0.00, '2017-05-01 23:01:03', NULL, 1);
/*!40000 ALTER TABLE `provider` ENABLE KEYS */;

-- Volcando estructura para tabla admon.role
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla admon.role: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `name`) VALUES
	(1, 'admin'),
	(2, 'user');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Volcando estructura para tabla admon.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` int(11) NOT NULL DEFAULT '0',
  `user` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(100) NOT NULL DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_role` (`id_role`),
  CONSTRAINT `FK_role` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla admon.user: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `id_role`, `user`, `password`, `name`, `email`, `insert`, `update`, `status`) VALUES
	(1, 1, 'fmoreno', '0c766d337234790e623c5712f5b7b6c7', 'fmoreno', 'fermorjuv@gmail.com', '2017-04-09 23:19:32', NULL, 1),
	(2, 2, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user', 'fermorjuv@gmail.com', '2017-04-09 23:19:40', NULL, 1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
