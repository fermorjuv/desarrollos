<script>
//Google Map 
$('.maps').click(function () {
    $('.maps iframe').css("pointer-events", "auto");
});
$( ".maps" ).mouseleave(function() {
  $('.maps iframe').css("pointer-events", "none"); 
});

$(function(){
    $("input[name=name]")[0].oninvalid = function () {
        if (this.value == '') {
            this.setCustomValidity("<?php echo $translate->_('This filed is required')?>");
        } else {
            this.setCustomValidity("");
        }
    };
    $("input[name=email]")[0].oninvalid = function () {
        if (this.value == '') {
            this.setCustomValidity("<?php echo $translate->_('This filed is required')?>");
        } else if (this.validity.patternMismatch) {
            this.setCustomValidity("<?php echo $translate->_('The email format is invalid')?>");
        } else {
            this.setCustomValidity("");
        }
    };
});
</script>