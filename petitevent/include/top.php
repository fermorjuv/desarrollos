<meta name="author" content="www.fmjweb.com.ar">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, maximum-scale=1">
<meta name="robots" content="index, follow" />

<link rel="stylesheet" href="<?php echo $baseUrl?>/css/zerogrid.css">
<link rel="stylesheet" href="<?php echo $baseUrl?>/css/style.css">
<link href="<?php echo $baseUrl?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo $baseUrl?>/css/menu.css">

<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $baseUrl?>/images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" href="<?php echo $baseUrl?>/images/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo $baseUrl?>/images/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="<?php echo $baseUrl?>/images/favicon/manifest.json">
<link rel="mask-icon" href="<?php echo $baseUrl?>/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">

<link href="https://plus.google.com/105190393196174927447" rel="publisher" />

<!--[if lt IE 8]>
   <div style=' clear: both; text-align:center; position: relative;'>
     <a href="http://windows.microsoft.com/en-US/internet-explorer/Items/ie/home?ocid=ie6_countdown_bannercode">
       <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
<![endif]-->
<!--[if lt IE 9]>
    <script src="<?php echo $baseUrl?>/js/html5.js"></script>
    <script src="<?php echo $baseUrl?>/js/css3-mediaqueries.js"></script>
<![endif]-->