<section id="container">
    <div class="wrap-container">
        <article class="content-box background-gray box-1 zerogrid">
                    <div class="art-header">
                        <hr class="line-2">
                        <h2><?php echo $translate->_('Our services') ?></h2>
                    </div>
            <div class="row wrap-box"><!--Start Box-->
                <div class="col-1-2">
                    <div class="wrap-col">
                        <div style="padding-top: 60px;">
                            <center><img src="<?php echo $baseUrl?>/images/servicios.jpg"></center>
                        </div>
                    </div>
                </div>
                <div class="col-1-2">
                    <div class="wrap-col">
                        <div class="row">
                            <div class="t-center" style="padding-top: 30px;">
                                <div class="header">
                                    <h2>Adem&aacute;s de la comida</h2>
                                </div>
                                <span>Un gran evento tiene miles de matices y a nosotros no nos gusta dejar ninguno desatendido. Es por eso que queremos poner a disposici&oacute;n de nuestros clientes todas las posibilidades para que su fiesta sea &uacute;nica e irrepetible. Para ello disponemos de servicio de alquiler de mobiliario (como sillas y mesas), manteles, cubresillas, vajilla, cuberter&iacute;a y un largo etc&eacute;tera.</span><br/><br>
                                <a href="<?php echo $baseUrl?>/contacto" class="button" >Inf&oacute;rmese de todo</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>