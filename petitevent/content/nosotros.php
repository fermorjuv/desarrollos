<section id="container">
    <div class="wrap-container">
        <!-----------------content-box-1-------------------->
        <article class="content-box background-gray box-1 zerogrid">
                    <div class="art-header">
                        <hr class="line-2">
                        <h2><?php echo $translate->_('About us') ?></h2>
                    </div>
            <div class="row wrap-box"><!--Start Box-->
                <div class="col-1-2">
                    <div class="wrap-col">
                        <div style="padding-top: 60px;">
                            <center><img src="<?php echo $baseUrl?>/images/asado.jpg"></center>
                        </div>
                    </div>
                </div>
                <div class="col-1-2">
                    <div class="wrap-col">
                        <div class="row">
                            <div class="t-center" style="padding-top: 30px;">
                                <div class="header">
                                    <h2>Carne <span class="color-red">a la parrilla</span></h2>
                                </div>
                                <strong>LA CARNE A LAS BRASAS SIEMPRE SABE MEJOR.</strong><br/><br>
                                <span>Nuestros productos de primera calidad son cocinados lentamente a las brasas en su punto justo, lo que le proporciona ese sabor tan caracter&iacute;stico y hace que la carne quede tan tierna y jugosa que se derrita en la boca.<br/>
                                ¿Qui&eacute;n puede resistirse a una buena carne hecha a la parrilla?</span><br/><br>
                                <a href="<?php echo $baseUrl?>/menu" class="button" >Ver nuestro men&uacute;</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        <!-----------------content-box-2-------------------->
        <article class="content-box background-white box-2 zerogrid">
            <div class="row wrap-box"><!--Start Box-->
                <div class="col-1-2 f-right">
                    <div class="wrap-col">
                        <div style="padding-top: 60px;">
                            <center><img src="<?php echo $baseUrl?>/images/empanadas.jpg"></center>
                        </div>
                    </div>
                </div>
                <div class="col-1-2">
                    <div class="wrap-col">
                        <div class="row">
                            <div class="t-center" style="padding-top: 30px;">
                                <div class="header">
                                    <h2>Entrantes</h2>
                                </div>
                                <strong>PORQUE EMPEZAR CON BUEN PIE ES ESENCIAL.</strong><br/><br>
                                <span>En las grandes celebraciones cada vez se le da m&aacute;s importancia a los entrantes o aperitivos que se sirven al inicio de esa comida o cena.<br/>
                                Nuestros entrantes criollos, seleccionados cuidadosamente, abren el apetito para los manjares que vendr&aacute;n despu&eacute;s.<br />
                                Antes de pasar a los platos principales los entrantes te estimular&aacute;n la vista, el olfato y el paladar para empezar una buena comida.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        <!-----------------content-box-3-------------------->
        <article class="content-box background-gray box-3 zerogrid">
            <div class="row wrap-box"><!--Start Box-->
                <div class="col-1-2">
                    <div class="wrap-col">
                        <div style="padding-top: 60px;">
                            <center><img src="<?php echo $baseUrl?>/images/ensalada.jpg"></center>
                        </div>
                    </div>
                </div>
                <div class="col-1-2">
                    <div class="wrap-col">
                        <div class="row">
                            <div class="t-center" style="padding-top: 30px;">
                                <div class="header">
                                    <h2>Barra de ensaladas</h2>
                                </div>
                                <strong>HAY QUE ESTAR SIEMPRE BIEN ACOMPA&Ntilde;ADO.</strong><br/><br>
                                <span>La comida rica no est&aacute; peleada con la comida sana. En Petit Event lo sabemos y por eso queremos brindar en nuestros eventos una amplia variedad de ensaladas para acompa&ntilde;ar nuestros platos.<br/>
                                La guarnici&oacute;n puede hacer que un buen plato, sea excelente. Adem&aacute;s, aporta nutrientes extra a nuestras comidas y sirven para complementar su valor cal&oacute;rico.</span><br/><br/>
                                <a href="<?php echo $baseUrl?>/contacto" class="button" >Cont&aacute;ctanos y haz tu reserva</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        <!-----------------content-box-4-------------------->
        <section class="content-box boxstyle-2 box-4">
            <div class="zerogrid">
                <div class="row wrap-box"><!--Start Box-->
                    <div class="header">
                        <hr class="line-1">
                        <h2 style="color: #fff;">Buscando la excelencia</h2>
                    </div>
                    <div class="post">
                        <div class="col-1-2">
                            <img src="<?php echo $baseUrl?>/images/evento.jpg"/>
                        </div>
                        <div class="col-1-2">
                            <div class="wrapper">
                                <h3>Un evento a tu medida</h3>
                                <p>En Petit Event deseamos que su fiesta se desarrolle en un entorno agradable, rodeado de la gente que usted quiere en esa fecha tan especial y que cada uno se seinta en libertad de elegir lo que desea comer.</p><br/>
                                <a class="button" href="<?php echo $baseUrl?>/eventos">Ver eventos</a>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </section>        
    </div>
</section>