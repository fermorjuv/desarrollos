<?php
$text = '';
$oldName = '';
$oldEmail = '';
$oldMessage = '';

if(count($_POST) && trim($_POST['name']) != '' && trim($_POST['email']) != '' && trim($_POST['message']) != '')
{
    $name       = $_POST['name'];
    $email      = $_POST['email'];
    $message    = $_POST['message'];

    $to         = "petitevent.catering@gmail.com";
    $subject    = "Formulario de contacto";
    $content    = "Nombre: " . $name ."\r\n Email: " . $email . "\r\n Mensaje:\r\n" . $message;
     
    $from       = "PetitEvent";
    $headers    = "From:" . $from . "\r\n";
    $headers    .= "Content-type: text/plain; charset=UTF-8" . "\r\n"; 
     
    if(@mail($to,$subject,$content,$headers))
    {
        $text = "<span class='color-red' style='font-size: 20px; line-height: 40px; margin: 10px;'>".$translate->_('Your email was sent successfully')."</span>";
    } else {
        $text = "<span class='color-red' style='font-size: 20px; line-height: 40px; margin: 10px;'>".$translate->_('An error has occurred. Please, try again')."</span>";
        $oldName = $name;
        $oldEmail = $email;
        $oldMessage = $message;
    }
}
?>
<section id="container">
    <div class="wrap-container clearfix">
        <div id="main-content">
            <div class="wrap-content zerogrid ">
                <article class="background-gray">
                    <div class="art-header">
                        <hr class="line-2">
                        <h2><?php echo $translate->_('Contact') ?></h2>
                    </div>
                    <div class="art-content">
                        <?php if ($text != '') { ?>
                        <!--Warning-->
                        <center><?php echo $text;?></center>
                        <!---->
                        <?php } ?>
                        <div class="row">
                            <div id="contact_form">
                                <form name="form1" id="ff" method="post" action="">
                                    <label class="row">
                                        <div class="col-1-2">
                                            <div class="wrap-col">
                                                <input type="text" name="name" id="name" placeholder="<?php echo $translate->_('Name') ?>" value="<?php echo $oldName ?>" required />
                                            </div>
                                        </div>
                                        <div class="col-1-2">
                                            <div class="wrap-col">
                                                <input type="text" name="email" id="email" placeholder="<?php echo $translate->_('Email') ?>" value="<?php echo $oldEmail ?>" required pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$" />
                                            </div>
                                        </div>
                                    </label>
                                    <label class="row">
                                        <div class="wrap-col">
                                            <textarea name="message" id="message" class="form-control" rows="4" cols="25" placeholder="<?php echo $translate->_('Message') ?>" required ><?php echo $oldMessage ?></textarea>
                                        </div>
                                    </label>
                                    <center><input class="sendButton" type="submit" name="Submit" value="<?php echo $translate->_('Send') ?>"></center>
                                </form>
                            </div>
                        </div>
                    </div>
                </article>
                <div class='embed-container maps'>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3075.3989865782787!2d2.687036015055484!3d39.57315737947217!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x129793848c40edcb%3A0x8b99db3a721f6e67!2sCarrer+Ca+Na+Melis%2C+28%2C+07007+Palma%2C+Illes+Balears%2C+Espa%C3%B1a!5e0!3m2!1ses!2sar!4v1485370730874" width="100%" height="370px" frameborder="0" style="border: 0"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>