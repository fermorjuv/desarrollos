<hr class="line">
<footer>
    <div class="wrap-footer">
        <div class="zerogrid">
            <div class="row">
                <div class="col-1-3">
                    <div class="wrap-col"></div>
                </div>
                <div class="col-1-3">
                    <div class="wrap-col">
                        <ul class="social-buttons">
                            <li><a href="https://twitter.com/petiteventok" target="_blank"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="https://www.facebook.com/PetitEvent" target="_blank"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="https://plus.google.com/105190393196174927447" target="_blank"><i class="fa fa-google-plus"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-1-3">
                    <div class="wrap-col">
                        <ul class="quick-link">
                            <?php if ($translate->getLocale() != 'es') { ?><li><a href="index.php<?php echo '?mod='.$module.'&lang=es' ?>">Espa&ntilde;ol</a></li><?php } ?>
                            <?php if ($translate->getLocale() != 'ca') { ?><li><a href="index.php<?php echo '?mod='.$module.'&lang=ca' ?>">Catal&agrave;</a></li><?php } ?>
                            <?php if ($translate->getLocale() != 'en') { ?><li><a href="index.php<?php echo '?mod='.$module.'&lang=en' ?>">English</a></li><?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>