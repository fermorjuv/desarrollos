<section id="container">
    <div class="wrap-container clearfix">
        <div id="main-content">
            <div class="wrap-content zerogrid ">
                <article class="background-gray">
                    <div class="art-header">
                        <hr class="line-2">
                        <h2><?php echo $translate->_('Photo album') ?></h2>
                    </div>
                    <div class="art-content">
                        <div class="row">
                            <div class="col-1-4">
                                <div class="wrap-col">
                                    <div class="item-container">
                                        <a class="example-image-link" href="<?php echo $baseUrl?>/images/photos/evento.jpg" data-lightbox="example-set" data-title="Descripci&oacute;n de la foto">
                                            <div class="item-caption">
                                                <div class="item-caption-inner">
                                                    <div class="item-caption-inner1">
                                                        <h3>Titulo</h3>
                                                        <span>Subtitulo</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <img class="example-image" src="<?php echo $baseUrl?>/images/photos/evento-thmb.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div class="item-container">
                                        <a class="example-image-link" href="<?php echo $baseUrl?>/images/photos/evento.jpg" data-lightbox="example-set" data-title="Descripci&oacute;n de la foto">
                                            <div class="item-caption">
                                                <div class="item-caption-inner">
                                                    <div class="item-caption-inner1">
                                                        <h3>Titulo</h3>
                                                        <span>Subtitulo</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <img class="example-image" src="<?php echo $baseUrl?>/images/photos/evento-thmb.jpg" alt=""/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1-4">
                                <div class="wrap-col">
                                    <div class="item-container">
                                        <a class="example-image-link" href="<?php echo $baseUrl?>/images/photos/evento.jpg" data-lightbox="example-set" data-title="Descripci&oacute;n de la foto">
                                            <div class="item-caption">
                                                <div class="item-caption-inner">
                                                    <div class="item-caption-inner1">
                                                        <h3>Titulo</h3>
                                                        <span>Subtitulo</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <img class="example-image" src="<?php echo $baseUrl?>/images/photos/evento-thmb.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div class="item-container">
                                        <a class="example-image-link" href="<?php echo $baseUrl?>/images/photos/evento.jpg" data-lightbox="example-set" data-title="Descripci&oacute;n de la foto">
                                            <div class="item-caption">
                                                <div class="item-caption-inner">
                                                    <div class="item-caption-inner1">
                                                        <h3>Titulo</h3>
                                                        <span>Subtitulo</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <img class="example-image" src="<?php echo $baseUrl?>/images/photos/evento-thmb.jpg" alt=""/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1-4">
                                <div class="wrap-col">
                                    <div class="item-container">
                                        <a class="example-image-link" href="<?php echo $baseUrl?>/images/photos/evento.jpg" data-lightbox="example-set" data-title="Descripci&oacute;n de la foto">
                                            <div class="item-caption">
                                                <div class="item-caption-inner">
                                                    <div class="item-caption-inner1">
                                                        <h3>Titulo</h3>
                                                        <span>Subtitulo</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <img class="example-image" src="<?php echo $baseUrl?>/images/photos/evento-thmb.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div class="item-container">
                                        <a class="example-image-link" href="<?php echo $baseUrl?>/images/photos/evento.jpg" data-lightbox="example-set" data-title="Descripci&oacute;n de la foto">
                                            <div class="item-caption">
                                                <div class="item-caption-inner">
                                                    <div class="item-caption-inner1">
                                                        <h3>Titulo</h3>
                                                        <span>Subtitulo</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <img class="example-image" src="<?php echo $baseUrl?>/images/photos/evento-thmb.jpg" alt=""/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1-4">
                                <div class="wrap-col">
                                    <div class="item-container">
                                        <a class="example-image-link" href="<?php echo $baseUrl?>/images/photos/evento.jpg" data-lightbox="example-set" data-title="Descripci&oacute;n de la foto">
                                            <div class="item-caption">
                                                <div class="item-caption-inner">
                                                    <div class="item-caption-inner1">
                                                        <h3>Titulo</h3>
                                                        <span>Subtitulo</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <img class="example-image" src="<?php echo $baseUrl?>/images/photos/evento-thmb.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div class="item-container">
                                        <a class="example-image-link" href="<?php echo $baseUrl?>/images/photos/evento.jpg" data-lightbox="example-set" data-title="Descripci&oacute;n de la foto">
                                            <div class="item-caption">
                                                <div class="item-caption-inner">
                                                    <div class="item-caption-inner1">
                                                        <h3>Titulo</h3>
                                                        <span>Subtitulo</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <img class="example-image" src="<?php echo $baseUrl?>/images/photos/evento-thmb.jpg" alt=""/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>