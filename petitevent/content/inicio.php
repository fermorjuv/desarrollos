<section id="container">
    <div class="wrap-container clearfix">
        <div id="main-content">
            <div class="content-box wrap-content zerogrid ">
                <article class="background-gray">
                    <div class="art-content">
                        <div class="row">
                            <div class="col-1-2">
                                <div class="wrap-col">
                                    <div>
                                        <img src="<?php echo $baseUrl?>/images/tarta.jpg">
                                    </div>
                                </div>
                            </div>
                            <div class="col-1-2">
                                <div class="wrap-col">
                                    <div class="row">
                                        <div class="t-center" style="padding-top: 40px;">
                                            <div class="header">
                                                <h2>Bienvenido a <span class="color-red">Petit Event</span></h2>
                                            </div>
                                            <p>Petit Event nace con la idea de introducir un nuevo concepto de catering en Baleares, m&aacute;s fresco, m&aacute;s divertido y relajado.</p>
                                            <p>No somos un buffet libre, pero s&iacute; queremos que cada comensal escoja lo que le apetece cada momento y vaya degustando mientras nuestros parrilleros le preparan el corte de su preferencia.</p>
                                            <p>Nos podemos adaptar a su necesidad, que comienza desde el mobiliario pasando por nuestra parrilla, nuestra barra de ensaladas y ¿por qu&eacute; no? algo dulce tambi&eacute;n. Pero si lo que busca es ir un poco m&aacute;s all&aacute; disponemos de un servicio de pi&ntilde;atas tem&aacute;ticas y todo tipo de decoraciones, como centros de mesa, iluminaci&oacute;n y nuestras divertidas paredes de photocall.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="background-white">
                    <div class="row wrap-box"><!--Start Box-->
                        <div class="col-1-2 f-right">
                            <div class="wrap-col">
                                <div style="padding-top: 60px;">
                                    <center><img src="<?php echo $baseUrl?>/images/celebracion.jpg"></center>
                                </div>
                            </div>
                        </div>
                        <div class="col-1-2">
                            <div class="wrap-col">
                                <div class="row">
                                    <div class="t-center" style="padding-top: 30px;">
                                        <div class="header">
                                            <h2>Disfrute de su fiesta</h2>
                                        </div>
                                        <p>Queremos que su &uacute;nica preocupaci&oacute;n en ese d&iacute;a tan especial sea c&oacute;mo se va a vestir; nosotros le tendremos todo a punto para que llegue dispuesto a disfrutar del tan esperado evento. Tenga la seguridad que cuidaremos de cada detalle para hacer de su fiesta o reuni&oacute;n algo inolvidable.</p>
                                        <p>No dude que le dejaremos a usted y sus invitados un buen sabor de boca. P&oacute;nganse en contacto con nosotros y h&aacute;ganos saber sus preferencias. Estamos seguros que podremos hacer de su evento algo inolvidable.</p><br/>
                                        <a href="<?php echo $baseUrl?>/contacto" class="button" >Cont&aacute;ctese con nosotros</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>