<header class="zerogrid">
    <div class="headertop">
        <ul class="social-buttons">
            <li class="facebook-btn"><div class="fb-like" data-href="https://www.facebook.com/PetitEvent" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div></li>
            <li class="twitter-btn"><a class="twitter-follow-button" href="https://twitter.com/petiteventok" data-size="Default" data-show-screen-name="false" data-show-count="false"><?php echo $translate->_('Follow') ?></a></li>
            <li class="gplus-btn"><div class="g-plusone" data-size="medium" data-annotation="none" data-href="https://plus.google.com/114947763066099956186/"></div></li>
        </ul>
        <ul class="language-buttons">
            <?php if ($translate->getLocale() != 'es') { ?><li><a href="<?php echo $baseUrl?>/<?php echo $module.'/es' ?>">Espa&ntilde;ol</a></li><?php } ?>
            <?php if ($translate->getLocale() != 'ca') { ?><li><a href="<?php echo $baseUrl?>/<?php echo $module.'/ca' ?>">Catal&agrave;</a></li><?php } ?>
            <?php if ($translate->getLocale() != 'en') { ?><li><a href="<?php echo $baseUrl?>/<?php echo $module.'/en' ?>">English</a></li><?php } ?>
        </ul>
    </div>
    <div class="logo">
        <hr class="line-1up">
        <a href="<?php echo $baseUrl?>/">Petit Event</a>
        <span><?php echo $translate->_('Catering & Events') ?></span>
        <hr class="line-1dwn">
    </div>
    <div id='cssmenu' class="align-center">
        <ul>
            <li<?php echo ($module == 'inicio') ? ' class="active"' : ''; ?>><a href='<?php echo $baseUrl?>/inicio'><span><?php echo $translate->_('Home') ?></span></a></li>
            <li<?php echo ($module == 'nosotros') ? ' class="active"' : ''; ?>><a href='<?php echo $baseUrl?>/nosotros'><span><?php echo $translate->_('About us') ?></span></a></li>
            <li<?php echo ($module == 'menu') ? ' class="active"' : ''; ?>><a href='<?php echo $baseUrl?>/menu'><span><?php echo $translate->_('Menu') ?></span></a></li>
            <li<?php echo ($module == 'servicios') ? ' class="active"' : ''; ?>><a href='<?php echo $baseUrl?>/servicios'><span><?php echo $translate->_('Services') ?></span></a></li>
            <li<?php echo ($module == 'fotos') ? ' class="active"' : ''; ?>><a href='<?php echo $baseUrl?>/fotos'><span><?php echo $translate->_('Photos') ?></span></a></li>
            <li<?php echo ($module == 'contacto') ? ' class="active last"' : ' class="last"'; ?>><a href='<?php echo $baseUrl?>/contacto'><span><?php echo $translate->_('Contact') ?></span></a></li>
        </ul>
    </div>
</header>