<section id="container">
    <div class="wrap-container clearfix">
        <div id="main-content">
            <div class="wrap-content zerogrid ">
                <article class="background-gray">
                    <div class="art-header">
                        <hr class="line-2">
                        <h2><?php echo $translate->_('Our menu') ?></h2>
                    </div>
                    <div class="art-content">
                        <div class="row">
                            <div class="col-1-4">
                                <div class="wrap-col">
                                    <div class="item-container">
                                        <img class="example-image" src="<?php echo $baseUrl?>/images/mallorquina.jpg" alt=""/>
                                    </div>
                                    <div class="header">
                                        <h2 class="item-header">Torrada mallorquina</h2>
                                    </div>
                                    <ul class="item-list">
                                        <li>Pollo</li>
                                        <li>Aguja de cerdo</li>
                                        <li>Panceta</li>
                                        <li>Chistorra</li>
                                        <li>Butifarr&oacute;n</li>
                                    </ul>
                                    <span class="item-small-text">Acompañado de salsa alioli y pan.</span>
                                </div>
                            </div>
                            <div class="col-1-4">
                                <div class="wrap-col">
                                    <div class="item-container">
                                        <img class="example-image" src="<?php echo $baseUrl?>/images/argentina.jpg" alt=""/>
                                    </div>
                                    <div class="header">
                                        <h2 class="item-header">Asado argentino</h2>
                                    </div>
                                    <ul class="item-list">
                                        <li>Chorizo criollo</li>
                                        <li>Morcilla</li>
                                        <li>Vac&iacute;o (ternera sin hueso)</li>
                                        <li>Asado (ternera con hueso)</li>
                                        <li>Pollo al limón</li>
                                    </ul>
                                    <span class="item-small-text">Acompañado de salsas chimichurri, criolla y barbacoa y pan.</span>
                                </div>
                            </div>
                            <div class="col-1-4">
                                <div class="wrap-col">
                                    <div class="item-container">
                                        <img class="example-image" src="<?php echo $baseUrl?>/images/mixta.jpg" alt=""/>
                                    </div>
                                    <div class="header">
                                        <h2 class="item-header">Barbacoa mixta</h2>
                                    </div>
                                    <ul class="item-list">
                                        <li>Entrecot de ternera</li>
                                        <li>Pollo</li>
                                        <li>Pluma ib&eacute;rica</li>
                                        <li>Panceta</li>
                                        <li>Chistorra</li>
                                        <li>Butifarr&oacute;n</li>
                                    </ul>
                                    <span class="item-small-text">Acompañado de salsas alioli y quemada y pan.</span>
                                </div>
                            </div>
                            <div class="col-1-4">
                                <div class="wrap-col">
                                    <div class="item-container">
                                        <img class="example-image" src="<?php echo $baseUrl?>/images/deluxe.jpg" alt=""/>
                                    </div>
                                    <div class="header">
                                        <h2 class="item-header">Barbacoa deluxe</h2>
                                    </div>
                                    <ul class="item-list">
                                        <li>Entrecot de ternera</li>
                                        <li>Pollo marinado</li>
                                        <li>Chuletas de cordero</li>
                                        <li>Pluma ib&eacute;rica</li>
                                    </ul>
                                    <span class="item-small-text">Acompañado de salsas alioli, quemada y especial y pan.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="background-white">
                    <div class="art-header">
                        <hr class="line-2">
                        <h2>Pasi&oacute;n por la carne</h2>
                    </div>
                    <div class="art-content">
                        <strong>Sabemos que el secreto de una buena carne a las brasas reside en buena parte en usar siempre materiales de primera calidad, adem&aacute;s de en dedicarle el tiempo necesario para alcanzar el punto ideal.</strong>
                        <div class="row">
                            <div class="col-1-2">
                                <div class="wrap-col post">
                                    <img src="<?php echo $baseUrl?>/images/brasas.jpg" alt=""/>
                                    <h3>El fuego</h3>
                                    <p>La base de toda comida a la parrilla es un buen fuego. Es por eso que en Petit Event simpre usamos carb&oacute;n de la mejor calidad.</p>
                                </div>
                            </div>
                            <div class="col-1-2">
                                <div class="wrap-col post">
                                    <img src="<?php echo $baseUrl?>/images/reloj.jpg" alt=""/>
                                    <h3>El tiempo</h3>
                                    <p>Dedicar el tiempo justo a cada corte de carne es esencial para que cada plato salga en el momento justo y con la cocci&oacute;n adecuada.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>