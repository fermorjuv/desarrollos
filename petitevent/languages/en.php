<?php
return array(
    'Catering & Events' => 'Catering & Events',
    'Home'              => 'Home',
    'About us'          => 'About us',
    'Menu'              => 'Menu',
    'Services'          => 'Services',
    'Photos'            => 'Photos',
    'Contact'           => 'Contact',
    'Follow'            => 'Follow',

    'About us'  => 'About us',

    'Our menu' => 'Our menu',

    'Our services' => 'Our services',

    'Photo album' => 'Photo album',

    'Name'              => 'Name',
    'Email'             => 'Email',
    'Message'           => 'Message',
    'Send'              => 'Send',
    'This filed is required'        => 'This filed is required',
    'The email format is invalid'   => 'The email format is invalid',
    'An error has occurred. Please, try again'  => 'An error has occurred. Please, try again',
    'Your email was sent successfully'          => 'Your email was sent successfully',

    'Coming soon...' => 'Coming soon...',
);