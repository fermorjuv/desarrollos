<?php
return array(
    'Catering & Events' => 'Catering y Eventos',
    'Home'              => 'Inicio',
    'About us'          => 'Nosotros',
    'Menu'              => 'Men&uacute;',
    'Services'          => 'Servicios',
    'Photos'            => 'Fotos',
    'Contact'           => 'Contacto',
    'Follow'            => 'Seguir',

    'About us'  => 'Sobre nosotros',

    'Our menu' => 'Nuestro men&uacute;',

    'Our services' => 'Nuestros servicios',

    'Photo album' => '&Aacute;lbum fotogr&aacute;fico',

    'Name'              => 'Nombre',
    'Email'             => 'Email',
    'Message'           => 'Mensaje',
    'Send'              => 'Enviar',
    'This filed is required'        => 'Este campo es requerido',
    'The email format is invalid'   => 'El formato de email es inválido',
    'An error has occurred. Please, try again'  => 'Ha ocurrido un error. Por favor, int&eacute;ntelo de nuevo',
    'Your email was sent successfully'          => 'Se ha enviado su mensaje correctamente',

    'Coming soon...' => 'Pr&oacute;ximamente...' ,
);