<?php
return array(
    'Catering & Events' => 'Catering i Esdeveniments',
    'Home'              => 'Inici',
    'About us'          => 'Nosaltres',
    'Menu'              => 'Men&uacute;',
    'Services'          => 'Serveis',
    'Photos'            => 'Photos',
    'Contact'           => 'Contacte',
    'Follow'            => 'Seguir',

    'About us'  => 'Sobre nosaltres',

    'Our menu' => 'El nostre men&uacute;',

    'Our services' => 'Els nostres serveis',

    'Photo album' => '&Agrave;lbum fotogr&agrave;fic',

    'Name'              => 'Nom',
    'Email'             => 'Correu electr&ograve;nic',
    'Message'           => 'Missatge',
    'Send'              => 'Enviar',
    'This filed is required'        => 'Aquest camp és requerit',
    'The email format is invalid'   => 'El format de email no és vàlid',
    'An error has occurred. Please, try again'  => 'Hi ha hagut un error. Intenteu-ho de nou, si us plau',
    'Your email was sent successfully'          => "S'ha enviat el missatge correctament",

    'Coming soon...' => 'Pr&ograve;ximament...' ,

);