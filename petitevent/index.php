<?php
session_start();

$baseUrl = 'http://localhost/petitevent';

require ('Translate.php'); 
$translate = new Translate();
$translate->setLocale('es');

if (!isset($_SESSION['lang'])) {
    $_SESSION['lang'] = $translate->getLocale();
} else {
    $translate->setAutomatic(false);
}
if (isset($_GET['lang'])) {
    $translate->setAutomatic(false);
    $_SESSION['lang'] = $_GET['lang'];
}
$translate->setLocale($_SESSION['lang']);

$module = empty($_GET['mod']) ? 'inicio' : $_GET['mod'] ;
$modules = array(
    'inicio'    => array('title' => '', 'description' => $translate->_('Catering & Events')),
    'nosotros'  => array('title' => ' - '.$translate->_('About us'), 'description' => ''),
    'menu'      => array('title' => ' - '.$translate->_('Menu'), 'description' => ''),
    'servicios' => array('title' => ' - '.$translate->_('Services'), 'description' => ''),
    'fotos'     => array('title' => ' - '.$translate->_('Photos'), 'description' => ''),
    'contacto'  => array('title' => ' - '.$translate->_('Contact'), 'description' => ''),
);
if (!in_array($module, array_keys($modules))) {
    header('Location: not-found.php');
}
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="<?php echo $translate->getLocale(); ?>"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="<?php echo $translate->getLocale(); ?>"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="<?php echo $translate->getLocale(); ?>"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="<?php echo $translate->getLocale(); ?>"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs
  ================================================== -->
    <title>Petit Event<?php echo $modules[$module]['title'] ?></title>
    <meta name="description" content="<?php echo $modules[$module]['description'] ?>">
    <meta charset="utf-8">

    <?php include('include/top.php'); ?>
    <?php @include('include/top-'.$module.'.php'); ?>

</head>

<body>
    <?php //include_once("include/analytics.php") ?>
    <?php include_once("include/facebook.php") ?>
    <div class="wrap-body">
        <?php include('content/_header.php'); ?>
        <!--////////////////////////////////////Container-->
        <?php include('content/'.$module.'.php'); ?>
        <!--////////////////////////////////////Footer-->
        <?php include('content/_footer.php'); ?>
        <?php include('include/bottom.php'); ?>
        <?php @include('include/bottom-'.$module.'.php'); ?>
    </div>
</body>
</html>