<?php
require_once('config.php');
?>
<!doctype html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, maximum-scale=1">
<meta name="robots" content="index, follow" />
<meta name="description" content="Curso Perfeccionamiento Mandatario Automotor. ECAP es un instituto de car&aacute;cter profesional que brinda cursos orientados a todo tipo de personas para oferecerles una r&aacute;pida salida laboral" />
<title>ECAP - Curso Perfeccionamiento Mandatario Automotor</title>
<link rel="icon" href="img/favicon.png" type="image/png">
<link href="css/all.css" rel="stylesheet" type="text/css" />
<link href="https://plus.google.com/+EcapArCursos" rel="publisher" />
 
<!--[if lt IE 9]>
    <script src="js/respond-1.1.0.min.js"></script>
    <script src="js/html5shiv.js"></script>
    <script src="js/html5element.js"></script>
<![endif]-->
 
</head>
<body>
<div id="hiddenTitle"><h1>ECAP - Escuela de Capacitaci&oacute;n Profesional</h1></div>
<!--Header_section-->
<?php include ('header-thin.php'); ?>
<!--Header_section--> 

<!--Hero_Section-->
<section id="hero_section" class="top_cont_outer">
  <div class="hero_wrapper">
    <div class="container">
      <div class="hero_section">
        <div class="row">
          <div>
            <div class="top_left_cont">
              <ul class="bxslider">
                <li><img src="img/perfeccionamiento-mandatario-automotor.png" alt="Curso Perfeccionamiento Mandatario Automotor" title="Curso Perfeccionamiento Mandatario Automotor" /></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Hero_Section--> 

<section id="internal">
<div class="inner_wrapper">
  <div class="container">
    <h2>Curso Perfeccionamiento Mandatario Automotor</h2>
    <div class="inner_section">
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <h3>Objetivos</h3><br/>
            <p>Actualizar y perfeccionar a los mandatarios de acuerdo a la normativa vigente.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Certificaciones obtenidas</h3><br/>
            <p>Diploma de asistencia y de aprobaci&oacute;n otorgado por ECAP.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Informaci&oacute;n general</h3><br/>
            <p><span class="bold">Duraci&oacute;n:</span> 12 horas.</p>
            <p><span class="bold">Precio:</span> $690 (incluyendo todos los materiales necesarios).</p>
            <p><span class="bold">Modalidad:</span> presencial.</p>
            <p><span class="bold">Condiciones de inscripci&oacute;n:</span></p>
            <ul class="listado">
              <li>Ser Mandatario Nacional.</li>
              <li>Abonar el curso a trav&eacute;s de cualquiera de nuestros medios de pago (efectivo, a trav&eacute;s de dep&oacute;sito o transferencia bancaria y/o cualquiera de los medios de pago habilitados a trav&eacute;s de Mercado Pago).</li>
            </ul>
            <p><span class="bold">Documentaci&oacute;n a presentar:</span></p>
            <ul class="listado">
              <li>DNI (Original y dos fotocopias).</li>
              <li>Ficha de inscripci&oacute;n.</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Programa</h3><br/>
            <ul class="listado">
              <li>Patente &Uacute;nica del Mercosur.</li>
              <li>Rentas.</li>
              <li>Prenda (Enajenaci&oacute;n, Prenda anticipada y cancelaci&oacute;n).</li>
              <li>Leasing.</li>
              <li>CETA y Licitud de Fondos.</li>
              <li>Actualizaci&oacute;n al nuevo C&oacute;digo Civil.</li>
              <li>Permisos de manejo y presentaci&oacute;n de poderes en municipios.</li>
              <li>Facturaci&oacute;n de 0 Km.</li>
              <li>C&aacute;lculo de costo del tr&aacute;mite y honorarios.</li>
              <li>Resoluci&oacute;n de dudas del alumnado.</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>&nbsp;</h3><br/>
            <p><span class="bold">Lugar de cursada:</span> Lavalle 648 5&deg; piso, Capital Federal.</p>
            <p><span class="bold">D&iacute;as y horario del curso:</span> Mi&eacute;rcoles de 18 a 22 hs.</p>
            <ul class="listado">
              <li>Clase 1: mi&eacute;rcoles 4 de noviembre de 2015</li>
              <li>Clase 2: mi&eacute;rcoles 11 de noviembre de 2015</li>
              <li>Clase 3: mi&eacute;rcoles 18 de noviembre de 2015</li>
            </ul>
            <br /><br />
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br /><br />
            <div id="inscriptionDiv" class="float-left"><a id="inscriptionBtn" class='contact_btn'>Inscribirse</a></div>
            <div id="inscriptionForm" class="float-left">
              <p>Por favor, introduzca lo siguientes datos para poder continuar con el proceso de inscripci&oacute;n</p>
              <form name="inscriptionForm" id="inscriptionForm" action="" method="post">
                <input id="inscriptionName" class="input-text" type="text" name="" value="Nombre completo" defaultValue="Nombre completo" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                <input id="inscriptionEmail" class="input-text" type="text" name="" value="E-mail" defaultValue="E-mail" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                <input id="inscriptionReference" class="input-text" type="text" name="" value="C&oacute;mo nos conoci&oacute;" defaultValue="C&oacute;mo nos conoci&oacute;" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                <input type="hidden" id="inscriptionCourse" value="<?php echo mycrypt('7')?>" />
                <a id="sendInscriptionBtn" class="contact_btn">Continuar</a>
                <p id="inscriptionError" class="contact-error"></p>
              </form>
            </div>
            <br/>
          </div>
        </div>  
      </div>
    </div>
    </div>
  </div> 
</div>
</section>

<?php include ('footer.php'); ?>

<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-all.js"></script>
<script type="text/javascript" src="js/inscription.js"></script>
<script type="text/javascript" src="js/analytics.js"></script>

</body>
</html>