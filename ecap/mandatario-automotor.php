<?php
require_once('config.php');
?>
<!doctype html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, maximum-scale=1">
<meta name="robots" content="index, follow" />
<meta name="description" content="Curso Mandatario Automotor. ECAP es un instituto de car&aacute;cter profesional que brinda cursos orientados a todo tipo de personas para oferecerles una r&aacute;pida salida laboral" />
<title>ECAP - Curso Mandatario Automotor</title>
<link rel="icon" href="img/favicon.png" type="image/png">
<link href="css/all.css" rel="stylesheet" type="text/css" />
<link href="https://plus.google.com/+EcapArCursos" rel="publisher" />
 
<!--[if lt IE 9]>
    <script src="js/respond-1.1.0.min.js"></script>
    <script src="js/html5shiv.js"></script>
    <script src="js/html5element.js"></script>
<![endif]-->
 
</head>
<body>
<div id="hiddenTitle"><h1>ECAP - Escuela de Capacitaci&oacute;n Profesional</h1></div>
<!--Header_section-->
<?php include ('header-thin.php'); ?>
<!--Header_section--> 

<!--Hero_Section-->
<section id="hero_section" class="top_cont_outer">
  <div class="hero_wrapper">
    <div class="container">
      <div class="hero_section">
        <div class="row">
          <div>
            <div class="top_left_cont">
              <ul class="bxslider">
                <li><img src="img/mandatario-automotor.png" alt="Curso Mandatario Automotor" title="Curso Mandatario Automotor" /></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Hero_Section--> 

<section id="internal">
<div class="inner_wrapper">
  <div class="container">
    <h2>Curso de Mandatario Automotor</h2>
    <div class="inner_section">
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <h3>Introducci&oacute;n</h3><br/>
            <p>El hoy "Mandatario Registral de Automotores y Cr&eacute;ditos Prendarios", es la evoluci&oacute;n profesional del tradicionalmente conocido en el medio popular como "Gestor de Automotores". Este curso oficial es dictado por ECAP - Fundaci&oacute;n Sol de Mayo, de manera intensiva y con una modalidad presencial, autorizada en forma directa por la DNRPA (Direcci&oacute;n Nacional del Registro la Propiedad del Automotor y Cr&eacute;ditos Prendarios de Argentina), en clases te&oacute;rico-pr&aacute;cticas, para formar &iacute;ntegramente al futuro profesional.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Objetivos</h3><br/>
            <p>Formar a los futuros Mandatarios brindando una s&oacute;lida formaci&oacute;n jur&iacute;dica y una muy importante capacitaci&oacute;n pr&aacute;ctica profesional que le permite al egresado una eficaz y r&aacute;pida salida laboral.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Certificaciones obtenidas</h3><br/>
            <p>Matr&iacute;cula Nacional de Mandatario DNRPA.</p>
            <p>Diploma de asistencia y de aprobaci&oacute;n otorgado por ECAP - Fund. Sol de Mayo.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Informaci&oacute;n general</h3><br/>
            <p><span class="bold">Duraci&oacute;n:</span> 96 horas.</p>
            <p><span class="bold">Precio:</span> $4.990.</p>
            <p><span class="bold">Modalidad:</span> presencial.</p>
            <p><span class="bold">Condiciones de inscripci&oacute;n:</span></p>
            <ul class="listado">
              <li>Ser mayor de edad o menor emancipado.</li>
              <li>No contar con antecedentes penales.</li>
              <li>No ser  funcionario y/o  empleado de la Direcci&oacute;n Nacional, encargados de Registro y/o  empleados de estos  y no pertenecer a las personas que presten servicios en la Direcci&oacute;n Nacional sin relaci&oacute;n de dependencia. Esta incompatibilidad ser&aacute; extensiva a los familiares hasta el segundo grado de consanguinidad y al c&oacute;nyuge.</li>
              <li>Completar la ficha de inscripci&oacute;n.</li>
              <li>Abonar el curso a trav&eacute;s de cualquiera de nuestros medios de pago (efectivo a trav&eacute;s de dep&oacute;sito o transferencia bancaria y/o cualquiera de los medios de pago habilitados a trav&eacute;s de Mercado Pago).</li>
            </ul>
            <p><span class="bold">Documentaci&oacute;n a presentar:</span></p>
            <ul class="listado">
              <li>DNI (Original y dos fotocopias).</li>
              <li>CUIL O CUIT (seg&uacute;n corresponda)</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Programa</h3><br/>
            <p><a target="_blank" href="programas/mandatario-automotor.pdf"><span><i class="fa fa-2x fa-file-pdf-o"></i></span></a></p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Lugar de Cursada, d&iacute;as y horarios</h3><br/>
            <p><span class="bold">Julio 2016</span></p>
            <p><span class="bold">Lugar de cursada:</span> Lavalle 648 5&deg; piso, Capital Federal.</p>
            <p><span class="bold">D&iacute;as y horario:</span> S&aacute;bados de 10 a 15 hs.</p>
            <ul class="listado">
              <li>Clase 1: 16 de julio de 2016</li>
              <li>Clase 2: 23 de julio de 2016</li>
              <li>Clase 3: 30 de julio de 2016</li>
              <li>Clase 4: 06 de agosto de 2016</li>
              <li>Clase 5: 13 de agosto de 2016</li>
              <li>Clase 6: 20 de agosto de 2016</li>
              <li>Clase 7: 27 de agosto de 2016</li>
              <li>Clase 8: 03 de septiembre de 2016</li>
              <li>Clase 9: 10 de septiembre de 2016</li>
              <li>Clase 10: 17 de septiembre de 2016</li>
              <li>Clase 11: 25 de septiembre de 2016</li>
              <li>Clase 12: 01 de octubre de 2016</li>
              <li>Examen final: 08 de octubre de 2016</li>
            </ul>
            <br /><br />
          </div>
        </div>
        <div class="col-lg-4">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>&nbsp;</h3><br/>
            <p><span class="bold">Agosto 2016</span></p>
            <p><span class="bold">Lugar de cursada:</span> Lavalle 648 5&deg; piso, Capital Federal.</p>
            <p><span class="bold">D&iacute;as y horario:</span> Lunes y Mi&eacute;rcoles de 18 a 22 hs.</p>
            <ul class="listado">
              <li>Clase 1: 10 de Agosto de 2016</li>
              <li>Clase 2: 17 de Agosto de 2016</li>
              <li>Clase 3: 22 de Agosto de 2016</li>
              <li>Clase 4: 24 de Agosto de 2016</li>
              <li>Clase 5: 29 de Agosto de 2016</li>
              <li>Clase 6: 31 de Agosto de 2016</li>
              <li>Clase 7: 5 de Septiembre de 2016</li>
              <li>Clase 8: 7 de Septiembre de 2016</li>
              <li>Clase 9: 12 de Septiembre de 2016</li>
              <li>Clase 10: 14 de Septiembre de 2016</li>
              <li>Clase 11: 19 de Septiembre de 2016</li>
              <li>Clase 12: 21 de Septiembre de 2016</li>
              <li>Clase 13: 26 de Septiembre de 2016</li>
              <li>Clase 14: 28 de Septiembre de 2016</li>
              <li>Clase 15: 03 de Octubre de 2016</li>
              <li>Examen final: 05 de Octubre de 2016</li>
            </ul>
            <br /><br />
          </div>
        </div>
        <div class="col-lg-4">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>&nbsp;</h3><br/>
            <p><span class="bold">Septiembre 2016</span></p>
            <p><span class="bold">Lugar de cursada:</span> Lavalle 648 5&deg; piso, Capital Federal.</p>
            <p><span class="bold">D&iacute;as y horario:</span> Martes y Jueves de 18 a 22 hs.</p>
            <ul class="listado">
              <li>Clase 1: 13 de Septiembre de 2016</li>
              <li>Clase 2: 15 de Septiembre de 2016</li>
              <li>Clase 3: 20 de Septiembre de 2016</li>
              <li>Clase 4: 22 de Septiembre de 2016</li>
              <li>Clase 5: 27 de Septiembre de 2016</li>
              <li>Clase 6: 29 de Septiembre de 2016</li>
              <li>Clase 7: 04 de Octubre de 2016</li>
              <li>Clase 8: 06 de Octubre de 2016</li>
              <li>Clase 9: 11 de Octubre de 2016</li>
              <li>Clase 10: 13 de Octubre de 2016</li>
              <li>Clase 11: 18 de Octubre de 2016</li>
              <li>Clase 12: 20 de Octubre de 2016</li>
              <li>Clase 13: 25 de Octubre de 2016</li>
              <li>Clase 14: 27 de Octubre de 2016</li>
              <li>Clase 15: 01 de Noviembre de 2016</li>
              <li>Examen final: 03 de Noviembre de 2016</li>
            </ul>
            <br /><br />
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br /><br />
            <div id="inscriptionDiv" class="float-left"><a id="inscriptionBtn" class='contact_btn'>Inscribirse</a></div>
              <div id="inscriptionForm" class="float-left">
                <p>Por favor, introduzca lo siguientes datos para poder continuar con el proceso de inscripci&oacute;n</p>
                <form name="inscriptionForm" id="inscriptionForm" action="" method="post">
                  <input id="inscriptionName" class="input-text" type="text" name="" value="Nombre completo" defaultValue="Nombre completo" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                  <input id="inscriptionEmail" class="input-text" type="text" name="" value="E-mail" defaultValue="E-mail" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                  <input id="inscriptionReference" class="input-text" type="text" name="" value="C&oacute;mo nos conoci&oacute;" defaultValue="C&oacute;mo nos conoci&oacute;" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                  <select id="inscriptionCourse" class="input-text" name="">
                    <option value="<?php echo mycrypt('18')?>">Julio: S&aacute;bados de 10 a 15 hs.</option>
                    <option value="<?php echo mycrypt('25')?>">Agosto: Lunes y Mi&eacute;rcoles de 18 a 22 hs.</option>
                    <option value="<?php echo mycrypt('26')?>">Septiembre: Martes y Jueves de 18 a 22 hs.</option>
                  </select>
                  <a id="sendInscriptionBtn" class="contact_btn">Continuar</a>
                  <p id="inscriptionError" class="contact-error"></p>
                </form>
              </div>
              <br/>
            </div>
          </div>  
        </div>
      </div>
    </div>
  </div> 
</div>
</section>

<?php include ('footer.php'); ?>

<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-all.js"></script>
<script type="text/javascript" src="js/inscription.js"></script>
<script type="text/javascript" src="js/analytics.js"></script>

</body>
</html>