<?php
require_once('config.php');

/*if (isset($_GET['c'])) {
  include ('sendwelcome.php');
}*/

?>
<!doctype html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, maximum-scale=1">
<meta name="robots" content="index, follow" />
<meta name="description" content="Medios de pago. ECAP es un instituto de car&aacute;cter profesional que brinda cursos orientados a todo tipo de personas para oferecerles una r&aacute;pida salida laboral" />
<title>ECAP - Medios de pago</title>
<link rel="icon" href="img/favicon.png" type="image/png">
<link href="css/all.css" rel="stylesheet" type="text/css" />
<link href="https://plus.google.com/+EcapArCursos" rel="publisher" />
 
<!--[if lt IE 9]>
    <script src="js/respond-1.1.0.min.js"></script>
    <script src="js/html5shiv.js"></script>
    <script src="js/html5element.js"></script>
<![endif]-->
 
</head>
<body>
<div id="hiddenTitle"><h1>ECAP - Escuela de Capacitaci&oacute;n Profesional</h1></div>
<!--Header_section-->
<?php include ('header-thin.php'); ?>
<!--Header_section--> 

<section id="internal">
<div class="inner_wrapper">
  <div class="container">
    <h2>Medios de pago</h2>
    <div class="inner_section delay-01s animated fadeInDown wow animated ">
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <?php if (isset($_GET['c']) && $_GET['c'] != '0') { ?>
            <h3>Gracias por inscribirse al curso!</h3>
            <p>Para finalizar con su inscripci&oacute;n podr&aacute; elegir uno de los siguientes medios de pago:</p>
            <?php } else { ?>
            <h3>Los medios de pago de los que disponemos son:</h3>
            <?php } ?>
          </div>
        </div>  
      </div>
      <div class="row borderTop payment-method-box">
        <div class=" col-lg-3 col-md-3 col-sm-3 col-xs-12 pull-left">
          <div class="payment-method">
            <img src="img/tarjeta-credito.png" alt="Tarjeta de cr&eacute;dito" title="Tarjeta de cr&eacute;dito" />
          </div>
        </div>
        <div class=" col-lg-8 col-md-8 col-sm-8 col-xs-12 pull-right ">
          <div>
            <h4>Tarjeta de cr&eacute;dito</h4>
              <?php if (isset($_GET['c']) && $_GET['c'] != '0') { ?><a href="<?php echo $baseUrl?>/admin/api/inscription/<?php echo $_GET['c'] ?>" class="contact_btn">Realizar pago</a><br><?php } ?>
              <img src="img/mercadopago.jpg" title="MercadoPago - Medios de pago" alt="MercadoPago - Medios de pago" width="468" height="60"/>
          </div>
        </div> 
      </div>
      <div class="row borderTop payment-method-box">
        <div class=" col-lg-3 col-md-3 col-sm-3 col-xs-12 pull-left">
          <div class="payment-method">
            <img src="img/transferencia-deposito.png" alt="Transferencia o dep&oacute;sito" title="Transferencia o dep&oacute;sito" />
          </div>
        </div>
        <div class=" col-lg-8 col-md-8 col-sm-8 col-xs-12 pull-right ">
          <div>
            <h4>Transferencia o dep&oacute;sito bancario</h4>
            <p>Consultanos a <a href="mailto:info@ecap.com.ar">info@ecap.com.ar</a> nuestros datos bancarios para realizar la transferencia o el dep&oacute;sito.</p>
            <p>En el caso que elijas este medio de pago por favor informanos lo antes posible, indicando tu nombre, curso y la fecha del pago.</p>
          </div>
        </div> 
      </div>
      <div class="row borderTop payment-method-box">
        <div class=" col-lg-3 col-md-3 col-sm-3 col-xs-12 pull-left">
          <div class="payment-method">
            <img src="img/efectivo.png" alt="Efectivo" title="Efectivo" />
          </div>
        </div>
        <div class=" col-lg-8 col-md-8 col-sm-8 col-xs-12 pull-right ">
          <div>
            <h4>Pago en cuotas en efectivo (Efectivo, Rapipago o Pagofácil)</h4>
            <p>Consult&aacute; por pagos en efectivo con nuestro sistema de cuotas sin inter&eacute;s.</p>
            <p>Escribinos a <a href="mailto:info@ecap.com.ar">info@ecap.com.ar</a> o llamanos a nuestro tel&eacute;fono <a href="tel:01136351342">(011) 3635-1342</a> y consult&aacute; sin compromiso.</p>
          </div>
        </div> 
      </div>
    </div>
  </div> 
</div>
</section>

<?php include ('footer.php'); ?>

<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-all.js"></script>
<script type="text/javascript" src="js/analytics.js"></script>

<!-- Google Code for conversion_ecap Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 878852043;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "9bInCNv9j2gQy--IowM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/878852043/?label=9bInCNv9j2gQy--IowM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</body>
</html>