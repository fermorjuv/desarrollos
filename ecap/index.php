<?php 
require_once('config.php');
?>
<!doctype html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, maximum-scale=1">
<meta name="robots" content="index, follow" />
<meta name="description" content="ECAP es un instituto de car&aacute;cter profesional que brinda cursos orientados a todo tipo de personas para oferecerles una r&aacute;pida salida laboral" />
<title>ECAP - Escuela de Capacitaci&oacute;n Profesional</title>
<link rel="icon" href="img/favicon.png" type="image/png">
<link href="css/all.css" rel="stylesheet" type="text/css" />
<link href="https://plus.google.com/+EcapArCursos" rel="publisher" />

<!--[if lt IE 9]>
    <script src="js/respond-1.1.0.min.js"></script>
    <script src="js/html5shiv.js"></script>
    <script src="js/html5element.js"></script>
<![endif]-->
 
</head>
<body>
<div id="hiddenTitle"><h1>ECAP - Escuela de Capacitaci&oacute;n Profesional</h1></div>
<!--Header_section-->
<header id="header_wrapper">
<div id="fb-root"></div>
  <div class="container">
    <div class="header_box">
      <div class="logo"><a href="#"><img src="img/logo.png" alt="ECAP" title="ECAP"></a></div>
      <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
          <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div id="main-nav" class="collapse navbar-collapse navStyle">
          <ul class="nav navbar-nav" id="mainNav">
            <li class="active"><a href="#hero_section" class="scroll-link">Inicio</a></li>
            <li><a href="#nosotros" class="scroll-link">Nosotros</a></li>
            <li><a href="#cursos" class="scroll-link">Cursos</a></li>
            <li><a href="#instalaciones" class="scroll-link">Instalaciones</a></li>
            <li><a href="#noticias" class="scroll-link">Noticias</a></li>
            <li><a href="#contacto" class="scroll-link">Contacto</a></li>
            <li class="social-first-button"><div class="fb-like" layout="button" data-width="40px"></div></li>
            <li class="pre-social-button"><a href="https://twitter.com/ecapcursos" class="twitter-follow-button" data-show-count="false" data-lang="es" data-show-screen-name="false" data-dnt="true">S</a></li>
            <li class="social-button"><div class="g-plusone" data-size="tall" data-annotation="none" data-href="https://plus.google.com/+EcapArCursos"></div></li>
          </ul>
        </div>
      </nav>
    </div>
  </div>
</header>
<!--Header_section--> 

<!--Hero_Section-->
<section id="hero_section" class="top_cont_outer">
  <div class="hero_wrapper">
    <div class="container">
      <div class="hero_section">
        <div class="row">
          <div class=" col-lg-7 col-md-7 col-sm-7 col-xs-12 pull-left">
            <div class="top_left_cont">
              <ul class="bxslider">
                <li><a href="mandatario-automotor.php"><img src="img/slider-mandatario-automotor.png" alt="Curso Mandatario Automotor" title="Curso Mandatario Automotor" /></a></li>
                <li><a href="mandatario-previsional.php"><img src="img/slider-mandatario-previsional.png" alt="Curso Asesor Previsional" title="Curso Asesor Previsional" /></a></li>
                <li><a href="secretariado-empresarial.php"><img src="img/slider-secretariado-empresarial.png" alt="Secretariado Empresarial" title="Secretariado Empresarial" /></a></li>
                <li><a href="relaciones-publicas.php"><img src="img/slider-relaciones-publicas.png" alt="Curso de Relaciones P&uacute;blicas" title="Curso de Relaciones P&uacute;blicas" /></a></li>
                <li><a href="reparacion-celulares.php"><img src="img/slider-reparacion-celulares.png" alt="Curso de Reparaci&aacute;n de Celulares" title="Curso de Reparaci&aacute;n de Celulares" /></a></li>
                <li><a href="monitoreo-cctv.php"><img src="img/slider-monitoreo-cctv.png" alt="Curso de Monitoreo CCTV" title="Curso de Monitoreo CCTV" /></a></li>
                <li><a href="monitoreo-satelital.php"><img src="img/slider-monitoreo-satelital.png" alt="Curso de Monitoreo Satelital" title="Curso de Monitoreo Satelital" /></a></li>
              </ul>
            </div>
          </div>
          <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-right">
            <div id="information-div">
              <form name="informationForm" id="informationForm" action="sendinformation.php" method="post">
                <p id="informationTitle">&iexcl;Informate ahora!</p>
                <input id="informationEmail" class="input-text" type="text" name="" value="E-mail *" defaultValue="E-mail *" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                <select id="informationCurso" class="input-text" type="text" name="" >
                  <option value="">Seleccione un curso...</option>
                  <option value="Mandatario Automotor">Curso de Mandatario Automotor</option>
                  <option value="Asesor Previsional">Curso de Asesor Previsional</option>
                  <option value="Secretariado Empresarial">Curso de Secretariado Empresarial</option>
                  <option value="Relaciones P&uacute;blicas">Curso de Relaciones P&uacute;blicas</option>
                  <option value="Reparaci&aacute;n de Celulares">Curso de Reparaci&aacute;n de Celulares</option>
                  <option value="Monitoreo CCTV">Curso de Monitoreo CCTV</option>
                  <option value="Monitoreo Satelital">Curso de Monitoreo Satelital</option>
                </select>
                <a id="informationSend" class="contact_btn">Me interesa</a>
              </form>
            </div>
            <div id="informationCall">
              <div id="informationCallBanner">
                Llamanos al:<br /><a href="tel:01136351342">(011) 4262-3995</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Hero_Section--> 


<!--Aboutus-->
<section id="nosotros">
<div class="inner_wrapper">
  <div class="container">
    <h2>ECAP - ESCUELA DE CAPACITACI&Oacute;N PROFESIONAL</h2>
    <div class="inner_section">
      <div class="row">
        <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-right"><img src="img/about-img.jpg" class="img-circle delay-03s animated wow zoomIn" alt="Cursos ECAP" title="Cursos ECAP"></div>
        <div class=" col-lg-7 col-md-7 col-sm-7 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <h3>Sobre nosotros</h3><br/> 
            <p>Nuestra misi&oacute;n es formar y capacitar profesionales que puedan desempe&ntilde;arse tanto de forma independiente como en relaci&oacute;n de dependencia en cualquier parte del pa&iacute;s. Nuestros cursos est&aacute;n dirigidos a hombres y mujeres que no puedan cursar una carrera de nivel superior, pero que reconozcan en ellos la necesidad de capacitarse para tener una profesi&oacute;n que les permita manejar sus tiempos y les brinden una r&aacute;pida y lucrativa salida laboral. Los mismos son dictados de manera intensiva y los &uacute;nicos requisitos excluyentes para inscribirse son ser mayor de 18 a&ntilde;os y poseer estudios primarios completos.</p>
          </div>
          <div class="work_bottom"> <span>Si quiere saber m&aacute;s sobre nosotros...</span> <a href="#contacto" class="contact_btn">Cont&aacute;ctenos</a> </div>       
        </div>
      </div>
    </div>
  </div> 
  </div>
</section>
<!--Aboutus--> 


<!--Service-->
<section  id="cursos" class="top_cont_outer">
  <div class="container">
    <h2>CURSOS</h2>
    <div class="accordion delay-03s animated wow zoomIn">
      <dl>
        <dt>
          <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">MANDATARIO AUTOMOTOR</a>
        </dt>
        <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
          <p>El hoy "Mandatario Registral de Automotores y Cr&eacute;ditos Prendarios", es la evoluci&oacute;n profesional del tradicionalmente conocido en el medio popular como "Gestor de Automotores".<br /><br /><a href="mandatario-automotor.php" class="contact_btn">IR AL CURSO</a></p>
        </dd>
        <dt>
          <a href="#accordion2" aria-expanded="false" aria-controls="accordion2" class="accordion-title accordionTitle js-accordionTrigger">ASESOR PREVISIONAL</a>
        </dt>
        <dd class="accordion-content accordionItem is-collapsed" id="accordion2" aria-hidden="true">
          <p>El alumno podr&aacute; realizar profesionalmente tr&aacute;mites referentes a gesti&oacute;n previsional, usando las t&eacute;cnicas imprescindibles relacionadas con la funci&oacute;n, normativas y jurisprudencia.<br /><br /><a href="mandatario-previsional.php" class="contact_btn">IR AL CURSO</a></p>
        </dd>
        <dt>
          <a href="#accordion3" aria-expanded="false" aria-controls="accordion3" class="accordion-title accordionTitle js-accordionTrigger">SECRETARIADO EMPRESARIAL</a>
        </dt>
        <dd class="accordion-content accordionItem is-collapsed" id="accordion3" aria-hidden="true">
          <p>La secretaria empresarial posee un rol ligada a las relaciones humanas y p&uacute;blicas. Excelente manejo de PC, redacci&oacute;n comercial y protocolar. Manejo de agenda, coordinaci&oacute;n y participaci&oacute;n en forma directa con la parte jer&aacute;rquica de la empresa. Debe contar con el criterio suficiente para tomar decisiones en el momento oportuno. La secretaria empresarial es HOY esencial en toda aquella organizaci&oacute;n, cuyo personal jer&aacute;rquico necesite realizar relaciones p&uacute;blicas para poder desempe&ntilde;ar en la misma un crecimiento y progreso.<br /><br /><a href="secretariado-empresarial.php" class="contact_btn">IR AL CURSO</a></p>
        </dd>
        <dt>
          <a href="#accordion4" aria-expanded="false" aria-controls="accordion4" class="accordion-title accordionTitle js-accordionTrigger">RELACIONES P&Uacute;BLICAS</a>
        </dt>
        <dd class="accordion-content accordionItem is-collapsed" id="accordion4" aria-hidden="true">
          <p>El  relacionista  p&uacute;blico es el encargado de aquellas acciones destinadas a la gesti&oacute;n de la comunicaci&oacute;n entre una organizaci&oacute;n y una comunidad. Tiene como finalidad la construcci&oacute;n y el mantenimiento de una imagen positiva de la entidad a la que pertenece.<br /><br /><a href="relaciones-publicas.php" class="contact_btn">IR AL CURSO</a></p>
        </dd>
        <dt>
          <a href="#accordion5" aria-expanded="false" aria-controls="accordion5" class="accordion-title accordionTitle js-accordionTrigger">REPARACI&Oacute;N DE CELULARES</a>
        </dt>
        <dd class="accordion-content accordionItem is-collapsed" id="accordion5" aria-hidden="true">
          <p>En un mundo en donde la tecnolog&iacute;a avanza d&iacute;a a d&iacute;a, los celulares se han vuelto peque&ntilde;as computadoras de las que dependemos para comunicarnos, para trabajar, para informarnos, o simplemente para momentos de ocio. A trav&eacute;s de este curso te brindamos todos los conocimientos y herramientas, para que puedas insertarte en el mundo de la tecnolog&iacute;a celular de las marcas mas importantes y masivas, como un verdadero profesional.<br /><br /><a href="reparacion-celulares.php" class="contact_btn">IR AL CURSO</a></p>
        </dd>
        <dt>
          <a href="#accordion6" aria-expanded="false" aria-controls="accordion6" class="accordion-title accordionTitle js-accordionTrigger">MONITOREO CCTV</a>
        </dt>
        <dd class="accordion-content accordionItem is-collapsed" id="accordion6" aria-hidden="true">
          <p>La capacitaci&oacute;n busca presentar al alumno distintos aspectos del rol que ocupa un operador de monitoreo de video seguridad. Otorgar&aacute; los fundamentos de aplicabilidad de distintas t&eacute;cnicas de comunicaci&oacute;n y abordar&aacute; la relevancia del operador como un eslab&oacute;n de la cadena de seguridad que forma parte del sistema integral de seguridad de Pan American Oil. Entender&aacute; el marco regulatorio de la materia.<br /><br /><a href="monitoreo-cctv.php" class="contact_btn">IR AL CURSO</a></p>
        </dd>
        <dt>
          <a href="#accordion6" aria-expanded="false" aria-controls="accordion6" class="accordion-title accordionTitle js-accordionTrigger">MONITOREO SATELITAL</a>
        </dt>
        <dd class="accordion-content accordionItem is-collapsed" id="accordion6" aria-hidden="true">
          <p>El curso busca presentar al alumno una muestra de la gran cantidad de elementos tecnol&oacute;gicos dise&ntilde;ados para aplicaciones de rastreo satelital vehicular. Otorgar&aacute; los fundamentos de aplicabilidad de dichos elementos y permitir&aacute; que se expandan las posibilidades de aplicaci&oacute;n junto con los nuevos desarrollos del mercado. Al mismo tiempo se incorporan casos de estudio en donde los sistemas de rastreo satelital vehicular permitan al operador detectar situaciones de riesgo.<br /><br /><a href="monitoreo-satelital.php" class="contact_btn">IR AL CURSO</a></p>
        </dd>
      </dl>
    </div>
</section>
<!--Service-->


<!-- Portfolio -->
<section id="instalaciones" class="content"> 
  
  <!-- Container -->
  <div class="container portfolio_title"> 
    
    <!-- Title -->
    <div class="section-title">
      <h2>INSTALACIONES</h2>
    </div>
    <!--/Title --> 
    
  </div>
  <!-- Container -->
  
  <div class="portfolio-top"></div>
  
  <!-- Portfolio Filters -->
  <div class="portfolio">  
    
    <div id="filters" class="sixteen columns">
      <ul class="clearfix">
        <li><a id="all" href="#" data-filter="*" class="active">
          <h5>Todas</h5>
          </a></li>
        <li><a class="" href="#" data-filter=".aulas">
          <h5>Aulas</h5>
          </a></li>
        <li><a class="" href="#" data-filter=".instituto">
          <h5>Instituto</h5>
          </a></li>
      </ul>
    </div>
    <!--/Portfolio Filters --> 
    
    <!-- Portfolio Wrapper -->
    <div class="isotope fadeInLeft animated wow" style="position: relative; overflow: hidden; height: 480px;" id="portfolio_wrapper"> 
      
      <!-- Portfolio Item -->
      <div style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1); width: 337px; opacity: 1;" class="portfolio-item one-four   instituto isotope-item">
        <div class="portfolio_img"> <img src="img/instituto-1.jpg"  alt="Recepci&oacute;n" title="Recepci&oacute;n" /> </div>        
        <div class="item_overlay">
          <div class="item_info"> 
            <h4 class="project_name">Recepci&oacute;n</h4>
          </div>
        </div>
        </div>
      <!--/Portfolio Item --> 
      
      <!-- Portfolio Item-->
      <div style="position: absolute; left: 0px; top: 0px; transform: translate3d(337px, 0px, 0px) scale3d(1, 1, 1); width: 337px; opacity: 1;" class="portfolio-item one-four  aulas isotope-item">
        <div class="portfolio_img"> <img src="img/instituto-2.jpg" alt="Aulas computaci&oacute;n" title="Aulas computaci&oacute;n" /> </div>
        <div class="item_overlay">
          <div class="item_info"> 
            <h4 class="project_name">Aulas de computaci&oacute;n</h4>
          </div>
        </div>
      </div>
      <!--/Portfolio Item --> 
      
      <!-- Portfolio Item -->
      <div style="position: absolute; left: 0px; top: 0px; transform: translate3d(674px, 0px, 0px) scale3d(1, 1, 1); width: 337px; opacity: 1;" class="portfolio-item one-four  instituto  isotope-item">
        <div class="portfolio_img"> <img src="img/instituto-3.jpg" alt="Coffee break" title="Coffee break" /> </div>
        <div class="item_overlay">
          <div class="item_info"> 
            <h4 class="project_name">Coffee break</h4>
          </div>
        </div>
      </div>
      <!--/Portfolio Item--> 
      
      <!-- Portfolio Item-->
      <div style="position: absolute; left: 0px; top: 0px; transform: translate3d(1011px, 0px, 0px) scale3d(1, 1, 1); width: 337px; opacity: 1;" class="portfolio-item one-four  aulas  prototype web isotope-item">
        <div class="portfolio_img"> <img src="img/instituto-4.jpg" alt="Aulas te&oacute;ricas" title="Aulas te&oacute;ricas" /> </div>
        <div class="item_overlay">
          <div class="item_info"> 
            <h4 class="project_name">Aulas te&oacute;ricas</h4>
          </div>
        </div>
      </div>
      <!-- Portfolio Item --> 
      
    </div>
    <!--/Portfolio Wrapper --> 
    
  </div>
  <!--/Portfolio Filters -->
  
  <div class="portfolio_btm"></div>
  
  
  <div id="project_container">
    <div class="clear"></div>
    <div id="project_data"></div>
  </div>
 
  
</section>
<?php 
$content = file_get_contents('http://ecap.com.ar/blog/feed/');
$xml = new SimpleXmlElement($content);
$news = array(
    $xml->channel->item[0],
    $xml->channel->item[1],
    $xml->channel->item[2]
);
?>
<section id="noticias">
  <div class="container">
    <h2>NOTICIAS</h2>
    <h4 class="project-section-title">Directamente extra&iacute;das de nuestro blog de noticias "<a href="blog/">El Mundo del Gestor</a>"</h4>
    <div class="service_wrapper">
      <div class="row">
        <div class="col-lg-4">
          <div class="service_block">
            <div class="service_icon delay-03s animated wow zoomIn"> <span><i class="fa <?php echo formatIconNew($news[0]->category) ?>"></i></span> </div>
            <h3 class="animated fadeInUp wow"><?php echo $news[0]->title ?></h3>
            <p class="animated fadeInDown wow"><?php echo formatContentNew($news[0]->children("content", true)) ?><br /><br /><a target="_blank" href="<?php echo $news[0]->link ?>">Leer m&aacute;s</a></p>
          </div>
        </div>
        <div class="col-lg-4 borderLeft">     
          <div class="service_block">
            <div class="service_icon icon2 delay-03s animated wow zoomIn"> <span><i class="fa <?php echo formatIconNew($news[1]->category) ?>"></i></span> </div>
            <h3 class="animated fadeInUp wow"><?php echo $news[1]->title ?></h3>
            <p class="animated fadeInDown wow"><?php echo formatContentNew($news[1]->children("content", true)) ?><br /><br /><a target="_blank" href="<?php echo $news[1]->link ?>">Leer m&aacute;s</a></p>
          </div>
        </div>
        <div class="col-lg-4 borderLeft">
          <div class="service_block">
            <div class="service_icon icon3 delay-03s animated wow zoomIn"> <span><i class="fa <?php echo formatIconNew($news[2]->category) ?>"></i></span> </div>
            <h3 class="animated fadeInUp wow"><?php echo $news[2]->title ?></h3>
            <p class="animated fadeInDown wow"><?php echo formatContentNew($news[2]->children("content", true)) ?><br /><br /><a target="_blank" href="<?php echo $news[2]->link ?>">Leer m&aacute;s</a></p>
          </div>
        </div>
      </div>
     </div>
  </div>
</section>
<!--Footer-->
<?php include ('footer.php'); ?>

<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.bxslider.js"></script>
<script type="text/javascript" src="js/jquery-all.js"></script>
<script src="https://apis.google.com/js/platform.js" async defer>{lang: 'es'}</script>
<script type="text/javascript" src="js/analytics.js"></script>
</body>
</html>