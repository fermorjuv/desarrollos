<div id="fb-root"></div>
<script src="https://apis.google.com/js/platform.js" async defer>{lang: 'es'}</script>
<header id="header_wrapper">
  <div class="container">
    <div class="header_box">
      <div class="logo"><a href="index.php"><img src="img/logo.png" alt="ECAP" title="ECAP"></a></div>
      <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
          <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div id="main-nav" class="collapse navbar-collapse navStyle">
          <ul class="nav navbar-nav" id="mainNav">
            <li><a href="index.php" class="scroll-link">Inicio</a></li>
            <li><a href="index.php#aboutUs" class="scroll-link">Nosotros</a></li>
            <li><a href="index.php#service" class="scroll-link">Cursos</a></li>
            <li><a href="index.php#Portfolio" class="scroll-link">Instalaciones</a></li>
            <li><a href="index.php#clients" class="scroll-link">Convenios</a></li>
            <?php /* <li><a href="#team" class="scroll-link">Equipo</a></li> */?>
            <li><a href="#contact" class="scroll-link">Contacto</a></li>
            <li class="social-first-button"><div class="fb-like" layout="button" data-width="40px"></div></li>
            <li class="pre-social-button"><a href="https://twitter.com/ecapcursos" class="twitter-follow-button" data-show-count="false" data-lang="es" data-show-screen-name="false" data-dnt="true">S</a></li>
            <li class="social-button"><div class="g-plusone" data-size="tall" data-annotation="none" data-href="https://plus.google.com/+EcapArCursos"></div></li>
          </ul>
        </div>
      </nav>
    </div>
  </div>
</header>