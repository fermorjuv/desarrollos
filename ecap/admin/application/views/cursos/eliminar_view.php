
<!-- Navigation -->
<?php echo $this->load->view('header_view', '', true); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cursos</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Eliminar curso
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php 
                            $attributes = array("id" => "cursosEliminar", "name" => "cursosEliminar", "role" => "form");
                            echo form_open("cursos/eliminar/".$curso->idCurso, $attributes);?>
                                <input type="hidden" name="idCurso" value="<?php echo $curso->idCurso?>" />
                                <div class="form-group">
                                    <label>&iquest;Est&aacute; seguro que desea eliminar el curso "<?php echo $curso->nombre ?>"?</label>
                                </div>
                                <button type="submit" class="btn btn-danger">Eliminar</button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

<?php echo $this->load->view('footer_view', '', true); ?>

<?php echo $this->load->view('close_view', '', true); ?>
