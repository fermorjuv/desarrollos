
<!-- Navigation -->
<?php echo $this->load->view('header_view', '', true); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cursos</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Alta de curso
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <?php 
                        $attributes = array("id" => "cursosAlta", "name" => "cursosAlta", "role" => "form");
                        echo form_open("cursos/alta", $attributes);?>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input class="form-control" id="nombre" name="nombre" type="text" value="<?php echo set_value('nombre'); ?>" />
                                    <span class="text-danger"><?php echo form_error('nombre'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Fecha de inicio</label>
                                    <input class="form-control" id="fechaInicio" name="fechaInicio" type="text" value="<?php echo set_value('fechaInicio'); ?>" />
                                    <span class="text-danger"><?php echo form_error('fechaInicio'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Fecha de finalizaci&oacute;n</label>
                                    <input class="form-control" id="fechaFin" name="fechaFin" type="text" value="<?php echo set_value('fechaFin'); ?>" />
                                    <span class="text-danger"><?php echo form_error('fechaFin'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Precio</label>
                                    <input class="form-control" id="precio" name="precio" type="text" value="<?php echo set_value('precio'); ?>" />
                                    <span class="text-danger"><?php echo form_error('precio'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Descripci&oacute;n</label>
                                    <textarea class="form-control" id="descripcion" name="descripcion"><?php echo set_value('descripcion'); ?></textarea>
                                    <span class="text-danger"><?php echo form_error('descripcion'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>URL</label>
                                    <input class="form-control" id="url" name="url" type="text" value="<?php echo set_value('url'); ?>" />
                                    <span class="text-danger"><?php echo form_error('url'); ?></span>
                                </div>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

<?php echo $this->load->view('footer_view', '', true); ?>

<script>
$(document).ready(function() {
    $("#fechaInicio").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "2015:<?php echo date('Y')+1?>"
    });
    $("#fechaFin").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "2015:<?php echo date('Y')+1?>"
    });
    $.datepicker.setDefaults($.datepicker.regional['es']);
});
</script>

<?php echo $this->load->view('close_view', '', true); ?>
