
<!-- Navigation -->
<?php echo $this->load->view('header_view', '', true); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cursos</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Listado de cursos
                    <div style="float:right">
                        <a href="<?php echo site_url("cursos/alta"); ?>" class="btn btn-primary btn-xs">Alta curso</a>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Inicio</th>
                                    <th>Inicio</th>
                                    <th>Precio</th>
                                    <th>Estado</th>
                                    <th>Alumnos</th>
                                    <?php //<th>Archivos</th> ?> 
                                    <?php if ($this->session->userdata('rol') == 1) {  ?><th>Acciones</th><?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach($cursos as $curso) {
                                    $hoy = date('Y-m-d');
                                    if ($curso->fechaInicio == '') {
                                        $estado = 'Pausado';
                                    } elseif ($curso->fechaFin <= $hoy)
                                    {
                                        $estado = 'Finalizado';
                                    } elseif ($curso->fechaInicio > $hoy) {
                                        $estado = 'No iniciado';
                                    } else {
                                        $estado = 'Iniciado';
                                    }
                                ?>
                                <tr class="gradeA">
                                    <td><?php echo $curso->nombre ?></td>
                                    <td><?php echo ($curso->fechaInicio != '') ? date("d-m-Y", strtotime($curso->fechaInicio)) : ''; ?></td>
                                    <td><?php echo ($curso->fechaInicio != '') ? $curso->fechaInicio : ''; ?></td>
                                    <td><?php echo number_format($curso->precio, 2, ',', '.'); ?></td>
                                    <td><span style="color:<?php echo formatColorEstadoCurso($estado)?>"><?php echo $estado ?></span></td>
                                    <td><?php echo $curso->alumnos; if ($curso->alumnos > 0) { ?>&nbsp;<a href="<?php echo site_url("cursos/alumnosPorCurso/".$curso->idCurso); ?>" class="btn btn-primary btn-xs">Listado</a><?php } ?></td>
                                    <?php if ($this->session->userdata('rol') == 1) {  ?><td><a href="<?php echo site_url("cursos/editar/".$curso->idCurso); ?>" class="btn btn-primary btn-xs">Editar</a>&nbsp;<a href="<?php echo site_url("cursos/eliminar/".$curso->idCurso); ?>" class="btn btn-danger btn-xs">Eliminar</a></td><?php } ?>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

<?php echo $this->load->view('footer_view', '', true); ?>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
                responsive: true,
                language: {
                    url: "//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json"
                },
                iDisplayLength: 50,
                aoColumns: [
                    {bSortable: true},
                    {iDataSort: 2},
                    {bVisible: false},
                    {bSortable: true},
                    {bSortable: true},
                    {bSortable: false},
                    //{bSortable: false},
                    {bSortable: false, sWidth: 85}
                ]
        });
    });
    </script>

<?php echo $this->load->view('close_view', '', true); ?>
