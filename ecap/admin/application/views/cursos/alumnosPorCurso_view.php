
<!-- Navigation -->
<?php echo $this->load->view('header_view', '', true); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Alumnos del <?php echo $curso->nombre ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Listado de alumnos del <?php echo $curso->nombre ?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables">
                            <thead>
                                <tr>
                                    <th>Alumno</th>
                                    <th>Pagado</th>
                                    <th>Debe</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($alumnos as $alumno) { ?>
                                <tr class="gradeA">
                                    <td><?php echo $alumno->alumno ?></td>
                                    <td><?php echo number_format($alumno->pagado, 2, ',', '.') ?></td>
                                    <td><?php echo number_format($alumno->precio - $alumno->pagado, 2, ',', '.') ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

<?php echo $this->load->view('footer_view', '', true); ?>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
                responsive: true,
                language: {
                    url: "//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json"
                }
        });
    });
    </script>

<?php echo $this->load->view('close_view', '', true); ?>
