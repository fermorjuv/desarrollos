
<!-- Navigation -->
<?php echo $this->load->view('header_view', '', true); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Pagos</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ver pago
                    <div style="float:right">
                        <?php if ($this->session->userdata('rol') == '1') { ?>&nbsp;<a href="<?php echo site_url("pagos/eliminar/".$pago->idPago); ?>" class="btn btn-danger btn-xs">Eliminar</a><?php } ?>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Curso</label>
                                <input class="form-control" id="curso" name="curso" type="text" value="<?php echo $pago->curso; ?>" disabled />
                            </div> 
                            <div class="form-group">
                                <label>Fecha del pago</label>
                                <input class="form-control" id="fecha" name="fecha" type="text" value="<?php echo date("d/m/Y", strtotime($pago->fecha)); ?>" disabled />
                            </div>
                            <div class="form-group">
                                <label>Pago</label>
                                <input class="form-control" id="pago" name="pago" type="text" value="<?php echo number_format($pago->pago, 2, ',', '.'); ?>" disabled />
                            </div>
                            <div class="form-group">
                                <label>Tipo</label>
                                <input class="form-control" id="tipo" name="tipo" type="text" value="<?php echo formatTipoPago($pago->tipo); ?>" disabled />
                            </div>
                            <div class="form-group">
                                <label>C&oacute;digo</label>
                                <input class="form-control" id="codigo" name="codigo" type="text" value="<?php echo (isset($pago->codigo)) ? $pago->codigo : '' ; ?>" disabled />
                            </div>
                            <div class="form-group">
                                <label>Observaciones</label>
                                <textarea class="form-control" id="notas" name="notas" disabled><?php echo $pago->notas ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

<?php echo $this->load->view('footer_view', '', true); ?>

<?php echo $this->load->view('close_view', '', true); ?>
