<!-- Navigation -->
<?php echo $this->load->view('header_view', '', true); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Pagos</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Eliminar pago
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php 
                            $attributes = array("id" => "pagosEliminar", "name" => "pagosEliminar", "role" => "form");
                            echo form_open("pagos/eliminar/".$pago->idPago, $attributes);?>
                                <input type="hidden" name="idPago" value="<?php echo $pago->idPago?>" />
                                <div class="form-group">
                                    <label>&iquest;Est&aacute; seguro que desea eliminar el pago del alumno <?php echo $pago->alumno ?> para el <?php echo $pago->curso ?>?</label>
                                </div>
                                <button type="submit" class="btn btn-danger">Eliminar</button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

<?php echo $this->load->view('footer_view', '', true); ?>

<?php echo $this->load->view('close_view', '', true); ?>
