
<!-- Navigation -->
<?php echo $this->load->view('header_view', '', true); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Pagos</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Alta de pago
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <?php 
                        $attributes = array("id" => "pagosAlta", "name" => "pagosAlta", "role" => "form");
                        echo form_open("pagos/alta/".$idAlumno, $attributes);?>
                            <input type="hidden" id="idAlumno" name="idAlumno" value="<?php echo $idAlumno?>" />
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Curso</label>
                                    <select class="form-control" id="curso" name="curso">
                                        <?php foreach ($cursos as $combocurso) { ?>
                                        <option value="<?php echo $combocurso->idCurso?>" <?php echo set_select('curso', $combocurso->idCurso); ?>><?php echo $combocurso->nombre?></option>
                                        <?php } ?>
                                    </select>
                                </div> 
                                <div class="form-group">
                                    <label>Fecha del pago</label>
                                    <input class="form-control" id="fecha" name="fecha" type="text" value="<?php echo set_value('fecha'); ?>" />
                                    <span class="text-danger"><?php echo form_error('fecha'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Importe</label>
                                    <input class="form-control" id="pago" name="pago" type="text" value="<?php echo set_value('pago'); ?>" />
                                    <span class="text-danger"><?php echo form_error('pago'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Tipo</label>
                                    <select class="form-control" id="tipo" name="tipo">
                                        <?php foreach ($tiposPago as $idTipo => $tipo) { ?>
                                        <option value="<?php echo $idTipo?>" <?php echo set_select('tipo', $idTipo); ?>><?php echo $tipo?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('tipo'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>C&oacute;digo</label>
                                    <input class="form-control" id="codigo" name="codigo" type="text" value="<?php echo set_value('codigo'); ?>" />
                                    <span class="text-danger"><?php echo form_error('codigo'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Observaciones</label>
                                    <textarea class="form-control" id="notas" name="notas"><?php echo set_value('notas'); ?></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

<?php echo $this->load->view('footer_view', '', true); ?>

<script>
$(document).ready(function() {
    $("#fecha").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "2015:<?php echo date('Y')?>"
    });
    $.datepicker.setDefaults($.datepicker.regional['es']);
});
</script>

<?php echo $this->load->view('close_view', '', true); ?>
