<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ecap - admin</title>
    <link rel="icon" href="<?php echo base_url("assets/img/favicon.png"); ?>" type="image/png">
    <link href="<?php echo base_url("assets/bower_components/bootstrap/dist/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/bower_components/metisMenu/dist/metisMenu.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/bower_components/jquery-ui/jquery-ui.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/bower_components/datatables-responsive/css/dataTables.responsive.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/dist/css/sb-admin-2.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/bower_components/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url("index"); ?>">ECAP - ADMINISTRACI&Oacute;N</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="<?php echo site_url("alumnos"); ?>"><i class="fa fa-user fa-fw"></i> Alumnos</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url("cursos"); ?>"><i class="fa fa-book fa-fw"></i> Cursos</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url("inscripciones"); ?>"><i class="fa fa-pencil fa-fw"></i> Preinscripciones</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url("pagos"); ?>"><i class="fa fa-money fa-fw"></i> Pagos</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Reportes<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo site_url("reportes/listadoAlumnos"); ?>">Alumnos</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url("reportes/alumnosPorCurso"); ?>">Alumnos por curso</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url("reportes/inscripcionesPorCurso"); ?>">Preinscripciones por curso</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url("reportes/pagosPorAlumno"); ?>">Pagos por alumno</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="https://www.mercadopago.com.ar" target="_blank"><i class="fa fa-dollar fa-fw"></i> MercadoPago</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>