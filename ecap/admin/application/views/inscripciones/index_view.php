
<!-- Navigation -->
<?php echo $this->load->view('header_view', '', true); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Preinscripciones</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Listado de preinscripciones
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables">
                            <thead>
                                <tr>
                                    <th>Email</th>
                                    <th>Curso</th>
                                    <th>Fecha</th>
                                    <th>Fecha</th>
                                    <th>Referencia</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($inscripciones as $inscripcion) {   ?>
                                <tr class="gradeA">
                                    <td><?php echo $inscripcion->email ?></td>
                                    <td><?php echo $inscripcion->curso ?></td>
                                    <td><?php echo date("d-m-Y", strtotime($inscripcion->fecha)); ?></td>
                                    <td><?php echo $inscripcion->fecha ?></td>
                                    <td><?php echo $inscripcion->referencia ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

<?php echo $this->load->view('footer_view', '', true); ?>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function() {
    $('#dataTables').DataTable({
            responsive: true,
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json"
            },
            order: [[ 2, "desc" ]],
            iDisplayLength: 50,
            aoColumns: [
                {bSortable: true},
                {bSortable: true},
                {iDataSort: 3},
                {bVisible: false},
                {bSortable: true}
            ]
    });
});
</script>

<?php echo $this->load->view('close_view', '', true); ?>
