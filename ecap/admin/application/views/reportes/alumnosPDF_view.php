<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <style>
    body {
        font-family: 'sans-serif'
    }
    </style>
</head>

<body>
    <table width="540" border="1" cellpadding="15" cellspacing="0">
        <tbody>
            <tr>
                <td align="center"><h1>Listado de alumnos<?php echo (isset($curso)) ? '&nbsp;'.$curso->nombre : '' ; ?></h1></td>
            </tr>
        </tbody>
    </table>
    <br />
    <?php foreach($alumnos as $alumno) { ?>
        <table width="540" border="1" cellpadding="5" cellspacing="0">
            <tbody>
                <tr>
                    <td><h2>&nbsp;&nbsp;<?php echo $alumno->alumno ?></h2></td>
                </tr>
                <tr>
                    <td><table width="100%" border="0" cellpadding="5" cellspacing="0">
                        <tr>
                            <td><strong>Email: </strong><?php echo $alumno->email?></td>
                            <td><strong>DNI: </strong><?php echo ($alumno->dni != '0') ? $alumno->dni : '' ;?></td>
                        </tr>
                        <tr>
                            <td><strong>Celular: </strong><?php echo ($alumno->celular != '') ? $alumno->celular : '' ;?></td>
                            <td><strong>Tel&eacute;fono: </strong><?php echo ($alumno->telefono != '') ? $alumno->telefono : '' ;?></td>
                        </tr>
                        <tr>
                            <td><strong>Email: </strong><?php echo ($alumno->email != '') ? $alumno->email : '' ;?></td>
                            <td></td>
                        </tr>
                    </table></td>
                </tr>
            </tbody>
        </table>
        <br />
    <?php } ?>
</body>

</html>
