
<!-- Navigation -->
<?php echo $this->load->view('header_view', '', true); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Reporte Alumnos por curso</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <?php 
        $attributes = array("id" => "alumnosPorCurso", "name" => "alumnosPorCurso", "role" => "form");
        echo form_open("reportes/alumnosPorCurso", $attributes);?>
            <div class="col-lg-5">
                <div class="form-group">
                    <select class="form-control" id="curso" name="curso">
                        <?php foreach ($cursos as $combocurso) { ?>
                        <option value="<?php echo $combocurso->idCurso?>" <?php echo set_select('curso', $combocurso->idCurso); ?>><?php echo $combocurso->nombre?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-lg-1">
                <button type="submit" class="btn btn-primary">Filtrar</button>
            </div>
        <?php echo form_close(); ?>
    </div>
    <?php if (isset($curso)) { ?>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php if (isset($curso)) { ?>
                        Listado de alumnos del <?php echo $curso->nombre ?>
                    <?php } else { ?>
                        Listado de alumnos por curso
                    <?php } ?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables">
                            <thead>
                                <tr>
                                    <th>Alumno</th>
                                    <th>Pagado</th>
                                    <th>Debe</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($alumnos as $alumno) { ?>
                                <tr class="gradeA">
                                    <td><?php echo $alumno->alumno ?></td>
                                    <td><?php echo number_format($alumno->precio, 2, ',', '.') ?></td>
                                    <td><?php echo number_format($alumno->precio - $alumno->pagado, 2, ',', '.') ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php } ?>
</div>
<!-- /#page-wrapper -->

<?php echo $this->load->view('footer_view', '', true); ?>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function() {
    $('#dataTables').DataTable({
            responsive: true,
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json"
            },
            dom: 'T<"clear">lfrtip',
            iDisplayLength: 50,
            tableTools: {
                "sSwfPath": "<?php echo base_url('assets/bower_components/datatables-plugins/features/tableTools/')?>/swf/copy_csv_xls_pdf.swf",
                "aButtons": [ "xls", "pdf" ]
            }
    });
});
</script>

<?php echo $this->load->view('close_view', '', true); ?>
