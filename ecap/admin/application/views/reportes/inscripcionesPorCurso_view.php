
<!-- Navigation -->
<?php echo $this->load->view('header_view', '', true); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Reporte Inscripciones por curso</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <?php 
        $attributes = array("id" => "inscripcionesPorCurso", "name" => "inscripcionesPorCurso", "role" => "form");
        echo form_open("reportes/inscripcionesPorCurso", $attributes);?>
            <div class="col-lg-5">
                <div class="form-group">
                    <select class="form-control" id="curso" name="curso">
                        <?php foreach ($cursos as $combocurso) { ?>
                        <option value="<?php echo $combocurso->idCurso?>" <?php echo set_select('curso', $combocurso->idCurso); ?>><?php echo $combocurso->nombre?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-lg-1">
                <button type="submit" class="btn btn-primary">Filtrar</button>
            </div>
        <?php echo form_close(); ?>
    </div>
    <?php if (isset($curso)) { ?>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php if (isset($curso)) { ?>
                        Listado de inscripciones del <?php echo $curso->nombre ?>
                    <?php } else { ?>
                        Listado de inscripciones por curso
                    <?php } ?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables">
                            <thead>
                                <tr>
                                    <th>Email</th>
                                    <th>Fecha</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($inscriptos as $inscripto) { ?>
                                <tr class="gradeA">
                                    <td><?php echo $inscripto->email ?></td>
                                    <td><?php echo date("d-m-Y", strtotime($inscripto->fecha)); ?></td>
                                    <td><?php echo $inscripto->fecha; ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php } ?>
</div>
<!-- /#page-wrapper -->


<?php echo $this->load->view('footer_view', '', true); ?>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function() {
    $('#dataTables').DataTable({
            responsive: true,
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json"
            },
            dom: 'T<"clear">lfrtip',
            tableTools: {
                "sSwfPath": "<?php echo base_url('assets/bower_components/datatables-plugins/features/tableTools/')?>/swf/copy_csv_xls_pdf.swf",
                "aButtons": [ "xls", "pdf" ]
            },
            iDisplayLength: 50,
            aoColumns: [
                {bSortable: true},
                {iDataSort: 2},
                {bVisible: false}
            ]
    });
});
</script>

<?php echo $this->load->view('close_view', '', true); ?>
