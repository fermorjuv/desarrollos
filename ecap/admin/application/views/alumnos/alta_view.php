

<!-- Navigation -->
<?php echo $this->load->view('header_view', '', true); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Alumnos</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Alta de alumno
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <?php 
                        $attributes = array("id" => "alumnosAlta", "name" => "alumnosAlta", "role" => "form");
                        echo form_open("alumnos/alta", $attributes);?>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Apellidos y nombres</label>
                                    <input tabindex="3" class="form-control" id="nombre" name="nombre" type="text" value="<?php echo set_value('nombre'); ?>" />
                                    <span class="text-danger"><?php echo form_error('nombre'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Fecha de nacimiento</label>
                                    <input tabindex="5" class="form-control" id="fechaNacimiento" name="fechaNacimiento" type="text" value="<?php echo set_value('fechaNacimiento'); ?>" />
                                    <span class="text-danger"><?php echo form_error('fechaNacimiento'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Domicilio</label>
                                    <input tabindex="7" class="form-control" id="domicilio" name="domicilio" type="text" value="<?php echo set_value('domicilio'); ?>" />
                                    <span class="text-danger"><?php echo form_error('domicilio'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Provincia</label>
                                    <input tabindex="9" class="form-control" id="provincia" name="provincia" type="text" value="<?php echo set_value('provincia'); ?>" />
                                    <span class="text-danger"><?php echo form_error('provincia'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input tabindex="1" class="form-control" id="email" name="email" type="text" value="<?php echo set_value('email'); ?>" />
                                    <span class="text-danger"><?php echo form_error('email'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Celular</label>
                                    <input tabindex="11" class="form-control" id="celular" name="celular" type="text" value="<?php echo set_value('celular'); ?>" />
                                    <span class="text-danger"><?php echo form_error('celular'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Estudios</label>
                                    <select tabindex="13" class="form-control" id="estudios" name="estudios">
                                        <?php foreach ($estudios as $estudio) { ?>
                                        <option value="<?php echo $estudio?>" <?php echo set_select('estudio', $estudio); ?>><?php echo $estudio?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('estudios'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Observaciones</label>
                                    <textarea tabindex="16" class="form-control" id="notas" name="notas"><?php echo set_value('notas'); ?></textarea>
                                </div>
                                <button tabindex="20" type="submit" class="btn btn-primary">Guardar</button>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>CUIL</label>
                                    <input tabindex="2" class="form-control" id="dni" name="dni" type="text" value="<?php echo set_value('dni'); ?>" />
                                    <span class="text-danger"><?php echo form_error('dni'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Nacionalidad</label>
                                    <input tabindex="6" class="form-control" id="nacionalidad" name="nacionalidad" type="text" value="<?php echo set_value('nacionalidad'); ?>" />
                                    <span class="text-danger"><?php echo form_error('nacionalidad'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Localidad</label>
                                    <input tabindex="8" class="form-control" id="localidad" name="localidad" type="text" value="<?php echo set_value('localidad'); ?>" />
                                    <span class="text-danger"><?php echo form_error('localidad'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>C&oacute;digo postal</label>
                                    <input tabindex="10" class="form-control" id="cp" name="cp" type="text" value="<?php echo set_value('cp'); ?>" />
                                    <span class="text-danger"><?php echo form_error('cp'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Tel&eacute;fono</label>
                                    <input tabindex="12" class="form-control" id="telefono" name="telefono" type="text" value="<?php echo set_value('telefono'); ?>" />
                                    <span class="text-danger"><?php echo form_error('telefono'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>C&oacute;mo nos conoci&oacute;</label>
                                    <input tabindex="4" class="form-control" id="referencia" name="referencia" type="text" value="<?php echo set_value('referencia'); ?>" />
                                    <span class="text-danger"><?php echo form_error('referencia'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Trabajo</label>
                                    <select tabindex="14" class="form-control" id="trabajo" name="trabajo">
                                        <?php
                                        $trabajos = array('Sí', 'No');
                                        foreach ($trabajos as $trabajo) { 
                                        ?>
                                        <option value="<?php echo $trabajo?>" <?php echo set_select('trabajo', $trabajo); ?>><?php echo $trabajo?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('trabajo'); ?></span>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

<?php echo $this->load->view('footer_view', '', true); ?>

<script>
$(document).ready(function() {
    $("#fechaNacimiento").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "1940:<?php echo date('Y')?>"
    });
    $.datepicker.setDefaults($.datepicker.regional['es']);
});
</script>

<?php echo $this->load->view('close_view', '', true); ?>
