
<!-- Navigation -->
<?php echo $this->load->view('header_view', '', true); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Alumnos</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ver alumno
                    <div style="float:right">
                        <a href="<?php echo site_url("alumnos/editar/".$alumno->idAlumno); ?>" class="btn btn-primary btn-xs">Editar</a>&nbsp;<a href="<?php echo site_url("alumnos/eliminar/".$alumno->idAlumno); ?>" class="btn btn-danger btn-xs">Eliminar</a>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Apellidos y nombres</label>
                                <input tabindex="3" class="form-control" id="nombre" name="nombre" type="text" value="<?php echo set_value('nombre', $alumno->nombre); ?>" disabled />
                            </div>
                            <div class="form-group">
                                <label>Fecha de nacimiento</label>
                                <input tabindex="5" class="form-control" id="fechaNacimiento" name="fechaNacimiento" type="text" value="<?php echo ($alumno->fechaNacimiento != '00/00/0000') ? set_value('fechaNacimiento', $alumno->fechaNacimiento) : ''; ?>" disabled />
                            </div>
                            <div class="form-group">
                                <label>Domicilio</label>
                                <input tabindex="7" class="form-control" id="domicilio" name="domicilio" type="text" value="<?php echo set_value('domicilio', $alumno->domicilio); ?>" disabled />
                            </div>
                            <div class="form-group">
                                <label>Provincia</label>
                                <input tabindex="9" class="form-control" id="provincia" name="provincia" type="text" value="<?php echo set_value('provincia', $alumno->provincia); ?>" disabled />
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input tabindex="1" class="form-control" id="email" name="email" type="text" value="<?php echo set_value('email', $alumno->email); ?>" disabled />
                            </div>
                            <div class="form-group">
                                <label>Celular</label>
                                <input tabindex="11" class="form-control" id="celular" name="celular" type="text" value="<?php echo set_value('celular', $alumno->celular); ?>" disabled />
                            </div>
                            <div class="form-group">
                                <label>Estudios</label>
                                <select tabindex="13" class="form-control" id="estudios" name="estudios" disabled>
                                    <?php foreach ($estudios as $estudio) { ?>
                                    <option value="<?php echo $estudio?>" <?php echo set_select('estudio', $estudio, $estudio == $alumno->estudios); ?>><?php echo $estudio?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Observaciones</label>
                                <textarea tabindex="16" class="form-control" id="notas" name="notas" disabled><?php echo set_value('notas', $alumno->notas); ?></textarea>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>CUIL</label>
                                <input tabindex="2" class="form-control" id="dni" name="dni" type="text" value="<?php echo ($alumno->dni != '0') ? set_value('dni', $alumno->dni) : ''; ?>" disabled />
                            </div>
                            <div class="form-group">
                                <label>Nacionalidad</label>
                                <input tabindex="6" class="form-control" id="nacionalidad" name="nacionalidad" type="text" value="<?php echo set_value('nacionalidad', $alumno->nacionalidad); ?>" disabled />
                            </div>
                            <div class="form-group">
                                <label>Localidad</label>
                                <input tabindex="8" class="form-control" id="localidad" name="localidad" type="text" value="<?php echo set_value('localidad', $alumno->localidad); ?>" disabled />
                            </div>
                            <div class="form-group">
                                <label>C&oacute;digo postal</label>
                                <input tabindex="10" class="form-control" id="cp" name="cp" type="text" value="<?php echo set_value('cp', $alumno->cp); ?>" disabled />
                            </div>
                            <div class="form-group">
                                <label>Tel&eacute;fono</label>
                                <input tabindex="12" class="form-control" id="telefono" name="telefono" type="text" value="<?php echo set_value('telefono', $alumno->telefono); ?>" disabled />
                            </div>
                            <div class="form-group">
                                <label>C&oacute;mo nos conoci&oacute;</label>
                                <input tabindex="4" class="form-control" id="referencia" name="referencia" type="text" value="<?php echo set_value('referencia', $alumno->referencia); ?>" disabled />
                            </div>
                            <div class="form-group">
                                <label>Trabajo</label>
                                <select tabindex="14" class="form-control" id="trabajo" name="trabajo" disabled>
                                    <?php
                                    $trabajos = array('Sí', 'No');
                                    foreach ($trabajos as $trabajo) { 
                                    ?>
                                    <option value="<?php echo $trabajo?>" <?php echo set_select('trabajo', $trabajo, $trabajo == $alumno->trabajo); ?>><?php echo $trabajo?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

<?php echo $this->load->view('footer_view', '', true); ?>

<?php echo $this->load->view('close_view', '', true); ?>
