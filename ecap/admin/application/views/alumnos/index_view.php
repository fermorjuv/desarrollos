
<!-- Navigation -->
<?php echo $this->load->view('header_view', '', true); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Alumnos</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Listado de alumnos
                    <div style="float:right">
                        <a href="<?php echo site_url("alumnos/alta"); ?>" class="btn btn-primary btn-xs">Alta alumno</a>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables">
                            <thead>
                                <tr>
                                    <th>Apellidos y nombres</th>
                                    <th>CUIL</th>
                                    <th>Email</th>
                                    <th>Cursos</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach($alumnos as $alumno) {
                                    $cursosAlumno = '';
                                    $arrayCursosAlumno = ($alumno->cursos != '') ? explode(',', $alumno->cursos) : array();
                                    foreach ($arrayCursosAlumno as $idCurso) {
                                        $idCurso = trim($idCurso);
                                        $url = site_url("matriculaciones/ver/".$alumno->idAlumno."/".$idCurso);
                                        $cursosAlumno .= '<br /><a href="'.$url.'">'.$cursos[$idCurso].'</a>';
                                    }

                                    $tieneDeuda = count($cursosImpagos[$alumno->idAlumno]) > 0;
                                ?>
                                <tr class="gradeA">
                                    <td><?php echo $alumno->nombre ?></td>
                                    <td><?php echo ($alumno->dni != '0') ? $alumno->dni : '' ?></td>
                                    <td><?php echo $alumno->email ?></td>
                                    <td><a href="<?php echo site_url("matriculaciones/alta/".$alumno->idAlumno); ?>" class="btn btn-primary btn-xs">Matricular</a><?php if ($cursosAlumno != '') { if ($tieneDeuda) { ?>&nbsp;<a href="<?php echo site_url("pagos/alta/".$alumno->idAlumno); ?>" class="btn btn-success btn-xs">Pagar</a><?php } echo $cursosAlumno; } ?></td>
                                    <td><a href="<?php echo site_url("alumnos/ver/".$alumno->idAlumno); ?>" class="btn btn-primary btn-xs">Ver</a></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php echo $this->load->view('footer_view', '', true); ?>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function() {
    $('#dataTables').DataTable({
            responsive: true,
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json"
            },
            order: [[ 0, "asc" ]],
            iDisplayLength: 100,
            aoColumns: [
                {bSortable: true},
                {bSortable: true},
                {bSortable: true},
                {bSortable: false},
                {bSortable: false, sWidth: 15}
            ]
    });
});
</script>

<?php echo $this->load->view('close_view', '', true); ?>
