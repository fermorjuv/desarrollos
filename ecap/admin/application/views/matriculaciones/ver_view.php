
<!-- Navigation -->
<?php echo $this->load->view('header_view', '', true); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Matriculaciones</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Matriculaci&oacute;n de <?php echo $alumno->nombre ?>
                    <div style="float:right">
                        <a href="<?php echo site_url("matriculaciones/editar/".$matriculacion->idAlumno.'/'.$matriculacion->idCurso); ?>" class="btn btn-primary btn-xs">Editar</a>&nbsp;<a href="<?php echo site_url("matriculaciones/eliminar/".$matriculacion->idAlumno.'/'.$matriculacion->idCurso); ?>" class="btn btn-danger btn-xs">Eliminar</a>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Curso</label>
                                <input class="form-control" id="curso" name="curso" type="text" value="<?php echo $curso->nombre ?>" disabled />
                            </div> 
                            <div class="form-group">
                                <label>Precio</label>
                                <input class="form-control" id="precio" name="precio" type="text" value="<?php echo $matriculacion->precio; ?>" disabled />
                            </div>        
                            <div class="form-group">
                                <label>Notas</label>
                                <textarea class="form-control" id="notas" name="notas" disabled><?php echo $matriculacion->notas; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-body -->
                <div class="panel-heading">
                    Pagos
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Fecha</th>
                                    <th>Importe</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach($pagos as $pago) {
                                ?>
                                <tr class="gradeA">
                                    <td><?php echo date("d/m/Y", strtotime($pago->fecha)); ?></td>
                                    <td><?php echo $pago->fecha ?></td>
                                    <td><?php echo $pago->pago ?></td>
                                    <td><?php if ($this->session->userdata('rol') == '1') { ?>&nbsp;<a href="<?php echo site_url("pagos/eliminar/".$pago->idPago); ?>" class="btn btn-danger btn-xs">Eliminar</a><?php } ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

<?php echo $this->load->view('footer_view', '', true); ?>

<script>
$(document).ready(function() {
    $("#fecha").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "2015:<?php echo date('Y')?>"
    });
    $.datepicker.setDefaults($.datepicker.regional['es']);

    $('#dataTables').DataTable({
            responsive: true,
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json"
            },
            "order": [[ 1, "desc" ]],
            iDisplayLength: 50,
            aoColumns: [
                {iDataSort: 1},
                {bVisible: false},
                {bSortable: true},
                {bSortable: false, sWidth: 45}
            ]
    });
});
</script>

<?php echo $this->load->view('close_view', '', true); ?>
