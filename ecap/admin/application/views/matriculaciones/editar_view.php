
<!-- Navigation -->
<?php echo $this->load->view('header_view', '', true); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Matriculaciones</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Matriculaci&oacute;n de <?php echo $alumno->nombre ?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <?php 
                        $attributes = array("id" => "matriculacionesAlta", "name" => "matriculacionesAlta", "role" => "form");
                        echo form_open("matriculaciones/editar/".$alumno->idAlumno.'/'.$curso->idCurso, $attributes);?>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Curso</label>
                                    <input class="form-control" id="curso" name="curso" type="text" value="<?php echo set_value('curso', $curso->nombre); ?>" disabled />
                                </div> 
                                <div class="form-group">
                                    <label>Precio</label>
                                    <input class="form-control" id="precio" name="precio" type="text" value="<?php echo set_value('precio', $matriculacion->precio); ?>" />
                                    <span class="text-danger"><?php echo form_error('precio'); ?></span>
                                </div>        
                                <div class="form-group">
                                    <label>Notas</label>
                                    <textarea class="form-control" id="notas" name="notas"><?php echo set_value('notas'); ?><?php echo set_value('notas', $matriculacion->notas); ?></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

<?php echo $this->load->view('footer_view', '', true); ?>

<script>
$(document).ready(function() {
    $("#fecha").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "2015:<?php echo date('Y')?>"
    });
    $.datepicker.setDefaults($.datepicker.regional['es']);
});
</script>

<?php echo $this->load->view('close_view', '', true); ?>
