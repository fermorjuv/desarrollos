<!-- Navigation -->
<?php echo $this->load->view('header_view', '', true); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Matriculaciones</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Desmatricular alumno de curso
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php 
                            $attributes = array("id" => "matriculacionesEliminar", "name" => "matriculacionesEliminar", "role" => "form");
                            echo form_open("matriculaciones/eliminar/".$alumno->idAlumno."/".$curso->idCurso, $attributes);?>
                                <input type="hidden" name="idAlumno" value="<?php echo $alumno->idAlumno ?>" />
                                <input type="hidden" name="idCurso" value="<?php echo $curso->idCurso ?>" />
                                <div class="form-group">
                                    <label>&iquest;Est&aacute; seguro que desea desmatricular al alumno <?php echo $alumno->nombre ?> del <?php echo $curso->nombre ?>?</label>
                                </div>
                                <button type="submit" class="btn btn-danger">Desmatricular</button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

<?php echo $this->load->view('footer_view', '', true); ?>

<?php echo $this->load->view('close_view', '', true); ?>
