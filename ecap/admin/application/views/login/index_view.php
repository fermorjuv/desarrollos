<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ecap - admin</title>
    <link rel="icon" href="<?php echo base_url("assets/img/favicon.png"); ?>" type="image/png">
    <link href="<?php echo base_url("assets/bower_components/bootstrap/dist/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/bower_components/metisMenu/dist/metisMenu.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/dist/css/sb-admin-2.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/bower_components/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

  <div class="container">
      <div class="row">
          <div class="col-md-4 col-md-offset-4">
              <div class="login-panel panel panel-default">
                  <div class="panel-heading">
                      <h3 class="panel-title">Ingrese en el sistema</h3>
                  </div>
                  <div class="panel-body">
                      <?php 
                      $attributes = array("id" => "loginform", "name" => "loginform", "role" => "form");
                      echo form_open("login/index", $attributes);?>
                          <fieldset>
                              <div class="form-group">
                                  <input class="form-control" id="txt_username" name="txt_username" placeholder="Usuario" type="text" value="<?php echo set_value('txt_username'); ?>" autofocus />
                                  <span class="text-danger"><?php echo form_error('txt_username'); ?></span>
                              </div>
                              <div class="form-group">
                                  <input class="form-control" id="txt_password" name="txt_password" placeholder="Clave" type="password" value="" />
                                  <span class="text-danger"><?php echo form_error('txt_password'); ?></span>
                              </div>
                              <?php echo $this->session->flashdata('msg'); ?>
                              <!-- Change this to a button or input when using this as a form -->
                              <input id="btn_login" name="btn_login" type="submit" class="btn btn-lg btn-success btn-block" value="Ingresar" />
                          </fieldset>
                      <?php echo form_close(); ?>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <!-- jQuery -->
  <script src="<?php echo base_url("assets/bower_components/jquery/dist/jquery.min.js"); ?>"></script>
  <!-- Bootstrap Core JavaScript -->
  <script src="<?php echo base_url("assets/bower_components/bootstrap/dist/js/bootstrap.min.js"); ?>"></script>
  <!-- Metis Menu Plugin JavaScript -->
  <script src="<?php echo base_url("assets/bower_components/metisMenu/dist/metisMenu.min.js"); ?>"></script>
  <!-- Custom Theme JavaScript -->
  <script src="<?php echo base_url("assets/dist/js/sb-admin-2.js"); ?>"></script>

</body>
</html>
