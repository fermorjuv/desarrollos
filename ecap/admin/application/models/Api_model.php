<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //get the username & password from tbl_usrs
    function insertPayment($id, $topic, $externalReference = '')
    {
        /*switch ($topic) {
            case 'cancel':
                // CANCELACION
                $paymentInfo = array(
                    'status' => 200,
                    'response' => array(
                        'collection' => array(
                            'id' => $id,
                            'status' => 'cancelled',
                            'status_detail' => 'by_payer',

                            'site_id' => 'AR',
                            'payer' => array(
                                'id' => '456',
                                'first_name' => 'FernandoML',
                                'last_name' => 'MorenoML',
                                'phone' => array(
                                    'area_code' => '011',
                                    'number' => '43211234',
                                    'extension' => '43'
                                ),
                                'identification' => array(
                                    'type' => 'DNI',
                                    'number' => '94090987',

                                ),
                                'email' => 'fermorjuvML@gmail.com',
                                'nickname' => 'MLFERNANDO'
                            ),
                            'transaction_amount' => '3900.00',
                            'currency_id' => 'ARS',
                            'installments' => '1',
                            'payment_type' => 'credit_card',
                            'payment_method_id' => 'visa',


                            'date_created' => '2015-08-22 10:11:12',
                            'date_approved' => '2015-08-22 11:12:13',
                            'money_release_date' => '2015-08-22 12:13:14',
                            'last_modified' => '2015-08-22 13:14:15',
                        ),
                    ),
                );
                break;

            case 'total':
                // APPROVED PAGO TOTAL
                $paymentInfo = array(
                    'status' => 200,
                    'response' => array(
                        'collection' => array(
                            'id' => $id,
                            'status' => 'approved',
                            'status_detail' => 'accredited',
                            'external_reference' => mycrypt('7|lalala@lala.com|4'),

                            'site_id' => 'AR',
                            'payer' => array(
                                'id' => '456',
                                'first_name' => 'FernandoML',
                                'last_name' => 'MorenoML',
                                'phone' => array(
                                    'area_code' => '011',
                                    'number' => '43211234',
                                    'extension' => '43'
                                ),
                                'identification' => array(
                                    'type' => 'DNI',
                                    'number' => '94090987',

                                ),
                                'email' => 'fermorjuvML@gmail.com',
                                'nickname' => 'MLFERNANDO'
                            ),
                            'transaction_amount' => '3900.00',
                            'currency_id' => 'ARS',
                            'installments' => '1',
                            'payment_type' => 'credit_card',
                            'payment_method_id' => 'visa',


                            'date_created' => '2015-08-22 10:11:12',
                            'date_approved' => '2015-08-22 11:12:13',
                            'money_release_date' => '2015-08-22 12:13:14',
                            'last_modified' => '2015-08-22 13:14:15',
                        ),
                    ),
                );
                break;

            case 'parcial':
                // APPROVED PAGO PARCIAL
                $paymentInfo = array(
                    'status' => 200,
                    'response' => array(
                        'collection' => array(
                            'id' => $id,
                            'status' => 'approved',
                            'status_detail' => 'accredited',
                            'external_reference' => mycrypt('8|lololo@lolo.com|4'),

                            'site_id' => 'AR',
                            'payer' => array(
                                'id' => '456',
                                'first_name' => 'FernandoML',
                                'last_name' => 'MorenoML',
                                'phone' => array(
                                    'area_code' => '011',
                                    'number' => '43211234',
                                    'extension' => '43'
                                ),
                                'identification' => array(
                                    'type' => 'DNI',
                                    'number' => '94090987',

                                ),
                                'email' => 'fermorjuvML@gmail.com',
                                'nickname' => 'MLFERNANDO'
                            ),
                            'transaction_amount' => '1300',
                            'currency_id' => 'ARS',
                            'installments' => '1',
                            'payment_type' => 'credit_card',
                            'payment_method_id' => 'visa',


                            'date_created' => '2015-08-22 10:11:12',
                            'date_approved' => '2015-08-22 11:12:13',
                            'money_release_date' => '2015-08-22 12:13:14',
                            'last_modified' => '2015-08-22 13:14:15',
                        ),
                    ),
                );
                break;

            default: die;
        }*/
        $this->load->library('mercadopago');

        $this->mercadopago->sandbox_mode(FALSE);

        $paymentInfo = $this->mercadopago->get_payment_info($id);

        if (isset($paymentInfo["status"]) && $paymentInfo["status"] == 200) {


            //Leemos la informacion del pago
            $info = $paymentInfo['response']['collection'];

            $fp = fopen('data.txt', 'a+');
            fwrite($fp, json_encode($info));
            fclose($fp);
            
            /*
            //Formateamos las fechas
            $dateCreated = (isset($info['date_created']) && $info['date_created'] != '') ? date('Y-m-d H:i:s', strtotime($info['date_created'])) : '';
            $dateApproved = (isset($info['date_approved']) && $info['date_approved'] != '') ? date('Y-m-d H:i:s', strtotime($info['date_approved'])) : '';
            $moneyReleaseDate = (isset($info['money_release_date']) && $info['money_release_date'] != '') ? date('Y-m-d H:i:s', strtotime($info['money_release_date'])) : '';

            //Comprobamos si es un pago nuevo o ya estaba en la base de datos
            $this->db->select('idPago, idInscripcion');
            $this->db->from('pago');
            $this->db->where('codigo', $info['id']);
            $query = $this->db->get();

            if ($query->num_rows() > 0) {

                //Si ya estaba en la base de datos leemos los datos
                $row = $query->row();
                $idPago = $row->idPago;
                $idInscripcion = $row->idInscripcion;

                //Nos fijamos si lo que viene es una cancelacion
                $this->load->model('pagos_model');
                $cancelled = $this->pagos_model->getEstadosPagoError();
                if (is_numeric($idInscripcion) && $idInscripcion != '' && in_array($info['status'], $cancelled)) {

                    $this->load->model('inscripciones_model');
                    $this->inscripciones_model->cancelarPago($idInscripcion);
                }

                //Formateamos la fecha de modificacion
                $lastModified = (isset($info['last_modified']) && $info['last_modified'] != '') ? date('Y-m-d H:i:s', strtotime($info['last_modified'])) : '';

                //Preparamos la modificacion
                $data = array(
                    'fecha'             => $dateCreated, 
                    'fechaAprobado'     => $dateApproved, 
                    'fechaCobrado'      => $moneyReleaseDate, 
                    'fechaModificado'   => $lastModified,
                    'pais'              => $info['site_id'],
                    'clienteId'         => $info['payer']['id'], 
                    'clienteNombres'    => $info['payer']['first_name'], 
                    'clienteApellidos'  => $info['payer']['last_name'], 
                    'clienteTelefono'   => $info['payer']['phone']['area_code']."-".$info['payer']['phone']['number']."-".$info['payer']['phone']['extension'], 
                    'clienteDni'        => $info['payer']['identification']['type']." ".$info['payer']['identification']['number'], 
                    'clienteEmail'      => $info['payer']['email'], 
                    'clienteNick'       => $info['payer']['nickname'], 
                    'pago'              => $info['transaction_amount'],
                    'moneda'            => $info['currency_id'], 
                    'estado'            => $info['status'], 
                    'estadoDetalle'     => $info['status_detail'], 
                    'cuotas'            => $info['installments'], 
                    'tipo'              => $info['payment_type'], 
                    'metodo'             => $info['payment_method_id']  
                );

                if ($externalReference != '') {
                    //Desencriptamos la referencia externa
                    $externalReference = mydecrypt($externalReference);

                    $arrayExternalReference = explode('|', $externalReference);

                    //Si todo es correcto (vienen todos los datos)
                    if (count($arrayExternalReference) == 3) {

                        list($idInscripcion, $email, $idCurso) = $arrayExternalReference;

                        $data['idInscripcion']      = $idInscripcion;
                        $data['idCurso']            = $idCurso; 
                        $data['codigoInscripcion']  = $externalReference;

                        $this->load->model('inscripciones_model');
                        $inscripcion = $this->inscripciones_model->getInscripcion($idInscripcion);
                        //Si la inscripción no tiene algún pago ya lo cargamos
                        if ($inscripcion->estado < 2) {
                            $this->inscripciones_model->insertarPago($idInscripcion, $info['transaction_amount']);
                        }
                    }
                }

                $this->db->where('idPago', $idPago);
                $this->db->update('pago', $data);

            } else {

                if ($externalReference == '') {
                    //Desencriptamos la referencia externa
                    $externalReference = mydecrypt($info['external_reference']);
                } else {
                    $externalReference = mydecrypt($externalReference);
                }
                
                $data = explode('|', $externalReference);

                $idInscripcion = 0;
                $idCurso = 0;

                //Si todo es correcto (vienen todos los datos)
                if (count($data) == 3) {

                    list($idInscripcion, $email, $idCurso) = $data;

                    //Si todas las variables son correctas
                    if (is_numeric($idInscripcion) && is_numeric($idCurso) && $email != '') {

                        $this->load->model('inscripciones_model');
                        $this->inscripciones_model->insertarPago($idInscripcion, $info['transaction_amount']);

                    } else {

                        $idInscripcion = 0;
                        $idCurso = 0;
                    }
                }

                //Preparamos la insercion
                $data = array(
                    'idInscripcion'     => $idInscripcion,
                    'idCurso'           => $idCurso,
                    'codigo'            => $info['id'],
                    'fecha'             => $dateCreated, 
                    'fechaAprobado'     => $dateApproved, 
                    'fechaCobrado'      => $moneyReleaseDate, 
                    'pais'              => $info['site_id'],
                    'clienteId'         => $info['payer']['id'], 
                    'clienteNombres'    => $info['payer']['first_name'], 
                    'clienteApellidos'  => $info['payer']['last_name'], 
                    'clienteTelefono'   => $info['payer']['phone']['area_code']."-".$info['payer']['phone']['number']."-".$info['payer']['phone']['extension'], 
                    'clienteDni'        => $info['payer']['identification']['type']." ".$info['payer']['identification']['number'], 
                    'clienteEmail'      => $info['payer']['email'], 
                    'clienteNick'       => $info['payer']['nickname'], 
                    'pago'              => $info['transaction_amount'],
                    'moneda'            => $info['currency_id'], 
                    'estado'            => $info['status'], 
                    'estadoDetalle'     => $info['status_detail'], 
                    'cuotas'            => $info['installments'], 
                    'tipo'              => $info['payment_type'], 
                    'metodo'            => $info['payment_method_id'], 
                    'codigoInscripcion' => $externalReference
                );

                $this->db->insert('pago', $data);
            }*/
            return true;
        } else {
            return false;
        }
    }
}
?>