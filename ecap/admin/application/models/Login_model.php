<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function getUser($usr, $pwd)
     {
          $this->db->select('idAdmin, nombre, rol');
          $this->db->from('admin');
          $this->db->where(
               array(
                    'usuario' => $usr,
                    'clave'   => md5($pwd),
                    'activo'  => '1'
               )
          );
          $query = $this->db->get();

          return $query->result();
     }
}?>