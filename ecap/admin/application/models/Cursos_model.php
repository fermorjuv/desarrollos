<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cursos_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function getCursos()
     {
          $this->db->select('
               curso.idCurso, 
               curso.nombre, 
               curso.fechaInicio, 
               curso.fechaFin, 
               curso.moneda, 
               curso.precio, 
               curso.descripcion, 
               curso.url, 
               count(DISTINCT matriculacion.idMatriculacion) as alumnos
          ', FALSE);
          $this->db->from('curso');
          $this->db->join('matriculacion', 'curso.idCurso = matriculacion.idCurso', 'left');
          $this->db->where('curso.activo', '1');
          $this->db->order_by('curso.nombre');
          $this->db->group_by('curso.idCurso');
          $query = $this->db->get();

          return $query->result();
     }

     function getCursosTodos()
     {
          $this->db->select('
               curso.idCurso, 
               curso.nombre, 
               curso.fechaInicio, 
               curso.fechaFin, 
               curso.moneda, 
               curso.precio, 
               curso.descripcion, 
               curso.url
          ');
          $this->db->from('curso');
          $this->db->order_by('curso.nombre');
          $this->db->group_by('curso.idCurso');
          $query = $this->db->get();
          
          return $query->result();
     }

     function getCurso($idCurso)
     {

          $this->db->select('idCurso, nombre, fechaInicio, fechaFin, moneda, precio, descripcion, url');
          $this->db->from('curso');
          $this->db->where('idCurso', $idCurso);
          $query = $this->db->get();

          if ($this->db->count_all_results() > 0) {
               return $query->row();
          } else {
               return null;
          }
     }

     function getAlumnosPorCurso($idCurso)
     {
          $this->db->select(' alumno.nombre as alumno, alumno.email, alumno.dni, alumno.nacionalidad, 
                              DATE_FORMAT(alumno.fechaNacimiento, "%d/%m/%Y") as fechaNacimiento, 
                              alumno.domicilio, alumno.localidad, alumno.provincia, alumno.cp,
                              alumno.telefono, alumno.celular,
                              alumno.estudios, alumno.trabajo, 
                              alumno.referencia, alumno.notas,
                              matriculacion.precio, 
                              SUM(pago.pago) AS pagado', FALSE);
          $this->db->from('alumno');
          $this->db->join('matriculacion', 'alumno.idAlumno = matriculacion.idAlumno AND matriculacion.idCurso = '.$idCurso);
          $this->db->join('pago', 'matriculacion.idMatriculacion = pago.idMatriculacion', 'left');
          $this->db->group_by('alumno.idAlumno');
          $this->db->order_by('alumno.nombre');
          $query = $this->db->get();
          
          return $query->result();
     }

     //Funcion que devuelve los cursos activos que no ha hecho el alumno
     function getNoCursosPorAlumno($idAlumno)
     {
          //Creamos un array con los cursos que tiene el alumno
          $CI =& get_instance();
          $CI->load->model('alumnos_model');
          $cursosAlumno = $CI->alumnos_model->getCursosPorAlumno($idAlumno);
          $arrayCurso = array();
          foreach ($cursosAlumno as $curso) {
               $arrayCurso[] = $curso->idCurso;
          }

          $this->db->select(' curso.idCurso, 
                              curso.nombre, 
                              curso.fechaInicio, 
                              curso.fechaFin, 
                              curso.moneda, 
                              curso.precio, 
                              curso.descripcion, 
                              curso.url');
          $this->db->from('curso');
          $this->db->where('curso.activo', '1');
          $this->db->where('curso.fechaFin >=', date('Y-m-d'));
          if (count($arrayCurso) > 0) {
               $this->db->where_not_in('curso.idCurso', $arrayCurso);
          }          
          $this->db->order_by('curso.nombre');
          $this->db->group_by('curso.idCurso');
          $query = $this->db->get();

          return $query->result();
     }

     function insert($data) 
     {
          if ($data['fechaInicio'] != '') {
               $fecha = date_create_from_format('d/m/Y', $data['fechaInicio'])->format('Y-m-d');
               $data['fechaInicio'] = $fecha;
          } else {
               unset($data['fechaInicio']);
          }
          if ($data['fechaFin'] != '') {
               $fecha = date_create_from_format('d/m/Y', $data['fechaFin'])->format('Y-m-d');
               $data['fechaFin'] = $fecha;
          } else {
               unset($data['fechaFin']);
          }

          $this->db->insert('curso', $data); 
          
          return $this->db->affected_rows();
     }

     function edit($idCurso, $data) 
     {
          $this->db->select('idCurso');
          $this->db->from('curso');
          $this->db->where(
               array(
                    'activo'       => '1',
                    'idCurso !='   => $idCurso,
                    'nombre'       => $data['nombre'],
               )
          );

          if ($this->db->count_all_results() > 0) {
               return -1;
          } else {

               $dataUpdate = array(
                    'nombre'            => $data['nombre'],
                    'precio'            => $data['precio'],
                    'descripcion'       => $data['descripcion'],
                    'url'               => $data['url'],
               );

               if ($data['fechaInicio'] != '') {
                    $fecha = date_create_from_format('d/m/Y', $data['fechaInicio'])->format('Y-m-d');
                    $dataUpdate['fechaInicio'] = $fecha;
               }
               if ($data['fechaFin'] != '') {
                    $fecha = date_create_from_format('d/m/Y', $data['fechaFin'])->format('Y-m-d');
                    $dataUpdate['fechaFin'] = $fecha;
               }

               $this->db->where('idCurso', $idCurso);
               $this->db->update('curso', $dataUpdate);

               return $this->db->affected_rows();
          }
     }

     function delete($idCurso) 
     {
          $this->db->where('idCurso', $idCurso);
          $this->db->update('curso', array('activo' => '0'));
          
          return $this->db->affected_rows();
     }
}?>