<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pagos_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function getPagos()
     {
          $this->db->select("
               pago.idPago,
               pago.codigo,
               pago.fecha,
               pago.pago,
               pago.tipo,
               TRIM(IFNULL(curso.nombre, '')) as curso,
               alumno.nombre as alumno,
          ", FALSE);
          $this->db->from('pago');
          $this->db->join('matriculacion', 'pago.idMatriculacion= matriculacion.idMatriculacion', 'left');
          $this->db->join('curso', 'matriculacion.idCurso = curso.idCurso', 'left');
          $this->db->join('alumno', 'matriculacion.idAlumno = alumno.idAlumno', 'left');
          $query = $this->db->get();

          return $query->result();
     }

     function getPagosMatriculacion($idMatriculacion)
     {
          $this->db->select("
               pago.idPago,
               pago.codigo,
               pago.fecha,
               pago.pago,
               pago.tipo,
               TRIM(IFNULL(curso.nombre, '')) as curso,
               alumno.nombre as alumno,
          ", FALSE);
          $this->db->from('pago');
          $this->db->join('matriculacion', 'pago.idMatriculacion= matriculacion.idMatriculacion', 'left');
          $this->db->join('curso', 'matriculacion.idCurso = curso.idCurso', 'left');
          $this->db->join('alumno', 'matriculacion.idAlumno = alumno.idAlumno', 'left');
          $this->db->where('matriculacion.idMatriculacion', $idMatriculacion);
          $query = $this->db->get();

          return $query->result();
     }

     function getPagosAlumno($idAlumno)
     {
          $this->db->select("
               pago.idPago,
               pago.codigo,
               pago.fecha,
               pago.pago,
               pago.tipo,
               curso.nombre as curso,
               alumno.nombre as alumno,
          ", FALSE);
          $this->db->from('pago');
          $this->db->join('matriculacion', 'pago.idMatriculacion= matriculacion.idMatriculacion');
          $this->db->join('curso', 'matriculacion.idCurso = curso.idCurso');
          $this->db->join('alumno', 'matriculacion.idAlumno = alumno.idAlumno');
          $this->db->where('matriculacion.idAlumno', $idAlumno);
          $query = $this->db->get();

          return $query->result();
     }

     function getPago($idPago)
     {
          $this->db->select("
               pago.idPago,
               pago.idMatriculacion,
               pago.codigo,
               pago.fecha,
               pago.pago,
               pago.tipo,
               pago.notas,
               TRIM(IFNULL(curso.nombre, '')) as curso,
               TRIM(IFNULL(alumno.nombre, '')) as alumno,
          ");
          $this->db->from('pago');
          $this->db->join('matriculacion', 'pago.idMatriculacion= matriculacion.idMatriculacion', 'left');
          $this->db->join('curso', 'matriculacion.idCurso = curso.idCurso', 'left');
          $this->db->join('alumno', 'matriculacion.idAlumno = alumno.idAlumno', 'left');
          $this->db->where('idPago', $idPago);
          $query = $this->db->get();

          return $query->row();
     }

     function getDeuda($idMatriculacion)
     {
          $this->db->select("matriculacion.precio, IFNULL(SUM(pago.pago), 0) as pagado", FALSE);
          $this->db->from('matriculacion');
          $this->db->join('pago', 'matriculacion.idMatriculacion = pago.idMatriculacion', 'left');
          $this->db->where('matriculacion.idMatriculacion', $idMatriculacion);
          $this->db->group_by('matriculacion.idMatriculacion');
          $query = $this->db->get();
          $matriculacion = $query->row();

          return round(($matriculacion->precio - $matriculacion->pagado), 2);
     }

     function alumnoTieneDeuda($idAlumno)
     {
          $this->db->select("matriculacion.precio, IFNULL(SUM(pago.pago), 0) as pagado", FALSE);
          $this->db->from('matriculacion');
          $this->db->join('pago', 'matriculacion.idMatriculacion = pago.idMatriculacion', 'left');
          $this->db->where('matriculacion.idAlumno', $idAlumno);
          $this->db->group_by('matriculacion.idMatriculacion');
          $query = $this->db->get();

          return $query->result();
     }

     function getTiposPago()
     {
          return array(
               'check'        => 'Cheque',
               'deposit'      => 'Dep&oacute;sito',
               'cash'         => 'Efectivo',
               'mercadolibre' => 'MercadoLibre',
               'credit_card'  => 'Tarjeta de cr&eacute;dito',
               'debit_card'   => 'Tarjeta de d&eacute;bito',
               'transfer'     => 'Transferencia',
          );
     }     

     function insert($data) 
     {
          $data['fecha'] = date_create_from_format('d/m/Y', $data['fecha'])->format('Y-m-d');
          
          $this->db->insert('pago', $data);
          
          return $this->db->affected_rows();
     }

     function delete($idPago) 
     {
          $this->db->where('idPago', $idPago);
          $this->db->delete('pago');
          
          return $this->db->affected_rows();
     }
}?>