<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Matriculaciones_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function getMatriculacion($idAlumno, $idCurso)
     {
          $this->db->select('idMatriculacion, idCurso, idAlumno, precio, notas');
          $this->db->from('matriculacion');
          $this->db->where('idAlumno', $idAlumno);
          $this->db->where('idCurso', $idCurso);
          $query = $this->db->get();

          return $query->row();
     }

     function insert($data) 
     {
          $this->db->from('matriculacion');
          $this->db->where('matriculacion.idAlumno', $data['idAlumno']);
          $this->db->where('matriculacion.idCurso', $data['idCurso']);
          if ($this->db->count_all_results() == 0) {

               $this->db->insert('matriculacion', $data); 
               
               return $this->db->affected_rows();
          }

          return 0;
     }

     function edit($idMatriculacion, $data) 
     {
          $this->db->where('idMatriculacion', $idMatriculacion);
          $this->db->update('matriculacion', $data);

          return $this->db->affected_rows();
     }

     function delete($idAlumno, $idCurso) 
     {
          $this->db->where('idAlumno', $idAlumno);
          $this->db->where('idCurso', $idCurso);
          $this->db->delete('matriculacion');
          
          return $this->db->affected_rows();
     }
}?>