<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inscripciones_model extends CI_Model
{
    function __construct()
    {
      // Call the Model constructor
      parent::__construct();
    }

    function getInscripciones()
    {
        $this->db->select('
            inscripcion.idInscripcion, 
            inscripcion.idCurso,
            inscripcion.email, 
            inscripcion.fecha, 
            inscripcion.referencia, 
            TRIM(IFNULL(curso.nombre, "")) as curso
        ', FALSE);
        $this->db->from('inscripcion');
        $this->db->join('curso', 'inscripcion.idCurso = curso.idCurso', 'left');
        $query = $this->db->get();

        return $query->result();
    }

    function getInscripcion($idInscripcion)
    {
        $this->db->select('
            inscripcion.idInscripcion,
            inscripcion.idCurso, 
            inscripcion.email, 
            inscripcion.fecha, 
            inscripcion.referencia, 
            TRIM(IFNULL(curso.nombre, "")) as curso
        ');
        $this->db->from('inscripcion');
        $this->db->join('curso', 'inscripcion.idCurso = curso.idCurso', 'left');
        $this->db->where('idInscripcion', $idInscripcion);
        $query = $this->db->get();

        return $query->row();
    }

    function getLastInscripciones()
    {
        //Las ultimas inscripciones seran aquellas cuyo curso aun no ha empezado
        $this->db->select('idInscripcion');
        $this->db->from('inscripcion');
        $this->db->join('curso', 'inscripcion.idCurso = curso.idCurso');
        $this->db->where('DATE(NOW()) <= curso.fechaInicio');
        $query = $this->db->get();

        return $query->result();
    }

    function getInscripcionesPorCurso($idCurso)
    {
          $this->db->select("inscripcion.email, inscripcion.fecha", FALSE);
          $this->db->from('inscripcion');
          $this->db->where('inscripcion.idCurso', $idCurso);
          $this->db->where('inscripcion.email IS NOT NULL');
          $query = $this->db->get();

          return $query->result();
    }
    
    function insert($data)
    {
        $idCurso = mydecrypt($data['id']);
        $email = trim($data['email']);
        $referencia = trim($data['reference']);
        $nombre = trim($data['name']);

        $this->load->model('cursos_model');
        $curso = $this->cursos_model->getCurso($idCurso);

        if ($curso != null) {

            $sql = "SELECT idInscripcion
                    FROM inscripcion
                    WHERE idCurso = ".$this->db->escape($idCurso)." 
                        AND email = ".$this->db->escape($email);
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {

                $inscripcion = $query->row();

                $sql = "UPDATE inscripcion
                        SET nombre = ".$this->db->escape($nombre).",
                            referencia = ".$this->db->escape($referencia)."
                        WHERE idCurso = ".$this->db->escape($idCurso)." 
                            AND email = ".$this->db->escape($email);
                $query = $this->db->query($sql);
                    
                $codigoInscripcion = mycrypt($inscripcion->idInscripcion.'|'.$email.'|'.$idCurso);

                return $codigoInscripcion;

            } else {
                $sql = "INSERT INTO inscripcion (idCurso, nombre, email, referencia) 
                        VALUES (".$this->db->escape($idCurso).", ".$this->db->escape($nombre).", ".$this->db->escape($email).", ".$this->db->escape($referencia).")";
                $query = $this->db->query($sql);

                if ($this->db->affected_rows() > 0) {
                    
                    $codigoInscripcion = mycrypt($this->db->insert_id().'|'.$email.'|'.$idCurso);

                    return $codigoInscripcion;
                }
            }
        }
        return false;
    }
}
?>