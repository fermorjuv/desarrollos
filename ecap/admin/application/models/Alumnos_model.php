<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alumnos_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function getAlumnos()
     {
          $this->db->select('
               alumno.idAlumno, 
               alumno.email, 
               alumno.dni, 
               alumno.nombre,  
               GROUP_CONCAT(DISTINCT curso.idCurso SEPARATOR ",") as cursos
          ');
          $this->db->from('alumno');
          $this->db->join('matriculacion', 'alumno.idAlumno = matriculacion.idAlumno', 'left');
          $this->db->join('curso', 'matriculacion.idCurso = curso.idCurso AND curso.activo = 1 AND curso.fechaFin >= NOW()', 'left');
          $this->db->where('alumno.activo', '1');
          $this->db->group_by('alumno.idAlumno');
          $this->db->order_by('alumno.nombre');
          $query = $this->db->get();

          return $query->result();
     }

     function getAlumno($idAlumno)
     {
          $this->db->select('
               idAlumno, email, dni, nombre, nacionalidad, 
               DATE_FORMAT(fechaNacimiento, "%d/%m/%Y") as fechaNacimiento, 
               domicilio, localidad, provincia, cp,
               telefono, celular, estudios,
               trabajo, referencia, notas
          ');
          $this->db->from('alumno');
          $this->db->where('activo', '1');
          $this->db->where('idAlumno', $idAlumno);
          $query = $this->db->get();

          return $query->row();
     }

     function getAlumnoPorEmail($email)
     {
          $this->db->select('
               idAlumno, email, dni, nombre, nacionalidad, 
               DATE_FORMAT(fechaNacimiento, "%d/%m/%Y") as fechaNacimiento, 
               domicilio, localidad, provincia, cp,
               telefono, celular, estudios,
               trabajo, referencia, notas
          ');
          $this->db->from('alumno');
          $this->db->where('activo', '1');
          $this->db->where('email', $email);
          $query = $this->db->get();

          return $query->row();
     }

     function getCursosPorAlumno($idAlumno)
     {
          $this->db->select(" curso.idCurso, 
                              curso.nombre, 
                              curso.fechaInicio, 
                              curso.fechaFin, 
                              curso.moneda, 
                              curso.precio, 
                              curso.descripcion, 
                              curso.url");
          $this->db->from('curso');
          $this->db->join('matriculacion', 'curso.idCurso = matriculacion.idCurso');
          $this->db->join('alumno', 'matriculacion.idAlumno = alumno.idAlumno');
          $this->db->where('alumno.idAlumno', $idAlumno);
          $this->db->order_by('curso.nombre');
          $query = $this->db->get();
          
          return $query->result();
     }

     function getCursosImpagosPorAlumno($idAlumno)
     {
          $this->db->select(" curso.idCurso, 
                              curso.nombre, 
                              curso.fechaInicio, 
                              curso.fechaFin, 
                              matriculacion.precio, 
                              curso.descripcion, 
                              curso.url,
                              COALESCE(SUM(pago.pago),0) as pagado", false);
          $this->db->from('curso');
          $this->db->join('matriculacion', 'curso.idCurso = matriculacion.idCurso');
          $this->db->join('alumno', 'matriculacion.idAlumno = alumno.idAlumno');
          $this->db->join('pago', 'matriculacion.idMatriculacion = pago.idMatriculacion', 'left');
          $this->db->where('alumno.idAlumno', $idAlumno);
          $this->db->having('pagado < matriculacion.precio');
          $this->db->group_by('curso.idCurso');
          $this->db->order_by('curso.nombre');
          $query = $this->db->get();
          
          return $query->result();
     }

     function getEstudios()
     {
          return array(
               'Primario',
               'Secundario',
               'Secundario incompleto',
               'Terciario',
               'Terciario incompleto',
               'Universitario',
               'Universitario incompleto',
               'Posgrado',
               'Posgrado incompleto'
          );
     }

     function existeEmail($email, $idAlumno = null)
     {
          $this->db->select('idAlumno');
          $this->db->from('alumno');
          $this->db->where(
               array(
                    'activo'  => '1',
                    'email'   => $email,
               )
          );
          if (isset($idAlumno)) {
               $this->db->where('idAlumno !=', $idAlumno);
          }

          return ($this->db->count_all_results() > 0);
     }

     function insert($data) 
     {
          if ($data['fechaNacimiento'] != '') {
               $data['fechaNacimiento'] = date_create_from_format('d/m/Y', $data['fechaNacimiento'])->format('Y-m-d');
          }

          $this->db->insert('alumno', $data); 
          
          return $this->db->affected_rows();
     }

     function edit($idAlumno, $data) 
     {
          if ($data['fechaNacimiento'] != '') {
               $fechaNacimiento = date_create_from_format('d/m/Y', $data['fechaNacimiento'])->format('Y-m-d');
          } else {
               $fechaNacimiento = '';
          }

          $data = array(
               'email'             => $data['email'],
               'nombre'            => $data['nombre'],
               'dni'               => $data['dni'],
               'nacionalidad'      => $data['nacionalidad'],
               'fechaNacimiento'   => $fechaNacimiento,
               'domicilio'         => $data['domicilio'],
               'localidad'         => $data['localidad'],
               'provincia'         => $data['provincia'],
               'cp'                => $data['cp'],
               'telefono'          => $data['telefono'],
               'celular'           => $data['celular'],
               'estudios'          => $data['estudios'],
               'trabajo'           => $data['trabajo'],
               'referencia'        => $data['referencia'],
               'notas'             => $data['notas'],
            );

          $this->db->where('idAlumno', $idAlumno);
          $this->db->update('alumno', $data);

          return $this->db->affected_rows();
     }

     function delete($idAlumno) 
     {
          $this->db->where('idAlumno', $idAlumno);
          $this->db->update('alumno', array('activo' => '0'));
          
          return $this->db->affected_rows();
     }
}?>