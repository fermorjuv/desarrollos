<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inscripciones extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        //No se controla la sesión de todas las acciones en el constructor porque el alta se hace para todos
    }

    public function index()
    {
        if (!$this->session->userdata('loginuser')) {
            redirect('login/index');
        }

        $this->load->model('inscripciones_model');

        $data = array(
            'inscripciones' => $this->inscripciones_model->getInscripciones()
        );

        $this->load->view('inscripciones/index_view', $data);
    }

    public function alta()
    {
        $this->load->model('inscripciones_model');

        $codigoInscripcion = $this->inscripciones_model->insert($this->input->post());

        if ($codigoInscripcion !== FALSE) {
            echo $codigoInscripcion;
        } else {
            echo '0';
        }
    }
}
