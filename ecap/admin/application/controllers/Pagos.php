<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->database();

        if (!$this->session->userdata('loginuser')) {
            redirect('login/index');
        }
    }

    public function index()
    {
        $this->load->model('pagos_model');

        $data = array(
            'pagos' => $this->pagos_model->getPagos()
        );

        $this->load->view('pagos/index_view', $data);
    }

    public function ver($idPago)
    {
        $this->load->model('pagos_model');

        $pago = $this->pagos_model->getPago($idPago);

        $this->load->view('pagos/ver_view', array('pago' => $pago));
    }

    public function alta($idAlumno)
    {
        if ($this->session->userdata('rol') != '1') {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">No tienes permiso para dar de alta un pago</div>');
            redirect('inscripciones');
        }

        $this->load->model('pagos_model');
        $this->load->model('alumnos_model');
        $this->load->model('matriculaciones_model');
        $this->load->library('form_validation');

        $data = array(
            'codigo'    => $this->input->post("codigo"),
            'fecha'     => $this->input->post("fecha"),
            'pago'      => round($this->input->post("pago"), 2),
            'tipo'      => $this->input->post("tipo"),
            'notas'     => $this->input->post("notas"),
        );
        $matriculacion = $this->matriculaciones_model->getMatriculacion($idAlumno, $this->input->post("curso"));
        $idMatriculacion = (isset($matriculacion)) ? $matriculacion->idMatriculacion : 0;

        //set validations
        $this->form_validation->set_rules("fecha", "Fecha del pago", "trim|required|date_check");
        $this->form_validation->set_rules("pago", "Importe", "trim|required|numeric|callback_pago_check");

        if ($this->form_validation->run() == FALSE) {
            //validation fails
            $dataView = array(
                'idAlumno'  => $idAlumno,
                'cursos'    => $this->alumnos_model->getCursosImpagosPorAlumno($idAlumno),
                'tiposPago' => $this->pagos_model->getTiposPago(),
            );

            $this->load->view('pagos/alta_view', $dataView);
        } else {
            $data['idMatriculacion'] = $idMatriculacion;

            $pagoInsertado = $this->pagos_model->insert($data);

            if ($pagoInsertado) {
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Se ha insertado correctamente el pago</div>');
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Se ha producido un error al insertar el pago</div>');
            }
            redirect("pagos");
        }
    }

    public function eliminar($idPago)
    {
        $this->load->model('pagos_model');
        $this->load->library('form_validation');

        $pago = $this->pagos_model->getPago($idPago);

        if (!isset($pago)) {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">El pago que se trata de eliminar no existe</div>');
            redirect('pagos');
        }

        $result = $this->input->post("idPago");

        if (!isset($result)) {
            //validation fails
            $this->load->view('pagos/eliminar_view', array('pago' => $pago));
        } else {
            $result = $this->pagos_model->delete($idPago);
            switch ($result) {
                case 0:
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Se ha producido un error al eliminar el pago</div>');
                    break;
                default:
                    $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Se ha eliminado correctamente el pago</div>');
            };
            redirect("pagos");
        }
    }

    public function pago_check($pago)
    {
        $this->load->model('matriculaciones_model');
        $this->load->model('pagos_model');

        $matriculacion = $this->matriculaciones_model->getMatriculacion($this->input->post("idAlumno"), $this->input->post("curso"));
        $deuda = $this->pagos_model->getDeuda($matriculacion->idMatriculacion);

        $intpago    = intval($pago * 100);
        $intdebe    = intval($deuda * 100);

        if ($intpago > $intdebe) {
            $this->form_validation->set_message('pago_check', 'El campo %s no puede ser mayor que $'.round($deuda, 2));
            return false;
        }
        return true;
    }
}
