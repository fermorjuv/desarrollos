<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Matriculaciones extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        if (!$this->session->userdata('loginuser')) {
            redirect('login/index');
        }
    }

    public function alta($idAlumno)
    {
        $this->load->model('matriculaciones_model');
        $this->load->model('cursos_model');
        $this->load->model('alumnos_model');
        $this->load->library('form_validation');

        $data = array(
            'idAlumno'  => $idAlumno,
            'idCurso'   => $this->input->post("curso"),
            'precio'    => $this->input->post("precio"),
            'notas'     => $this->input->post("notas"),
        );

        $dataView['alumno'] = $this->alumnos_model->getAlumno($idAlumno);
        $dataView['cursos'] = $this->cursos_model->getNoCursosPorAlumno($idAlumno);

        //set validations
        $this->form_validation->set_rules("precio", "Precio", "trim|required|numeric");

        if ($this->form_validation->run() == FALSE) {
            //validation fails
            $this->load->view('matriculaciones/alta_view', $dataView);
        } else {
            if ($this->matriculaciones_model->insert($data) == 0) {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Se ha producido un error al matricular al alumno</div>');
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Se ha matriculado correctamente al alumno</div>');
            }
            redirect("matriculaciones/ver/".$data['idAlumno']."/".$data['idCurso']);
        }
    }

    public function ver($idAlumno, $idCurso)
    {
        $this->load->model('alumnos_model');
        $this->load->model('cursos_model');
        $this->load->model('pagos_model');
        $this->load->model('matriculaciones_model');
        $this->load->library('form_validation');

        $matriculacion = $this->matriculaciones_model->getMatriculacion($idAlumno, $idCurso);

        $data = array(
            'matriculacion'  => $matriculacion,
            'alumno'         => $this->alumnos_model->getAlumno($idAlumno),
            'curso'          => $this->cursos_model->getCurso($idCurso),
            'pagos'          => $this->pagos_model->getPagosMatriculacion($matriculacion->idMatriculacion)
        );

        $this->load->view('matriculaciones/ver_view', $data);
    }

    public function editar($idAlumno, $idCurso)
    {
        $this->load->model('alumnos_model');
        $this->load->model('cursos_model');
        $this->load->model('pagos_model');
        $this->load->model('matriculaciones_model');
        $this->load->library('form_validation');        

        $data = array(
            'precio' => $this->input->post("precio"),
            'notas'  => $this->input->post("notas"),
        );

        $matriculacion = $this->matriculaciones_model->getMatriculacion($idAlumno, $idCurso);
        if (count($this->pagos_model->getPagosMatriculacion($matriculacion->idMatriculacion)) > 0) {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">No se puede editar la matriculaci&oacute;n del alumno del curso porque tiene pagos hechos</div>');
            redirect("matriculaciones/ver/".$idAlumno."/".$idCurso);
        }

        //set validations
        $this->form_validation->set_rules("precio", "Precio", "trim|required|numeric");

        if ($this->form_validation->run() == FALSE) {

            $dataView = array(
                'matriculacion'  => $matriculacion,
                'alumno'         => $this->alumnos_model->getAlumno($idAlumno),
                'curso'          => $this->cursos_model->getCurso($idCurso),
            );
            //validation fails
            $this->load->view('matriculaciones/editar_view', $dataView);
        } else {
            if ($this->matriculaciones_model->edit($matriculacion->idMatriculacion, $data) == 0) {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Se ha producido un error al editar la matriculaci&oacute;n del alumno</div>');
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Se ha modificado correctamente la matriculaci&oacute;n del alumno</div>');
            }
            redirect("matriculaciones/ver/".$idAlumno."/".$idCurso);
        }
    }

    public function eliminar($idAlumno, $idCurso)
    {
        $this->load->model('alumnos_model');
        $this->load->model('cursos_model');
        $this->load->model('pagos_model');
        $this->load->model('matriculaciones_model');
        $this->load->library('form_validation');

        $alumno = $this->alumnos_model->getAlumno($idAlumno);
        $curso = $this->cursos_model->getCurso($idCurso);

        if (!isset($alumno) && !isset($curso)) {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">No se puede desmatricular al alumno del curso</div>');
            redirect("matriculaciones/ver/".$idAlumno."/".$idCurso);
        }

        $matriculacion = $this->matriculaciones_model->getMatriculacion($idAlumno, $idCurso);
        if (count($this->pagos_model->getPagosMatriculacion($matriculacion->idMatriculacion)) > 0) {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">No se puede desmatricular al alumno del curso porque tiene pagos hechos</div>');
            redirect("matriculaciones/ver/".$idAlumno."/".$idCurso);
        }

        $postIdAlumno = $this->input->post("idAlumno");
        $postIdCurso = $this->input->post("idCurso");

        if (!isset($postIdAlumno) || !isset($postIdCurso)) {
            //validation fails
            $this->load->view('matriculaciones/eliminar_view', array('alumno' => $alumno, 'curso' => $curso));
        } else {
            $result = $this->matriculaciones_model->delete($postIdAlumno, $postIdCurso);
            switch ($result) {
                case 0:
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Se ha producido un error al desmatricular al alumno del curso</div>');
                    redirect("matriculaciones/ver/".$idAlumno."/".$idCurso);
                    break;
                default:
                    $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Se ha desmatriculado correctamente el alumno del curso</div>');
                    redirect("alumnos");
            };
        }
    }
}
