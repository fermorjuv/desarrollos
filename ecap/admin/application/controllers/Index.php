<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->database();

        if (!$this->session->userdata('loginuser')) {
            redirect('login/index');
        }
    }

    public function index()
    {
        $this->load->view('index/index_view');
    }
}
