<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function listadoAlumnos()
    {
        $this->load->model('cursos_model');
        $this->load->library('form_validation');

        $data = array(
            'cursos' => $this->cursos_model->getCursosTodos()
        );

        if ($this->input->post("curso") != '') {
            
            $data['alumnos'] = $this->cursos_model->getAlumnosPorCurso($this->input->post("curso"));
            $data['curso'] = $this->cursos_model->getCurso($this->input->post("curso"));

            $this->load->helper(array('dompdf', 'file'));
            $html = $this->load->view('reportes/alumnosPDF_view', $data, true);
            pdf_create($html, 'listado_alumnos_'.date('d-m-Y'));

        }

        $this->load->view('reportes/listadoAlumnos_view', $data);
    }

    public function alumnosPorCurso()
    {
        $this->load->model('cursos_model');
        $this->load->library('form_validation');

        $this->form_validation->set_rules("curso", "Curso", "trim|required");

        $data = array(
            'cursos' => $this->cursos_model->getCursosTodos()
        );

        if ($this->form_validation->run() != FALSE) {
            $data['alumnos'] = $this->cursos_model->getAlumnosPorCurso($this->input->post("curso"));
            $data['curso'] = $this->cursos_model->getCurso($this->input->post("curso"));
        }

        $this->load->view('reportes/alumnosPorCurso_view', $data);
    }

    public function inscripcionesPorCurso()
    {
        $this->load->model('cursos_model');
        $this->load->library('form_validation');

        $this->form_validation->set_rules("curso", "Curso", "trim|required");

        $data = array(
            'cursos' => $this->cursos_model->getCursosTodos()
        );

        if ($this->form_validation->run() != FALSE) {
            $this->load->model('inscripciones_model');
            $data['inscriptos'] = $this->inscripciones_model->getInscripcionesPorCurso($this->input->post("curso"));
            $data['curso'] = $this->cursos_model->getCurso($this->input->post("curso"));
        }

        $this->load->view('reportes/inscripcionesPorCurso_view', $data);
    }

    public function pagosPorAlumno()
    {
        $this->load->model('alumnos_model');
        $this->load->model('inscripciones_model');
        $this->load->library('form_validation');

        $this->form_validation->set_rules("alumno", "Alumno", "trim|required");

        $data = array(
            'alumnos' => $this->alumnos_model->getAlumnos()
        );

        if ($this->form_validation->run() != FALSE) {
            $this->load->model('alumnos_model');
            $this->load->model('pagos_model');

            $data['alumno'] = $this->alumnos_model->getAlumno($this->input->post("alumno"));
            $data['pagos']  = $this->pagos_model->getPagosAlumno($this->input->post("alumno"));
        }

        $this->load->view('reportes/pagosPorAlumno_view', $data);
    }
}
