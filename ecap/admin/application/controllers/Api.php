<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /*public function getPayment($id)
    {
        $this->load->library('mercadopago');

        $this->mercadopago->sandbox_mode(FALSE);

        $paymentInfo = $this->mercadopago->get_payment_info($id);

        echo 'Payment<br>';
        var_dump($paymentInfo);
    }*/

    public function inscription($codigo)
    {
        $validacion = mydecrypt($codigo);
        $data = explode('|', $validacion);

        if (count($data) == 3) {

            list($idInscripcion, $email, $idCurso) = $data;

            if (is_numeric($idInscripcion) && is_numeric($idCurso) && $email != '') {
                
                $this->load->model('cursos_model');
                $curso = $this->cursos_model->getCurso($idCurso);

                if (isset($curso)) {

                    $this->load->library('mercadopago');
                        
                    $preference_data = array(
                        "items" => array(
                           array(
                               "title"        => $curso->nombre,
                               "quantity"     => 1,
                               "currency_id"  => $curso->moneda,
                               "unit_price"   => (float)$curso->precio
                           )
                        ),
                        "back_urls" => array(
                            "success" => "http://ecap.com.ar/payment.php?c=".mycrypt($curso->url.'|'.$curso->nombre.'|success'),
                            "pending" => "http://ecap.com.ar/payment.php?c=".mycrypt($curso->url.'|'.$curso->nombre.'|pending'),
                            "failure" => "http://ecap.com.ar/".$curso->url,
                        ),
                        "external_reference" => $codigo
                    );

                    $preference = $this->mercadopago->create_preference($preference_data);

                    redirect($preference['response']['init_point']);
                } else {
                    redirect('http://ecap.com.ar/error.php?c=1&c='.$codigo);
                }
            } else {
                redirect('http://ecap.com.ar/error.php?c=2&c='.$codigo);
            }
        } else {
            redirect('http://ecap.com.ar/error.php?c=3&c='.$codigo);
        }
    }

    public function payment()
    {
        $queryString = $_SERVER['QUERY_STRING'];
        $getElements = explode('&', $queryString);
        $get = array();
        foreach ($getElements as $getElement) {
            $element = explode('=', $getElement);
            if ($element[0] == 'id' || $element[0] == 'topic') {
                $get[$element[0]] = $element[1];
            }
        }
        
        if (count($get) == 2) {
            $this->load->model('api_model');
            $this->api_model->insertPayment($get["id"], $get["topic"]);
        }
    }

    /*public function getAlumno($email)
    {
        $email = mydecrypt($email);
        $this->load->model('alumnos_model');
        $this->load->model('cursos_model');
        $this->load->model('pagos_model');
        $alumno = $this->alumnos_model->getAlumnoPorEmail($email);

        $data = array();
        if ($alumno != null) {
            $cursos = $this->alumnos_model->getCursosPorAlumno($alumno->idAlumno);
            foreach ($cursos as $curso) {
                $alumnoCurso = $curso;
                $alumnoCurso->archivos = $this->cursos_model->getArchivosPorCurso($curso->idCurso);
                unset($alumnoCurso->idCurso);
                unset($alumnoCurso->moneda);
                unset($alumnoCurso->precio);
                unset($alumnoCurso->descripcion);
                unset($alumnoCurso->url);
                $data['cursos'][] = $alumnoCurso;

            }

            $pagos = $this->pagos_model->getPagosPorAlumno($email);
            foreach ($pagos as $pago) {
                unset($pago->idPago);
                unset($pago->fechaCobrado);
                unset($pago->email);
                $data['pagos'][] = $pago;
            }
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }*/
}
