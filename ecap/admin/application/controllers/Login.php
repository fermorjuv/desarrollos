<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function index()
    {
        $this->load->library('form_validation');
        //load the login model
        $this->load->model('login_model');
        
        //get the posted values
        $username = $this->input->post("txt_username");
        $password = $this->input->post("txt_password");

        //set validations
        $this->form_validation->set_rules("txt_username", "Usuario", "trim|required");
        $this->form_validation->set_rules("txt_password", "Clave", "trim|required");

        if ($this->form_validation->run() == FALSE) {
            //validation fails
            $this->session->sess_destroy();
            $this->load->view('login/index_view');
        } else {
            //validation succeeds
            if ($this->input->post('btn_login') == "Ingresar") {
                //check if username and password is correct
                $users = $this->login_model->getUser($username, $password);
                if (count($users) > 0) //active user record is present
                {
                    $user = array_shift($users);
                    //set the session variables
                    $sessiondata = array(
                        'username'  => $username,
                        'nombre'    => $user->nombre,
                        'rol'       => $user->rol,
                        'loginuser' => TRUE
                    );
                    $this->session->set_userdata($sessiondata);
                    redirect("index");
                } else {
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Usuario y/o clave incorrectos</div>');
                    redirect('login/index');
                }
            } else {
                redirect('login/index');
            }
        }
    }
}
