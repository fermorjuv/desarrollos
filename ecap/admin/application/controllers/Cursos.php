<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cursos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->database();

        if (!$this->session->userdata('loginuser')) {
            redirect('login/index');
        }
    }

    public function index()
    {
        $this->load->model('cursos_model');

        $data = array(
            'cursos' => $this->cursos_model->getCursos()
        );

        $this->load->view('cursos/index_view', $data);
    }

    public function alumnosPorCurso($idCurso)
    {
        $this->load->model('cursos_model');

        $data = array(
            'alumnos' => $this->cursos_model->getAlumnosPorCurso($idCurso),
            'curso' => $this->cursos_model->getCurso($idCurso),
        );

        $this->load->view('cursos/alumnosPorCurso_view', $data);
    }

    public function archivosPorCurso($idCurso)
    {
        if (!isset($idCurso)) {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">No existe el curso indicado</div>');
            redirect('cursos');
        } else {
            $this->load->model('cursos_model');

            $data = array(
                'archivos' => $this->cursos_model->getArchivosPorCurso($idCurso),
                'curso' => $this->cursos_model->getCurso($idCurso),
            );

            $this->load->view('cursos/archivosPorCurso_view', $data);   
        }
    }

    public function alta()
    {
        if ($this->session->userdata('rol') != '1') {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">No tienes permiso para dar de alta un curso</div>');
            redirect('cursos');
        }

        $this->load->model('cursos_model');
        $this->load->library('form_validation');

        //set validations
        $this->form_validation->set_rules("nombre", "Nombre", "trim|required");
        $this->form_validation->set_rules("fechaInicio", "Fecha de inicio", "trim|date_check");
        $this->form_validation->set_rules("fechaFin", "Fecha de finalizaci&oacute;n", "trim|date_check");
        $this->form_validation->set_rules("precio", "Precio", "trim|required|numeric");
        $this->form_validation->set_rules("descripcion", "Descripci&oacute;n", "trim|required");
        $this->form_validation->set_rules("url", "URL", "trim|required");

        if ($this->form_validation->run() == FALSE) {
            //validation fails
            $this->load->view('cursos/alta_view');
        } else {
            $data = array(
                'nombre'        => $this->input->post("nombre"),
                'fechaInicio'   => $this->input->post("fechaInicio"),
                'fechaFin'      => $this->input->post("fechaFin"),
                'precio'        => round($this->input->post("precio"), 2),
                'moneda'        => 'ARS',
                'descripcion'   => $this->input->post("descripcion"),
                'url'           => $this->input->post("url"),
            );

            $cursoInsertado = ($this->cursos_model->insert($data) != 0);

            if ($cursoInsertado) {
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Se ha insertado correctamente el curso</div>');
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Se ha producido un error al insertar el curso</div>');
            }
            redirect("cursos");
        }
    }

    public function altaArchivo($idCurso)
    {
        if ($this->session->userdata('rol') != '1') {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">No tienes permiso para dar de alta un archivo en un curso</div>');
            redirect('cursos');
        }

        $this->load->model('cursos_model');
        $this->load->library('form_validation');

        //set validations
        $this->form_validation->set_rules("nombre", "Nombre", "trim|required");
        if (empty($_FILES['file']['name']))
        {
            $this->form_validation->set_rules('file', 'Archivo', 'required');
        }

        if ($this->form_validation->run() != FALSE) {

            $this->load->library('upload');

            $id = uniqid().uniqid();
            $folder = 'upload';
            $config['upload_path'] = "./$folder/";
            $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
            $config['file_name'] = $id;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('file')) {
                $img = $this->upload->data();
                
                $data = array(
                    'idCurso'   => $idCurso,
                    'nombre'    => $this->input->post("nombre"),
                    'filename'  => $_FILES['file']['name'],
                    'archivo'   => $folder.'/'.$id.$img['file_ext'],
                );

                $archivoInsertado = ($this->cursos_model->insertArchivo($data) > 0);

                if ($archivoInsertado) {
                    $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Se ha insertado correctamente el archivo del curso</div>');
                } else {
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Se ha producido un error al insertar el archivo</div>');
                }

                redirect("cursos/archivosPorCurso/$idCurso");
            } else {
                $data['errorFile'] = $this->upload->display_errors();
            }
        }

        $data['idCurso'] = $idCurso;
        $this->load->view('cursos/altaArchivo_view', $data);
    }

    public function editar($idCurso)
    {
        if ($this->session->userdata('rol') != '1') {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">No tienes permiso para editar un curso</div>');
            redirect('cursos');
        }

        $this->load->model('cursos_model');
        $this->load->library('form_validation');

        $curso = $this->cursos_model->getCurso($idCurso);

        if (!isset($curso)) {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">El curso que se trata de editar no existe</div>');
            redirect('cursos');
        }

        //set validations
        $this->form_validation->set_rules("nombre", "Nombre", "trim|required");
        $this->form_validation->set_rules("fechaInicio", "Fecha de inicio", "trim|date_check");
        $this->form_validation->set_rules("fechaFin", "Fecha de finalizaci&oacute;n", "trim|date_check");
        $this->form_validation->set_rules("precio", "Precio", "trim|required|numeric");
        $this->form_validation->set_rules("descripcion", "Descripci&oacute;n", "trim|required");
        $this->form_validation->set_rules("url", "URL", "trim|required");

        if ($this->form_validation->run() == FALSE) {
            //validation fails
            $dataView = array('curso' => $curso);
            $this->load->view('cursos/editar_view', $dataView);
        } else {
            $data = array(
                'nombre'        => $this->input->post("nombre"),
                'fechaInicio'   => $this->input->post("fechaInicio"),
                'fechaFin'      => $this->input->post("fechaFin"),
                'precio'        => round($this->input->post("precio"), 2),
                'moneda'        => 'ARS',
                'descripcion'   => $this->input->post("descripcion"),
                'url'           => $this->input->post("url"),
            );

            $result = $this->cursos_model->edit($idCurso, $data);
            switch ($result) {
                case -1:
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Ya existe otro curso con el mismo nombre</div>');
                    break;
                case 0:
                    $this->session->set_flashdata('msg', '<div class="alert alert-info text-center">No se ha modificado el curso</div>');
                    break;
                default:
                    $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Se ha editado correctamente el curso</div>');
            };
            redirect("cursos");
        }
    }

    public function eliminar($idCurso)
    {
        $this->load->model('cursos_model');
        $this->load->library('form_validation');

        $curso = $this->cursos_model->getCurso($idCurso);

        if (!isset($curso)) {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">El curso que se trata de eliminar no existe</div>');
            redirect('cursos');
        }

        $result = $this->input->post("idCurso");

        if (!isset($result)) {
            //validation fails
            $this->load->view('cursos/eliminar_view', array('curso' => $curso));
        } else {
            $result = $this->cursos_model->delete($idCurso);
            switch ($result) {
                case 0:
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Se ha producido un error al eliminar el curso</div>');
                    break;
                default:
                    $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Se ha eliminado correctamente el curso</div>');
            };
            redirect("cursos");
        }
    }
}
