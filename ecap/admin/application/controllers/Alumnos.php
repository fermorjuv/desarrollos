<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumnos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->database();

        if (!$this->session->userdata('loginuser')) {
            redirect('login/index');
        }
    }

    public function index()
    {
        $this->load->model('alumnos_model');
        $this->load->model('cursos_model');

        $cursos = $this->cursos_model->getCursosTodos();
        $arrayCursos = array();
        foreach ($cursos as $curso) {
            $arrayCursos[$curso->idCurso] = $curso->nombre;
        }

        $alumnos = $this->alumnos_model->getAlumnos();
        $cursosImpagos = array();
        foreach ($alumnos as $alumno) {
            $cursosImpagos[$alumno->idAlumno] = $this->alumnos_model->getCursosImpagosPorAlumno($alumno->idAlumno);
        }

        $data = array(
            'alumnos'       => $alumnos,
            'cursos'        => $arrayCursos,
            'cursosImpagos' => $cursosImpagos,
        );

        $this->load->view('alumnos/index_view', $data);
    }

    public function ver($idAlumno)
    {
        $this->load->model('alumnos_model');

        $alumno = $this->alumnos_model->getAlumno($idAlumno);

        $this->load->view('alumnos/ver_view', array('alumno' => $alumno));
    }

    public function alta()
    {
        $this->load->model('alumnos_model');
        $this->load->library('form_validation');

        $data = array(
            'email'           => $this->input->post("email"),
            'nombre'          => $this->input->post("nombre"),
            'dni'             => $this->input->post("dni"),
            'nacionalidad'    => $this->input->post("nacionalidad"),
            'fechaNacimiento' => $this->input->post("fechaNacimiento"),
            'domicilio'       => $this->input->post("domicilio"),
            'localidad'       => $this->input->post("localidad"),
            'provincia'       => $this->input->post("provincia"),
            'cp'              => $this->input->post("cp"),
            'telefono'        => $this->input->post("telefono"),
            'celular'         => $this->input->post("celular"),
            'estudios'        => $this->input->post("estudios"),
            'trabajo'         => $this->input->post("trabajo"),
            'referencia'      => $this->input->post("referencia"),
            'notas'           => $this->input->post("notas"),
        );

        //set validations
        $this->form_validation->set_rules("nombre", "Apellidos y nombres", "trim|required");
        $this->form_validation->set_rules("dni", "CUIL", "trim|required|min_length[8]|max_length[11]|numeric");
        $this->form_validation->set_rules("fechaNacimiento", "Fecha de nacimiento", "trim|date_check");
        $this->form_validation->set_rules("email", "Email", "trim|valid_email");

        $dataView = array('estudios' => $this->alumnos_model->getEstudios());

        if ($this->form_validation->run() == FALSE) {
            //validation fails
            $this->load->view('alumnos/alta_view', $dataView);
        } else {
            if ($this->alumnos_model->insert($data) == 0) {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Se ha producido un error al insertar el alumno</div>');
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Se ha insertado correctamente el alumno</div>');
            }
            redirect("alumnos");
        }
    }

    public function editar($idAlumno)
    {
        $this->load->model('alumnos_model');
        $this->load->library('form_validation');

        $alumno = $this->alumnos_model->getAlumno($idAlumno);

        if (!isset($alumno)) {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">El alumno que se trata de editar no existe</div>');
            redirect('alumnos');
        }

        $data = array(
            'email'           => $this->input->post("email"),
            'nombre'          => $this->input->post("nombre"),
            'dni'             => $this->input->post("dni"),
            'nacionalidad'    => $this->input->post("nacionalidad"),
            'fechaNacimiento' => $this->input->post("fechaNacimiento"),
            'domicilio'       => $this->input->post("domicilio"),
            'localidad'       => $this->input->post("localidad"),
            'provincia'       => $this->input->post("provincia"),
            'cp'              => $this->input->post("cp"),
            'telefono'        => $this->input->post("telefono"),
            'celular'         => $this->input->post("celular"),
            'estudios'        => $this->input->post("estudios"),
            'trabajo'         => $this->input->post("trabajo"),
            'referencia'      => $this->input->post("referencia"),
            'notas'           => $this->input->post("notas"),
        );

        //set validations
        $this->form_validation->set_rules("nombre", "Apellidos y nombres", "trim|required");
        $this->form_validation->set_rules("dni", "CUIL", "trim|required|min_length[8]|max_length[11]|numeric");
        $this->form_validation->set_rules("fechaNacimiento", "Fecha de nacimiento", "trim|date_check");
        $this->form_validation->set_rules("email", "Email", "trim|valid_email");

        $dataView = array(
            'alumno' => $alumno,
            'estudios' => $this->alumnos_model->getEstudios()
        );

        if ($this->form_validation->run() == FALSE) {
            //validation fails
            $this->load->view('alumnos/editar_view', $dataView);
        } else {
            $result = $this->alumnos_model->edit($idAlumno, $data);
            switch ($result) {
                case 0:
                    $this->session->set_flashdata('msg', '<div class="alert alert-info text-center">No se ha modificado el alumno</div>');
                    break;
                default:
                    $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Se ha editado correctamente el alumno</div>');
            };
            redirect("alumnos");
        }
    }

    public function eliminar($idAlumno)
    {
        $this->load->model('alumnos_model');
        $this->load->library('form_validation');

        $alumno = $this->alumnos_model->getAlumno($idAlumno);

        if (!isset($alumno)) {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">El alumno que se trata de eliminar no existe</div>');
            redirect('alumnos');
        }

        $result = $this->input->post("idAlumno");

        if (!isset($result)) {
            //validation fails
            $this->load->view('alumnos/eliminar_view', array('alumno' => $alumno));
        } else {
            $result = $this->alumnos_model->delete($idAlumno);
            switch ($result) {
                case 0:
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Se ha producido un error al eliminar el alumno</div>');
                    break;
                default:
                    $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Se ha eliminado correctamente el alumno</div>');
            };
            redirect("alumnos");
        }
    }
}
