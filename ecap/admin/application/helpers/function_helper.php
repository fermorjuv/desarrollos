<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
function formatEstado($estado)
{
    switch ($estado) {
        case 'approved' : //El pago fue aprobado y acreditado.
            return 'Aprobado';
            break;
        case 'pending' : //El usuario no completó el proceso de pago.
            return 'Pendiente';
            break;
        case 'in_process' : //El pago está siendo revisado.
            return 'En proceso';
            break;
        case 'rejected' : //El pago fue rechazado. El usuario puede intentar nuevamente.
            return 'Rechazado';
            break;
        case 'refunded' : //(estado terminal) El pago fue devuelto al usuario.
            return 'Devuelto';
            break;
        case 'cancelled' : //(estado terminal) El pago fue cancelado por superar el tiempo necesario para realizar el pago o por una de las partes.
            return 'Cancelado';
            break;
        case 'in_mediation' : //Se inició una disputa para el pago.
            return 'En mediaci&oacute;n';
            break;
        case 'charged_back' : //(estado terminal) Se realizó un contracargo en la tarjeta de crédito.
            return 'Contracargo';
            break;
        default: 
            return 'Error';
    }
}

function formatColorEstado($estado)
{
    switch ($estado) {
        case 'approved' : //El pago fue aprobado y acreditado.
            return 'green';
            break;
        case 'pending' : //El usuario no completó el proceso de pago.
        case 'in_process' : //El pago está siendo revisado.
        case 'in_mediation' : //Se inició una disputa para el pago.
            return 'orange';
            break;
        case 'rejected' : //El pago fue rechazado. El usuario puede intentar nuevamente.
        case 'refunded' : //(estado terminal) El pago fue devuelto al usuario.
        case 'cancelled' : //(estado terminal) El pago fue cancelado por superar el tiempo necesario para realizar el pago o por una de las partes.
        case 'charged_back' : //(estado terminal) Se realizó un contracargo en la tarjeta de crédito.
            return 'red';
            break;
        default: 
            return 'red';
    }
}

function formatEstadoDetalle($estadoDetalle)
{
    switch ($estadoDetalle) {
        case 'accredited' : //Done, your payment was accredited!
            return 'Acreditado';
            break;
        case 'pending_contingency' : //We are processing the payment. In less than an hour we will e-mail you the results.
            return 'Pendiente';
            break;
        case 'pending_review_manual' : //We are processing the payment. In less than 2 business days we will tell you by e-mail whether it has accredited or we need more information.
            return 'En revisi&oacute;n';
            break;
        case 'cc_rejected_bad_filled_card_number' : //Check the card number.
            return 'Rechazado n&uacute;mero err&oacute;neo tarjeta de cr&eacute;dito';
            break;
        case 'cc_rejected_bad_filled_date' : //Check the expiration date.
            return 'Rechazada fecha vencimiento tarjeta de cr&eacute;dito';
            break;
        case 'cc_rejected_bad_filled_security_code' : //Check the security code.
            return 'Rechazado c&oacute;digo de seguridad';
            break;
        case 'cc_rejected_blacklist' : //We could not process your payment.
            return 'Rechazado lista negra';
            break;
        case 'cc_rejected_call_for_authorize' : //You must authorize to payment_method_id the payment of amount to MercadoPago
            return 'Requiere autorizaci&oacute;n';
            break;
        case 'cc_rejected_card_disabled' : //Call payment_method_id to activate your card.
            return 'Tarjeta desactivada';
            break;
        case 'cc_rejected_card_error' : //We could not process your payment.
            return 'Tarjeta rechazada';
            break;
        case 'cc_rejected_duplicated_payment' : //You already made a payment for that amount.
            return 'Pago duplicado';
            break;
        case 'cc_rejected_high_risk' : //Your payment was rejected.
            return 'Pago rechazado';
            break;
        case 'cc_rejected_insufficient_amount' : //Your payment_method_id do not have sufficient funds.
            return 'Fondos insuficientes';
            break;
        case 'cc_rejected_invalid_installments' : //payment_method_id does not process payments in installments installments.
            return 'Rechazado por pago a plazo';
            break;
        case 'cc_rejected_max_attempts' : //You have reached the limit of allowed attempts.
            return 'M&aacute;xima cantidad de intentos';
            break;
        case 'cc_rejected_other_reason' : //payment_method_id did not process the payment.
        case 'cc_rejected_bad_filled_other' : //Check the information.
            return 'Rechazado motivo desconocido';
            break;
        default: 
            return 'Desconocido';
    }
}

function formatTipoPago($tipoPago)
{
    switch ($tipoPago) {
        case 'ticket' : //Printed ticket.
            return 'Ticket';
            break;
        case 'atm' : //Payment by ATM.
            return 'ATM';
            break;
        case 'credit_card' : //Payment by credit card.
            return 'T. cr&eacute;dito';
            break;
        case 'debit_card' : //Payment by debit card.
            return 'T. de d&eacute;bito';
            break;
        case 'prepaid_card' : //Payment by prepaid card
            return 'T. prepago';
            break;
        case 'transfer' :
            return 'Transferencia';
            break;
        case 'deposit' :
            return 'Dep&oacute;sito';
            break;
        case 'cash' :
            return 'Efectivo';
            break;
        case 'check' :
            return 'Cheque';
            break;
        case 'mercadolibre' :
            return 'Mercadolibre';
            break;
        default: 
            return $tipoPago;
    }   
}

function formatColorEstadoCurso($estado)
{
    switch($estado) {
        case 'Finalizado':
            return 'black';
            break;
        case 'No iniciado':
            return 'orange';
            break;
        case 'Iniciado':
            return 'green';
            break;
        case 'Pausado':
            return 'red';
            break;
        default:
            return 'black';
    }
}
















