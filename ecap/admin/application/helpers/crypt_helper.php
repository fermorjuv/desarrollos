<?php

function mycrypt($string) {
    return strtr(trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(ENCRYPTION_KEY), $string, MCRYPT_MODE_CBC, md5(md5(ENCRYPTION_KEY))))), '+/=', '-~_');
}

function mydecrypt($string) {
    return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(ENCRYPTION_KEY), base64_decode(strtr($string, '-~_', '+/=')), MCRYPT_MODE_CBC, md5(md5(ENCRYPTION_KEY))), "\0");
}