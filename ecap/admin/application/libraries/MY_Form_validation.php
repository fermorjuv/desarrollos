<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {
    protected $CI;

    function __construct()
    {
        parent::__construct();
        // reference to the CodeIgniter super object
        $this->CI =& get_instance();
    }

    function date_check($fecha)
    {          
        $this->CI->form_validation->set_message('date_check', 'La fecha %s no es v&aacute;lida.');
        
        //Si se inserto la fecha con /
        $fechaSplit = explode('/', $fecha);



        if (count($fechaSplit) == 3) {
            if (intval($fechaSplit[2]) < 1900) {
                return FALSE;
            }
            return (checkdate($fechaSplit[1], $fechaSplit[0], $fechaSplit[2]));
        }

        //Si se inserto la fecha con -
        $fechaSplit = explode('-', $fecha);

        if (count($fechaSplit) == 3) {
            if (intval($fechaSplit[2]) < 1900) {
                return FALSE;
            }
            return (checkdate($fechaSplit[1], $fechaSplit[0], $fechaSplit[2]));
        }
        
        return FALSE;
    }
}