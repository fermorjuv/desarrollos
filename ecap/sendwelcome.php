<?php
require_once('lib/mailer.php');

$codigo = mydecrypt($_GET['c']);
$data = explode('|', $codigo);

if (count($data) == 3) {

    list($idInscripcion, $email, $idCurso) = $data;

    $name = "Info ECAP";
    $to = $email;
    $from = "info@ecap.com.ar";

    $fileName = 'FormularioInscripcionEcap.pdf';
    $fileAttachment = 'docs/'.$fileName;

    $content = '<html>
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>ECAP - Escuela de Capacitaci&oacute;n Profesional</title>
                <style type="text/css">
                    body{margin: 0; font-family: arial, sans-serif;}
                    @media only screen and (max-width: 480px) {
                        body,table,td,p,a,li,blockquote {
                        -webkit-text-size-adjust:none !important;
                        }
                        table {width: 100% !important;}
                        .responsive-image img {
                        height: auto !important;
                        max-width: 100% !important;
                        width: 100% !important;
                        }
                    }
                </style>
                </head>
                <body style="font-family: Arial;">
                <p>Estimado alumno/a,</p> 
                <p>Ante todo queremos agradecerte por tu inter&eacute;s en nuestro curso. Solo est&aacute;s a un paso de pertenecer oficialmente a la comunidad educativa de ECAP.
                <p>Para ello complet&aacute; la solicitud de inscripci&oacute;n adjunta, la cual deber&aacute;s traer junto con la fotocopia de tu DNI y del CUIL/CUIT el d&iacute;a de inicio del mismo y realiza el pago seg&uacute;n el m&eacute;todo que m&aacute;s se ajuste a tus necesidades:
                <p>En el caso que quieras pagar con tarjeta de cr&eacute;dito a trav&eacute;s de Mercado Pago podes hacerlo directamente desde nuestra web: http://ecap.com.ar
                <p>Si quer&eacute;s pagar en cuotas a trav&eacute;s de RapiPago, Pago F&aacute;cil, Provincia Net, Link o mediante un dep&oacute;sito bancario o  una transferencia electr&oacute;nica, avisanos a info@ecap.com.ar o mandanos un Whatsapp al 11 3635-1342 y te enviaremos el link para que puedas bajar el cup&oacute;n de pago.<br />En el caso que elijas este medio de pago rogamos una vez realizado el pago tengas a bien enviarnos el comprobante a info@ecap.com.ar.
                <p>Esperando verte pr&oacute;ximamente por nuestras aulas, aprovechamos la ocasi&oacute;n para saludarte muy cordialmente.
                <p><img src="http://ecap.com.ar/img/logo-email.png">
                <p>Administraci&oacute;n ECAP<br />Lavalle 648 5º Piso<br />Whatsapp: 11 3635-1342<br />Web: <a href="http://ecap.com.ar">http://ecap.com.ar</a></p>
                </body>
                </html>';
 
    $subject = 'ECAP - Escuela de Capacitación Profesional';
     
    $email = new attach_mailer($name, $from, $to, $cc = "", $bcc = "", $subject, $content); 
    $email->create_attachment_part($fileAttachment); 
    $email->process_mail();

    echo $email->get_msg_str();
}