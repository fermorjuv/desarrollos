<?php
require_once('config.php');

$estados = array(
  'success',      //El pago fue aprobado y acreditado.
  'pending'       //El usuario no completó el proceso de pago.
);

$codigo = explode('|', mydecrypt($_GET['c']));

if (count($codigo) == 3 && (in_array($codigo[2], $estados))) {
    
    list($url, $curso, $estado) = $codigo;
?>
<!doctype html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, maximum-scale=1">
<meta name="robots" content="index, follow" />
<meta name="description" content="Resultado del pago. ECAP es un instituto de car&aacute;cter profesional que brinda cursos orientados a todo tipo de personas para oferecerles una r&aacute;pida salida laboral" />
<title>ECAP - Resultado del pago</title>
<link rel="icon" href="img/favicon.png" type="image/png">
<link href="css/all.css" rel="stylesheet" type="text/css" />
<link href="https://plus.google.com/+EcapArCursos" rel="publisher" />
 
<!--[if lt IE 9]>
    <script src="js/respond-1.1.0.min.js"></script>
    <script src="js/html5shiv.js"></script>
    <script src="js/html5element.js"></script>
<![endif]-->
 
</head>
<body>
<div id="hiddenTitle"><h1>ECAP - Escuela de Capacitaci&oacute;n Profesional</h1></div>
<!--Header_section-->
<header id="header_wrapper">
  <div class="container">
    <div class="header_box">
      <div class="logo"><a href="index.php"><img src="img/logo-thin.png" alt="ECAP" title="ECAP"></a></div>
    </div>
  </div>
</header>
<!--Header_section--> 

<section id="internal">
<div class="inner_wrapper">
  <div class="container">
    <h2><?php echo $curso?></h2>
    <div class="inner_section">
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <?php switch ($estado) {
                case 'success' : ?>
                    <p><span class="bold">Gracias por inscribirse al <?php echo $curso ?>!</span></p>
                    <br /><p>En breve le llegar&aacute; un email con toda la informaci&oacute;n del curso al correo electr&oacute;nico usado en la inscripci&oacute;n.</p>
                    <?php break;
                case 'pending' : ?>
                    <p><span class="bold">Gracias por inscribirse al <?php echo $curso ?>!</span></p>
                    <br /><p>Tan pronto como se nos acredite su pago le llegar&aacute; un email con toda la informaci&oacute;n del curso al correo electr&oacute;nico usado en la inscripci&oacute;n.</p>
                    <?php break;
                default: ?>
                    <p>Ha ocurrido un error inesperado con su pago. Por favor, pongase lo antes posible en contacto con nosotros para que podamos solucionar el problema.</p>
            <?php } ?>
            <br /><p>De todos modos podr&aacute; consultar toda la informaci&oacute;n del curso haciendo click <a href="<?php echo $url?>">aqu&iacute;</a>.</p>
          </div>
        </div>  
      </div>
    </div>
  </div> 
</div>
</section>

<?php include ('footer.php'); ?>

<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-all.js"></script>
<script type="text/javascript" src="js/analytics.js"></script>

<!-- Google Code for venta_ok Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 878852043;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "qrOvCJmajmgQy--IowM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/878852043/?label=qrOvCJmajmgQy--IowM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</body>
</html>
<?php
}
?>