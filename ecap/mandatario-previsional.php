<?php
require_once('config.php');
?>
<!doctype html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, maximum-scale=1">
<meta name="robots" content="index, follow" />
<meta name="description" content="Curso de Asesor Previsional. ECAP es un instituto de car&aacute;cter profesional que brinda cursos orientados a todo tipo de personas para oferecerles una r&aacute;pida salida laboral" />
<title>ECAP - Curso Asesor Previsional</title>
<link rel="icon" href="img/favicon.png" type="image/png">
<link href="css/all.css" rel="stylesheet" type="text/css" />
<link href="https://plus.google.com/+EcapArCursos" rel="publisher" />
 
<!--[if lt IE 9]>
    <script src="js/respond-1.1.0.min.js"></script>
    <script src="js/html5shiv.js"></script>
    <script src="js/html5element.js"></script>
<![endif]-->
 
</head>
<body>
<div id="hiddenTitle"><h1>ECAP - Escuela de Capacitaci&oacute;n Profesional</h1></div>
<!--Header_section-->
<?php include ('header-thin.php'); ?>
<!--Header_section--> 

<!--Hero_Section-->
<section id="hero_section" class="top_cont_outer">
  <div class="hero_wrapper">
    <div class="container">
      <div class="hero_section">
        <div class="row">
          <div>
            <div class="top_left_cont">
              <ul class="bxslider">
                <li><img src="img/mandatario-previsional.png" alt="Curso Asesor Previsional" title="Curso Asesor Previsional" /></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Hero_Section--> 

<section id="internal">
<div class="inner_wrapper">
  <div class="container">
    <h2>Curso de Asesor Previsional</h2>
    <div class="inner_section">
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <h3>Objetivos</h3><br/>
            <p>Es un curso que, m&aacute;s all&aacute; de la teor&iacute;a, se caracterizara por brindar todos los conocimientos para resolver casos pr&aacute;cticos, para que se pueda aplicar lo desarrollado en cada modulo y por lo tanto lograr una completa comprensi&oacute;n del temario, con el ejercicio resolviendo casos reales.</p>
            <p><span class="bold">Ning&uacute;n curso de Gestor&iacute;a Previsional a nivel nacional, habilita a los aspirantes a sacar matr&iacute;cula de ANSES. De todos modos, cualquier persona f&iacute;sica puede realizar cualquier tr&aacute;mite ante ANSES con los conocimientos de este programa.</span></p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Certificaciones obtenidas</h3><br/>
            <p>Diploma de asistencia y de aprobaci&oacute;n otorgado por ECAP.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Metodolog&iacute;a</h3><br/>
            <p>Este curso es de car&aacute;cter te&oacute;rico-pr&aacute;ctico. La metodolog&iacute;a ser&aacute; din&aacute;mica, se ense&ntilde;ar&aacute;n los aspectos legales, t&eacute;cnicos, y administrativos en cada caso.<br />Cabe destacar que este curso se dicta en un aula con PC de tan solo 12 personas (un alumno por PC), lo que lo hace intensivo, muy personalizado y din&aacute;mico.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Informaci&oacute;n general</h3><br/>
            <p><span class="bold">Duraci&oacute;n:</span> 30 horas.</p>
            <p><span class="bold">Precio:</span> $2.200 (Dentro de este precio est&aacute;n incluidos los materiales para estudio y el certificado de asistencia y aprobaci&oacute;n del mismo).</p>
            <p><span class="bold">Modalidad:</span> presencial.</p>
            <p><span class="bold">Condiciones de inscripci&oacute;n:</span></p>
            <ul class="listado">
              <li>Ser mayor de edad o menor emancipado.</li>
              <li>Completar la ficha de inscripci&oacute;n.</li>
              <li>Abonar el curso a trav&eacute;s de cualquiera de nuestros medios de pago (efectivo a trav&eacute;s de dep&oacute;sito o transferencia bancaria y/o cualquiera de los medios de pago habilitados a trav&eacute;s de Mercado Pago).</li>
            </ul>
            <p><span class="bold">Documentaci&oacute;n a presentar:</span></p>
            <ul class="listado">
              <li>DNI (Original y fotocopia).</li>
              <li>CUIL O CUIT (seg&uacute;n corresponda)</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Programa</h3><br/>
            <p><a target="_blank" href="programas/mandatario-previsional.pdf"><span><i class="fa fa-2x fa-file-pdf-o"></i></span></a></p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Lugar de Cursada, d&iacute;as y horarios</h3><br/>
            <p><span class="bold">Lugar de cursada:</span> Lavalle 648 5&deg; piso, Capital Federal.</p>
            <p><span class="bold">D&iacute;as y horario del curso:</span> Martes y jueves de 18.30 a 21.30 hs.</p>
            <ul class="listado">
              <li>Clase 1: Martes 13 de septiembre de 2016
              <li>Clase 2: Jueves 15 de septiembre de 2016
              <li>Clase 3: Martes 20 de septiembre de 2016
              <li>Clase 4: Jueves 22 de septiembre de 2016
              <li>Clase 5: Martes 27 de septiembre de 2016
              <li>Clase 6: Jueves 29 de septiembre de 2016
              <li>Clase 7: Martes 04 de octubre de 2016
              <li>Clase 8: Jueves 06 de octubre de 2016
              <li>Clase 9: Martes 11 de octubre de 2016
              <li>Clase 10: Jueves 13 de octubre de 2016
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br /><br />
            <div id="inscriptionDiv" class="float-left"><a id="inscriptionBtn" class='contact_btn'>Inscribirse</a></div>
            <div id="inscriptionForm" class="float-left">
              <p>Por favor, introduzca lo siguientes datos para poder continuar con el proceso de inscripci&oacute;n</p>
              <form name="inscriptionForm" id="inscriptionForm" action="" method="post">
                <input id="inscriptionName" class="input-text" type="text" name="" value="Nombre completo" defaultValue="Nombre completo" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                <input id="inscriptionEmail" class="input-text" type="text" name="" value="E-mail" defaultValue="E-mail" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                <input id="inscriptionReference" class="input-text" type="text" name="" value="C&oacute;mo nos conoci&oacute;" defaultValue="C&oacute;mo nos conoci&oacute;" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                <input type="hidden" id="inscriptionCourse" value="<?php echo mycrypt('27')?>" />
                <a id="sendInscriptionBtn" class="contact_btn">Continuar</a>
                <p id="inscriptionError" class="contact-error"></p>
              </form>
            </div>
            <br/>
          </div>
        </div>  
      </div>
    </div>
  </div> 
</div>
</section>

<?php include ('footer.php'); ?>

<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-all.js"></script>
<script type="text/javascript" src="js/inscription.js"></script>
<script type="text/javascript" src="js/analytics.js"></script>

</body>
</html>