<?php
define("ENCRYPTION_KEY", "MuFaSa");

function mycrypt($string) {
    return strtr(trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(ENCRYPTION_KEY), $string, MCRYPT_MODE_CBC, md5(md5(ENCRYPTION_KEY))))), '+/=', '-~_');
}

function mydecrypt($string) {
    return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(ENCRYPTION_KEY), base64_decode(strtr($string, '-~_', '+/=')), MCRYPT_MODE_CBC, md5(md5(ENCRYPTION_KEY))), "\0");
}

//Funcion para cortar el contenido del post blog hasta el "Leer mas"
function formatContentNew($content) {
    $init = strpos($content, '<p>');
    $end = strpos($content, '<span id="more-')-3;
    return substr($content, $init, $end-$init);
}

//Funcion para mostrar uno u otro icono segun el tema del post del blog
function formatIconNew($category) {
    switch ($category) {
        case 'Gestor Automotor':    return 'fa-car';
        case 'Gestor Judicial':     return 'fa-balance-scale';
        case 'Gestor Previsional':  return 'fa-umbrella';
        default:                    return 'fa-newspaper-o';
    }
}

$baseUrl = 'http://ecap.com.ar';