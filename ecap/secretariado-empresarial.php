<?php
require_once('config.php');
?>
<!doctype html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, maximum-scale=1">
<meta name="robots" content="index, follow" />
<meta name="description" content="Curso de Secretariado Empresarial. ECAP es un instituto de car&aacute;cter profesional que brinda cursos orientados a todo tipo de personas para oferecerles una r&aacute;pida salida laboral" />
<title>ECAP - Curso Secretariado Empresarial</title>
<link rel="icon" href="img/favicon.png" type="image/png">
<link href="css/all.css" rel="stylesheet" type="text/css" />
<link href="https://plus.google.com/+EcapArCursos" rel="publisher" />
 
<!--[if lt IE 9]>
    <script src="js/respond-1.1.0.min.js"></script>
    <script src="js/html5shiv.js"></script>
    <script src="js/html5element.js"></script>
<![endif]-->
 
</head>
<body>
<div id="hiddenTitle"><h1>ECAP - Escuela de Capacitaci&oacute;n Profesional</h1></div>
<!--Header_section-->
<?php include ('header-thin.php'); ?>
<!--Header_section--> 

<!--Hero_Section-->
<section id="hero_section" class="top_cont_outer">
  <div class="hero_wrapper">
    <div class="container">
      <div class="hero_section">
        <div class="row">
          <div>
            <div class="top_left_cont">
              <ul class="bxslider">
                <li><img src="img/secretariado-empresarial.png" alt="Curso Secretariado Empresarial" title="Curso Secretariado Empresarial" /></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Hero_Section--> 

<section id="internal">
<div class="inner_wrapper">
  <div class="container">
    <h2>Curso de Secretariado Empresarial</h2>
    <div class="inner_section">
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <h3>Introducci&oacute;n</h3><br/>
            <p>La secretaria empresarial posee un rol ligada a las relaciones humanas y p&uacute;blicas. Excelente manejo de PC, redacci&oacute;n comercial y protocolar. Manejo de agenda, coordinaci&oacute;n y participaci&oacute;n en forma directa con la parte jer&aacute;rquica de la empresa. Debe contar con el criterio suficiente para tomar decisiones en el momento oportuno. La secretaria empresarial es HOY esencial en toda aquella organizaci&oacute;n, cuyo personal jer&aacute;rquico necesite realizar relaciones p&uacute;blicas para poder desempe&ntilde;ar en la misma un crecimiento y progreso.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Objetivos</h3><br/>
            <p>Obtener la Capacidad y conocimientos t&eacute;cnicos para asistir a empresarios y/o ejecutivos superiores en diferentes &aacute;reas de la empresa. Obtener la Capacidad y conocimientos t&eacute;cnicos para asistir a empresarios y/o ejecutivos superiores en diferentes &aacute;reas de la empresa. Conocer el perfil profesional de un/a Secretario/a Ejecutivo/a. Aprender el manejo de agendas, coordinaci&oacute;n de vuelos, reuniones. Incorporar t&eacute;cnicas adecuadas de comunicaci&eacute;n interpersonal. Aprender a organizar eventos empresariales y reuniones de negocios.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Certificaciones obtenidas</h3><br/>
            <p>Diploma de asistencia y de aprobaci&oacute;n otorgado por ECAP.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Informaci&oacute;n general</h3><br/>
            <p><span class="bold">Duraci&oacute;n:</span> 24 horas.</p>
            <p><span class="bold">Precio:</span> $2.000 (Dentro de este precio est&aacute;n incluidos los materiales para estudio y el certificado de asistencia y aprobaci&oacute;n del mismo).</p>
            <p><span class="bold">Modalidad:</span> presencial.</p>
            <p><span class="bold">Condiciones de inscripci&oacute;n:</span></p>
            <ul class="listado">
              <li>Ser mayor de edad o menor emancipado.</li>
              <li>Completar la ficha de inscripci&oacute;n.</li>
              <li>Abonar el curso a trav&eacute;s de cualquiera de nuestros medios de pago (efectivo a trav&eacute;s de dep&oacute;sito o transferencia bancaria y/o cualquiera de los medios de pago habilitados a trav&eacute;s de Mercado Pago).</li>
            </ul>
            <p><span class="bold">Documentaci&oacute;n a presentar:</span></p>
            <ul class="listado">
              <li>DNI (Original y fotocopia).</li>
              <li>CUIL O CUIT (seg&uacute;n corresponda)</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Programa</h3><br/>
            <p><a target="_blank" href="programas/secretariado-empresarial.pdf"><span><i class="fa fa-2x fa-file-pdf-o"></i></span></a></p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br/><br/><h3>Lugar de Cursada, d&iacute;as y horarios</h3><br/>
            <p><span class="bold">Lugar de cursada:</span> Lavalle 648 5&deg; piso, Capital Federal.</p>
            <p><span class="bold">D&iacute;as y horario del curso:</span> S&aacute;bados de 11 a 14 hs.</p>
            <ul class="listado">
              <li>Clase 1: 13 de agosto de 2016</li>
              <li>Clase 2: 20 de agosto de 2016</li>
              <li>Clase 3: 27 de agosto de 2016</li>
              <li>Clase 4: 03 de septiembre de 2016</li>
              <li>Clase 5: 10 de septiembre de 2016</li>
              <li>Clase 6: 17 de septiembre de 2016</li>
              <li>Clase 7: 24 de septiembre de 2016</li>
              <li>Examen final: 01 de octubre de 2016</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated">
            <br /><br />
            <div id="inscriptionDiv" class="float-left"><a id="inscriptionBtn" class='contact_btn'>Inscribirse</a></div>
            <div id="inscriptionForm" class="float-left">
              <p>Por favor, introduzca lo siguientes datos para poder continuar con el proceso de inscripci&oacute;n</p>
              <form name="inscriptionForm" id="inscriptionForm" action="" method="post">
                <input id="inscriptionName" class="input-text" type="text" name="" value="Nombre completo" defaultValue="Nombre completo" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                <input id="inscriptionEmail" class="input-text" type="text" name="" value="E-mail" defaultValue="E-mail" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                <input id="inscriptionReference" class="input-text" type="text" name="" value="C&oacute;mo nos conoci&oacute;" defaultValue="C&oacute;mo nos conoci&oacute;" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                <input type="hidden" id="inscriptionCourse" value="<?php echo mycrypt('20')?>" />
                <a id="sendInscriptionBtn" class="contact_btn">Continuar</a>
                <p id="inscriptionError" class="contact-error"></p>
              </form>
            </div>
            <br/>
          </div>
        </div>  
      </div>
    </div>
  </div> 
</div>
</section>

<?php include ('footer.php'); ?>

<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-all.js"></script>
<script type="text/javascript" src="js/inscription.js"></script>
<script type="text/javascript" src="js/analytics.js"></script>

</body>
</html>