<footer class="footer_wrapper" id="contacto">
  <div class="container">
    <section class="page_section contact">
      <div class="contact_section">
        <h2>Contacto</h2>
        <div class="row">
          <div class="col-lg-4">
            
          </div>
          <div class="col-lg-4">
           
          </div>
          <div class="col-lg-4">
          
          </div>
        </div>
      </div>
      <div class="row">
         <div class="col-lg-12 wow fadeInLeft delay-06s">
          <div id="contact-div" class="form">
            <form name="contactForm" id="contactForm" action="sendemail.php" method="post">
              <p id="nombre-error" class="contact-error"></p>
              <input id="nombre" class="input-text" type="text" name="" value="Nombre *" defaultValue="Nombre *" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
              <p id="email-error" class="contact-error"></p>
              <input id="email" class="input-text" type="text" name="" value="E-mail *" defaultValue="E-mail *" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
              <p id="mensaje-error" class="contact-error"></p>
              <textarea id="mensaje" class="input-text text-area" cols="0" rows="0" placeholder="Mensaje *"></textarea>
              <input id="contactSubmit" class="input-btn" type="submit" value="Enviar">
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div class="container">
    <div class="twitter-hover social-slide"></div>
    <div class="footer_bottom"><span>ECAP - Escuela de Capacitaci&oacute;n Profesional<br />
      Lavalle 648, 5&deg; piso, Ciudad Aut&oacute;noma de Buenos Aires ( <a href="https://www.google.com.ar/maps/place/Lavalle+648,+C1047AAN+CABA/@-34.6022425,-58.3760184,17z/data=!3m1!4b1!4m2!3m1!1s0x95bccacc3f796b0b:0xe5262165134c25b6?hl=es" target="_blank"><i class="fa fa-map-marker"></i> ver mapa</a> )<br />
      Tel&eacute;fono: <a href="tel:01142623995">(011) 4262-3995</a>&nbsp;/&nbsp;Whatsapp: <a href="tel:01136351342">(011) 5967-3702</a><br />
      Email: <a href="mailto:info@ecap.com.ar">info@ecap.com.ar</a><br />
      Copyright &copy; 2015</span> </div>
  </div>
</footer>