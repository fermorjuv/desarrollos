$(document).ready(function() {

    function checkLength( o, n, min, max ) {

      if ( $('#'+o).val().length > max || $('#'+o).val().length < min || $('#'+o).val() == $('#'+o).prop("defaultValue") ) {
        if (n != "") {
          $('#'+o+'-error').show();
          $('#'+o+'-error').html( "La longitud de " + n + " debe ser entre " + min + " y " + max + "." );
        } else {
          $('#'+o).css("border-color", "red");
        }
        return false;
      } else {
        if (n != "") {
          $('#'+o+'-error').hide();
        } else {
          $('#'+o).css("border-color", "#aaa");
        }
        return true;
      }
    }

    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( $('#'+o).val() ) ) ) {
        if (n != "") {
          $('#'+o+'-error').show();
          $('#'+o+'-error').html( n );
        } else {
          $('#'+o).css("border-color", "red");
        }
        return false;
      } else {
        if (n != "") {
          $('#'+o+'-error').hide();
        } else {
          $('#'+o).css("border-color", "#aaa");
        }
        return true;
      }
    }

    function checkSelect( o, n, v ) {

      if ( $('#'+o).val() == v ) {
        if (n != "") {
          $('#'+o+'-error').show();
          $('#'+o+'-error').html( "El campo " + n + " no tiene un valor correcto" );
        } else {
          $('#'+o).css("border-color", "red");
        }
        return false;
      } else {
        if (n != "") {
          $('#'+o+'-error').hide();
        } else {
          $('#'+o).css("border-color", "#aaa");
        }
        return true;
      }
    }

    function sendContactForm(form) {
      var formURL = form.attr("action");

      $.post( formURL, { nombre: $('#nombre').val(), email: $('#email').val(), mensaje: $('#mensaje').val() })
      .done(function( data ) {
        $('#contact-div').removeClass('form');
        $('#contact-div').html('<p class="contact_info_result">'+data+'</p>');
      })
      .fail(function() {
        $('#contact-div').removeClass('form');
        $('#contact-div').html('<p class="contact_info_result">El formulario no se ha podido enviar. Por favor, int&eacute;ntelo m&aacute;s tarde.</p>');
      });
    }

    function sendInformationForm(form) {
      var formURL = form.attr("action");

      $.post( formURL, { email: $('#informationEmail').val(), curso: $('#informationCurso').val() })
      .done(function( data ) {
        $('#information-div').html('<p class="information_info_result">'+data+'</p>');
      })
      .fail(function() {
        $('#information-div').html('<p class="information_info_result">El formulario no se ha podido enviar. Por favor, int&eacute;ntelo m&aacute;s tarde.</p>');
      });
    }

    //CONTACT
    $("#contactSubmit").click(function() {

      var bValid = true;
      bValid = bValid && checkLength( "nombre", "nombre", 3, 80 );
      bValid = bValid && checkLength( "email", "e-mail", 6, 80 );
      bValid = bValid && checkLength( "mensaje", "mensaje", 1, 300 );

      bValid = bValid && checkRegexp( "name", /^[a-zA-Z]([0-9a-zA-Z_ ])+$/i, "El nombre es incorrecto" );
      bValid = bValid && checkRegexp( "email", /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "El email es incorrecto" );

      if ( bValid ) {
          sendContactForm($( "#contactForm" ));
      }
      return false;

    });

    //INFORMATION
    $("#informationSend").click(function() {

      var bValid = true;
      bValid = bValid && checkLength( "informationEmail", "", 6, 80 );
      bValid = bValid && checkRegexp( "informationEmail", /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "" );
      bValid = bValid && checkSelect( "informationCurso", "", "" );

      if ( bValid ) {
          sendInformationForm($( "#informationForm" ));
      }
      return false;
      
    });
});