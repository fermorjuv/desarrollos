$(document).ready(function() {
    $("#inscriptionForm").hide();

    $("#inscriptionBtn").click(function(){
      $("#inscriptionDiv").hide();
      $("#inscriptionForm").show();
    });

    $("#sendInscriptionBtn").click(function() {
        if (checkLength( 'inscriptionName', 'l nombre completo', 5, 200)
            && checkRegexp( 'inscriptionEmail', /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "El email es incorrecto" )
            && checkLength( 'inscriptionReference', ' c&oacute;mo nos conoci&oacute;', 5, 200)) {
            $.post( "http://ecap.com.ar/admin/inscripciones/alta", { id: $('#inscriptionCourse').val(), name: $('#inscriptionName').val(), email: $('#inscriptionEmail').val(), reference: $('#inscriptionReference').val() })
            .always(function( data ) {
                location.href = 'payment-method.php?c='+data;
            });   
        };
    });

});

function checkLength( o, n, min, max ) {
  if ( $('#'+o).val().length > max || $('#'+o).val().length < min || $('#'+o).val() == $('#'+o).prop("defaultValue") ) {
    $('#inscriptionError').show();
    $('#inscriptionError').html( "La longitud de" + n + " debe ser entre " + min + " y " + max + "." );
    return false;
  } else {
    $('#inscriptionError').hide();
    return true;
  }
}

function checkRegexp( o, regexp, n ) {
  if ( !( regexp.test( $('#'+o).val() ) ) ) {
    $('#inscriptionError').show();
    $('#inscriptionError').html( n );
    return false;
  } else {
    $('#inscriptionError').hide();
    return true;
  }
}