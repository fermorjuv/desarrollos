<!doctype html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, maximum-scale=1">
<meta name="robots" content="index, follow" />
<meta name="description" content="Error. ECAP es un instituto de car&aacute;cter profesional que brinda cursos orientados a todo tipo de personas para oferecerles una r&aacute;pida salida laboral" />
<title>ECAP - Error</title>
<link rel="icon" href="img/favicon.png" type="image/png">
<link href="css/all.css" rel="stylesheet" type="text/css" />
<link href="https://plus.google.com/+EcapArCursos" rel="publisher" />
 
<!--[if lt IE 9]>
    <script src="js/respond-1.1.0.min.js"></script>
    <script src="js/html5shiv.js"></script>
    <script src="js/html5element.js"></script>
<![endif]-->
 
</head>
<body>
<div id="hiddenTitle"><h1>ECAP - Escuela de Capacitaci&oacute;n Profesional</h1></div>
<!--Header_section-->
<header id="header_wrapper">
  <div class="container">
    <div class="header_box">
      <div class="logo"><a href="index.php"><img src="img/logo.png" alt="ECAP" title="ECAP"></a></div>
    </div>
  </div>
</header>
<!--Header_section--> 

<section id="internal">
<div class="inner_wrapper">
  <div class="container">
    <h2>Error</h2>
    <div class="inner_section">
      <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
          <div class=" delay-01s animated fadeInDown wow animated center">
            <p><span class="bold">Lo sentimos, pero ha ocurrido un error. Por favor, comun&iacute;quese con nosotros y trataremos de resolver su problema lo antes posible.</span></p>
          </div>
        </div>  
      </div>
    </div>
  </div> 
</div>
</section>

<?php include ('footer.php'); ?>

<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-all.js"></script>
<script type="text/javascript" src="js/analytics.js"></script>

</body>
</html>